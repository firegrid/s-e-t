from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import datetime

import settings
import style
from general.functions import writeLog

from gui.misc.Buttons import ImportantButton, AppButton
from gui.misc.Forms import TouchSpinBox

class TimeChangeWidget(QWidget):
	okSignal = pyqtSignal()

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.time = datetime.datetime.now()
		self.modified = 0

		self.dateLayout = QGridLayout()

		self.titleLabel = QLabel("")
		self.titleLabel.setFont(style.Fonts.h1)
		self.dateLayout.addWidget(self.titleLabel, 0, 1, 1, -1)

		self.dateLayout.addItem(style.Layout.vSpacer, 3, 0)
		self.dateLayout.addItem(style.Layout.hSpacer, 0, 0)
		self.dateLayout.addItem(style.Layout.hSpacer, 0, 4)
		self.dateLayout.addItem(style.Layout.hSpacer, 0, 7)


		self.dayBox = TouchSpinBox()
		self.dayBox.valueChanged.connect(self.changingSlot)
		self.dayBox.setRange(1, 31)
		
		self.monthBox = TouchSpinBox()
		self.monthBox.valueChanged.connect(self.changingSlot)
		self.monthBox.setRange(1, 12)
		
		self.yearBox = TouchSpinBox()
		self.yearBox.valueChanged.connect(self.changingSlot)
		self.yearBox.setRange(0, 9999)
		
		self.hourBox = TouchSpinBox()
		self.hourBox.valueChanged.connect(self.changingSlot)
		self.hourBox.setRange(0, 23)
		
		self.minuteBox = TouchSpinBox()
		self.minuteBox.valueChanged.connect(self.changingSlot)
		self.minuteBox.setRange(0, 59)


		#chain the SpinBoxes together (no connection to yearbox required)
		self.dayBox.maximumReached.connect(lambda:self.monthBox.incrementValue())
		self.monthBox.maximumReached.connect(lambda:self.yearBox.incrementValue())
		self.hourBox.maximumReached.connect(lambda:self.dayBox.incrementValue())
		self.minuteBox.maximumReached.connect(lambda:self.hourBox.incrementValue())

		self.dayBox.minimumReached.connect(lambda:self.monthBox.decrementValue())
		self.monthBox.minimumReached.connect(lambda:self.yearBox.decrementValue())
		self.hourBox.minimumReached.connect(lambda:self.dayBox.decrementValue())
		self.minuteBox.minimumReached.connect(lambda:self.hourBox.decrementValue())

		#add them to the layout
		dayLabel = QLabel("Tag")
		dayLabel.setFont(style.Fonts.h2)
		dayLabel.setFixedHeight(150)
		dayLabel.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
		monthLabel = QLabel("Monat")
		monthLabel.setFont(style.Fonts.h2)
		monthLabel.setFixedHeight(150)
		monthLabel.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
		yearLabel = QLabel("Jahr")
		yearLabel.setFont(style.Fonts.h2)
		yearLabel.setFixedHeight(150)
		yearLabel.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
		hourLabel = QLabel("Stunde")
		hourLabel.setFont(style.Fonts.h2)
		hourLabel.setFixedHeight(150)
		hourLabel.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
		minuteLabel = QLabel("Minute")
		minuteLabel.setFont(style.Fonts.h2)
		minuteLabel.setFixedHeight(150)
		minuteLabel.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

		labelRow = 1
		widgetRow = 2

		self.dateLayout.addWidget(dayLabel, labelRow, 1)
		self.dateLayout.addWidget(monthLabel, labelRow, 2)
		self.dateLayout.addWidget(yearLabel, labelRow, 3)
		self.dateLayout.addWidget(hourLabel, labelRow, 5)
		self.dateLayout.addWidget(minuteLabel, labelRow, 6)

		self.dateLayout.addWidget(self.dayBox, widgetRow, 1)
		self.dateLayout.addWidget(self.monthBox, widgetRow, 2)
		self.dateLayout.addWidget(self.yearBox, widgetRow, 3)
		self.dateLayout.addWidget(self.hourBox, widgetRow, 5)
		self.dateLayout.addWidget(self.minuteBox, widgetRow, 6)

		self.controlView = QWidget()
		self.controlLayout = QHBoxLayout(self.controlView)

		self.nowButton = AppButton("Jetzt")
		self.nowButton.clicked.connect(self.toNowTime)

		self.okButton = ImportantButton("Übernehmen")
		self.okButton.startAnimation()
		self.okButton.clicked.connect(self.sendSlot)

		self.controlLayout.addWidget(self.nowButton)
		self.controlLayout.addWidget(self.okButton)

		self.dateLayout.addWidget(self.controlView, 4, 0, 1, 8)

		self.changeTime()

		self.setLayout(self.dateLayout)

	def changeParams(self, time, reason):
		self.time = time
		self.titleLabel.setText(reason)
		self.changeTime()
		self.modified = 0

	def sendSlot(self):
		self.time = datetime.datetime(year=int(self.yearBox.value()), month=int(self.monthBox.value()), day=int(self.dayBox.value()), hour=int(self.hourBox.value()), minute=int(self.minuteBox.value()))
		self.okSignal.emit()

	def toNowTime(self):
		self.time = datetime.datetime.now()
		self.changeTime()
		self.modified = 2

	def changeTime(self):
		self.yearBox.setValue(self.time.year)
		self.monthBox.setValue(self.time.month)
		self.dayBox.setValue(self.time.day)
		self.hourBox.setValue(self.time.hour)
		self.minuteBox.setValue(self.time.minute)

	def changingSlot(self):
		self.modified = 1





