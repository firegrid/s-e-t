from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import style

from gui.QtFunctions import clearLayout

from gui.misc.flickcharm import *

class ScrollableArea(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QVBoxLayout(self)

		self.scrollableView = QScrollArea()
		self.scrollableView.setFrameShape(QFrame.NoFrame)
		self.scrollableView.setWidgetResizable(True)

		self.mainLayout.addWidget(self.scrollableView)

	def setInnerWidget(self, widget):
		self.scrollableView.setWidget(widget)

		self.charm = FlickCharm()
		self.charm.activateOn(self.scrollableView)


import math

class ColLayout(QWidget):
	'''
		cols = integer -> number of columns
		data = list of items
		itemWidget = widget of one item
	'''
	def __init__(self, cols, data, itemWidget, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QHBoxLayout(self)

		self.cols = cols
		self.itemWidget = itemWidget

		self.refresh(data)

	def addItem(self, item):
		self.lastCol.takeAt(self.lastCol.count()-1)
		self.lastCol.addWidget(item)
		self.lastCol.addItem(style.Layout.vSpacer)

	def refresh(self, data):
		# for i in reversed(range(self.mainLayout.count())):
		# 	l = self.mainLayout.itemAt(i).layout()
		# 	for i in reversed(range(l.count())):
		# 		if(l.itemAt(i).spacerItem):
		# 			l.takeAt(i)
		# 		else:
		# 			l.itemAt(i).widget().deleteLater()
		# 	l.deleteLater()


		clearLayout(self.mainLayout)

		minLength = 5 #todo customizeable??

		colLen = data.__len__()
		oneColumn = math.ceil(colLen / self.cols)

		if oneColumn < minLength:
			oneColumn = minLength

		start = 0

		for colIndex in range(self.cols):
			col = QVBoxLayout()

			for d in data[start:start+oneColumn]:
				item = self.itemWidget(d)

				col.addWidget(item)
			col.addItem(style.Layout.vSpacer)
			self.mainLayout.addLayout(col)
			start = start+oneColumn

		self.lastCol = col
		# self.lastCol.addItem(style.Layout.vSpacer)