from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import style

from gui.misc.Buttons import RequiredButton, OptionalButton
from gui.misc.Emitters import Emitter


class OptionalQWidget(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.mainLayout = QVBoxLayout(self)
		self.mainLayout.setContentsMargins(0,0,0,0)
		self.open = False

		self.showText = "Erweitert"
		self.hideText = "Erweitert"

		controlWidget = QWidget()
		controlLayout = QHBoxLayout(controlWidget)

		self.controlButton = OptionalButton(self.showText)
		self.controlButton.clicked.connect(self.toggleWidget)

		controlLayout.addWidget(self.controlButton)

		self.mainLayout.addWidget(controlWidget)

		self.contentView = QWidget()

		self.contentLayout = QVBoxLayout(self.contentView)
		self.contentLayout.setSpacing(25)
		self.contentView.hide()

		self.mainLayout.addWidget(self.contentView)

	def toggleWidget(self):
		self.open = not self.open

		if self.open:
			self.controlButton.stopAnimation()
			self.controlButton.setText(self.hideText)
			self.controlButton.hide()
			self.contentView.show()
		else:
			self.controlButton.setText(self.showText)
			self.contentView.hide()

	def setOptionalContent(self, widget):
		for i in range(self.contentLayout.count()):
			self.contentLayout.itemAt(i).widget().deleteLater()

		self.addOptionalContent(widget)

	def addOptionalContent(self, widget):
		self.contentLayout.addWidget(widget)

	def addOptionalSpacer(self):
		self.contentLayout.addItem(style.Layout.vSpacer)

	def reset(self):
		self.controlButton.show()

		self.controlButton.animation.startAnimation()
		self.open = True
		self.toggleWidget()



import os
import settings

class PictureWidget(QLabel):
	clicked = pyqtSignal()
	def __init__(self, pictureFile, specificNoPicPath="", parent=None):

		QLabel.__init__(self, parent)
		self.setScaledContents(True)
		self.setObjectName("picture")

		self.pixmap = QtGui.QPixmap(pictureFile)
	
		if self.pixmap.isNull():
			self.pixmap = QtGui.QPixmap(":/" + settings.Settings.get("Application", "imagePath") + pictureFile)
			# self.pixmap = QtGui.QPixmap(os.getcwd() + settings.Settings.get("Application", "imagePath") + pictureFile)

		if self.pixmap.isNull():
			self.pixmap = QtGui.QPixmap(":/media/images/firegrid_logo.png")

		self.setPixmap(self.pixmap.scaled(self.pixmap.width(), self.pixmap.height(), Qt.KeepAspectRatio))

		self.width = self.pixmap.width()
		self.height = self.pixmap.height()

		self.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))

	def mousePressEvent(self, event):
		Emitter.getInstance("clickSound").emit()
		self.clicked.emit()
		super(PictureWidget, self).mousePressEvent(event)

	def sizeHint(self):
		return QSize(self.width, self.height)




class ToggleWidget(QFrame):
	clicked = pyqtSignal()
	def __init__(self, permanentWidget, openWidget, closedWidget, checked = False, parent=None):
		QFrame.__init__(self, parent)

		self.open = checked

		self.mainLayout = QVBoxLayout(self)
		self.mainLayout.setSpacing(0)

		if permanentWidget != None:
			self.permanentWidget = permanentWidget
			self.mainLayout.addWidget(self.permanentWidget)

		self.closedWidget = closedWidget
		if self.closedWidget != None:
			self.mainLayout.addWidget(self.closedWidget)

		self.openWidget = openWidget
		if self.openWidget != None:
			self.mainLayout.addWidget(self.openWidget)
			

		if checked:
			if self.closedWidget != None:
				self.closedWidget.hide()
		else:
			if self.openWidget != None:
				self.openWidget.hide()


	def toggle(self):
		self.open = not self.open

		if self.open:
			if self.openWidget != None:
				self.openWidget.show()
			if self.closedWidget != None:
				self.closedWidget.hide()
		else:
			if self.openWidget != None:
				self.openWidget.hide()
			if self.closedWidget != None:
				self.closedWidget.show()

		# self.update()
		self.clicked.emit()


	def mousePressEvent(self, event):
		Emitter.getInstance("clickSound").emit()
		self.toggle()

		QFrame.mousePressEvent(self,event)



class StyledToggleWidget(ToggleWidget):
	clicked = pyqtSignal()
	def __init__(self, permanentWidget, openWidget, closedWidget, checked = False, openBG = None, closedBG = None, addStyle = "", parent=None):
		super(StyledToggleWidget, self).__init__(permanentWidget, openWidget, closedWidget, checked, parent)

		self.closedBG = closedBG
		self.openBG = openBG

		if openBG == None:
			self.openBG = style.Color.scheme.activeColor

		if closedBG == None:
			self.closedBG = style.Color.scheme.activeColor

		#todo make customizeable
		self.basicStyle = """StyledToggleWidget{
			border-radius: """+ "5px" +""";
			padding: 5px;
			"""+addStyle+"""
		}"""


		if checked:
			self.setStyleSheet(self.basicStyle + """StyledToggleWidget{background-color: """+self.openBG+""";}""")
		else:
			self.setStyleSheet(self.basicStyle + """StyledToggleWidget{background-color: """+self.closedBG+""";}""")

	def toggle(self):

		#"not" because it still has the previous var (var changes with parent class toggle below)
		if not self.open:
			self.setStyleSheet(self.basicStyle + """StyledToggleWidget{background-color: """+self.openBG+""";}""")
		else:
			self.setStyleSheet(self.basicStyle + """StyledToggleWidget{background-color: """+self.closedBG+""";}""")

		ToggleWidget.toggle(self)




class LongPressLabel(QPushButton):
	longClick = pyqtSignal()
	def __init__(self, text="", parent=None):
		QPushButton.__init__(self, text, parent)

		self.setStyleSheet("""
			LongPressLabel{
				background-color: none;
				color: white;
				border:none;
			}
			""")

		self.setAutoRepeat(True)
		self.setAutoRepeatDelay(5000)
		self.setAutoRepeatInterval(20000)
		self.clicked.connect(self.handleClicked)
		self._state = 0

	def handleClicked(self):
		if self.isDown():
			if self._state == 0:
				self._state = 1
				self.onLongPress()
		elif self._state == 1:
			self._state = 0

	def onLongPress(self):
		self.longClick.emit()
		


class ClickableLabel(QLabel):
	clicked = pyqtSignal()
	def __init__(self, text="", parent=None):
		QLabel.__init__(self, text, parent)

	def mousePressEvent(self, ev):
		Emitter.getInstance("clickSound").emit()
		self.clicked.emit()
		return # dont propagate click to lower widgets
	


class LoadingWidget(QFrame):
	def __init__(self):
		super(LoadingWidget, self).__init__()
		
		self.mainLayout = QVBoxLayout(self)
		self.mainLayout.setSpacing(35)

		loadingTitle = QLabel("Aktion wird durchgeführt.")
		loadingSubTitle = QLabel("Bitte haben Sie etwas Geduld.")

		loadingTitle.setFont(style.Fonts.loadingTitle)
		loadingSubTitle.setFont(style.Fonts.loadingSubTitle)
		loadingTitle.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
		loadingSubTitle.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

		self.mainLayout.addItem(style.Layout.vSpacer)
		self.mainLayout.addWidget(loadingTitle)		
		self.mainLayout.addWidget(loadingSubTitle)
		self.mainLayout.addItem(style.Layout.vSpacer)
		self.mainLayout.addItem(style.Layout.vSpacer)

		self.setStyleSheet("""
			QFrame{
				background-color: """ + style.Color.scheme.loadingScreenBackground + """;
			}""")