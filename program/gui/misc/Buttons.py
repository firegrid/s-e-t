from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import style

from gui.misc.Emitters import Emitter

class AppButton(QPushButton):
	def __init__(self, text="", parent=None):
		QPushButton.__init__(self, text, parent)

		self.basicStyle = """
		AppButton{
			border-radius: 10px;
			padding: 20px;
			background-color: """+style.Color.scheme.appButtonBackground+""";
			color: """+style.Color.scheme.appButtonColor+""";
		}
		AppButton:pressed{
			background-color: """+style.Color.scheme.appButtonPressedBackground+""";
		}
		"""

		self.setGraphicsEffect(style.Effects.shadow())

		self.setFont(style.Fonts.h2)

		self.setStyleSheet(self.basicStyle)

	def setNormal(self):
		self.setStyleSheet(self.basicStyle)

	def setStyle(self, style):
		self.basicStyle = self.basicStyle + """
		AppButton{
			""" + style + """
		}
		"""
		self.setNormal()

	def mousePressEvent(self,event):
		Emitter.getInstance("clickSound").emit()
		QPushButton.mousePressEvent(self,event)

class ImportantButton(AppButton):
	def __init__(self, text, parent=None):
		AppButton.__init__(self, text, parent)

		self.basicStyle = self.basicStyle + """
		AppButton{
			background-color: """+style.Color.scheme.appButtonImportantBackground+""";
			color: """+style.Color.scheme.appButtonImportantColor+""";
		}
		"""

		self.setNormal()

		self.animation = BorderPulse(self.animationUpdate, 1500, 1.0, 0.3)

	def stopAnimation(self):
		self.animation.stopAnimation()
		self.setNormal()

	def startAnimation(self):
		self.animation.startAnimation()

	def animationUpdate(self):
		self.setStyleSheet(self.basicStyle + """AppButton{""" + self.animation.animationStyle + """}""")


	
from gui.misc.Animations import BackgroundPulse, BorderPulse

class RequiredButton(AppButton):
	def __init__(self, text, parent=None):
		AppButton.__init__(self, text, parent)

		self.animation = BorderPulse(self.animationUpdate, 1500, 1.0, 0.3)
		self.animation.startAnimation()

	def startAnimation(self):
		self.animation.startAnimation()

	def stopAnimation(self):
		self.animation.stopAnimation()
		self.setNormal()

	def animationUpdate(self):
		self.setStyleSheet(self.basicStyle + """AppButton{""" + self.animation.animationStyle + """}""")

class OptionalButton(AppButton):
	def __init__(self, text, parent=None):
		AppButton.__init__(self, text, parent)

		self.animation = BorderPulse(self.animationUpdate, 1500, 1.0, 0.3, "51, 133, 46") # green
		self.animation.startAnimation()

	def startAnimation(self):
		self.animation.startAnimation()

	def stopAnimation(self):
		self.animation.stopAnimation()
		self.setNormal()

	def animationUpdate(self):
		self.setStyleSheet(self.basicStyle + """AppButton{""" + self.animation.animationStyle + """}""")

import settings
import os
from ressources import ressources

class PictureButton(AppButton):
	def __init__(self, pictureFile, specificNoPicPath="", parent=None):
		AppButton.__init__(self, "", parent)

		pixmap = QtGui.QPixmap(pictureFile)
	
		if pixmap.isNull():
			pixmap = QtGui.QPixmap(":/" + settings.Settings.get("Application", "imagePath") + pictureFile)

		if pixmap.isNull():
			pixmap = QtGui.QPixmap(":/media/images/firegrid_logo.png")

			
		self.icon = QIcon(pixmap)
		self.setIcon(self.icon)
		self.setIconSize(pixmap.rect().size())

		self.setStyleSheet(self.basicStyle + """AppButton{padding: 5px;}""")



class ToggleButton(AppButton):
	def __init__(self, text, checked = False, exclusive = True, pressedBG=None, normalBG=None, parent=None):
		AppButton.__init__(self, text, parent)
		
		if pressedBG == None:
			pressedBG = style.Color.scheme.buttonPressedColor

		if normalBG == None:
			normalBG = style.Color.scheme.appButtonBackground
		
		self.setAutoExclusive(exclusive)
		self.setCheckable(True)
		self.setChecked(checked)

		self.basicStyle = self.basicStyle + """
		AppButton:checked{
			background-color: """+pressedBG+""";
			color: """ + style.Color.scheme.foregroundColor + """;
		}
		AppButton{
			background-color: """+normalBG+""";
			color: """ + style.Color.scheme.foregroundColor + """;
		}
		"""

		self.setNormal()

# TODO deprecated??
class DescriptiveButtonBack(QWidget):
	clicked = pyqtSignal()

	def __init__(self, description, buttonText, required = False, parent=None):
		QWidget.__init__(self, parent)

		self.buttonText = buttonText

		self.mainLayout = QHBoxLayout()
		self.mainLayout.setSpacing(0)

		self.description = QLabel(description)
		self.description.setFont(style.Fonts.h3)
		self.description.setContentsMargins(0,0,0,0)

		self.mainLayout.addWidget(self.description)

		if required:
			self.button = RequiredButton(self.buttonText)
		else:
			self.button = ImportantButton(self.buttonText)

		self.button.clicked.connect(lambda:self.clicked.emit())
		self.mainLayout.addWidget(self.button)

		self.setLayout(self.mainLayout)

class DescriptiveButton(QWidget):
	clicked = pyqtSignal()

	def __init__(self, description, buttonText, required = False, model=None, layout=0, descsize=0, parent=None):
		QWidget.__init__(self, parent)
		self.model = model
		self.buttonText = buttonText
		if layout == 0:
			self.mainLayout = QVBoxLayout()
		else:
			self.mainLayout = QHBoxLayout()


		self.mainLayout.setSpacing(0)
		self.mainLayout.setContentsMargins(0,0,0,10)

		self.description = QLabel(description)
		if descsize == 0:
			self.description.setFont(style.Fonts.h4)
		else:
			self.description.setFont(style.Fonts.h2)

		self.description.setContentsMargins(0,0,0,3)

		self.mainLayout.addWidget(self.description)
		if layout == 1:
			self.mainLayout.addItem(style.Layout.hSpacer)

		if required:
			self.button = RequiredButton(self.buttonText)
		else:
			self.button = ImportantButton(self.buttonText)

		self.button.clicked.connect(lambda:self.clicked.emit())
		self.mainLayout.addWidget(self.button)

		self.setLayout(self.mainLayout)



