from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import style
from gui.misc.Buttons import AppButton, ImportantButton


class AppBox(QMessageBox):
	def __init__(self):
		QMessageBox.__init__(self)
		self.setFocusPolicy(Qt.StrongFocus)

	def display(self, title, message):
		self.setWindowTitle(title)
		self.setText(message)

		timer = QTimer(self)
		timer.timeout.connect(self.keepOnTop)
		timer.start(100)

		AppBox.exec_(self)

		timer.stop()

	def keepOnTop(self):
		self.raise_()
		self.activateWindow()
	

class AppQuestionBox(AppBox):
	def __init__(self):
		AppBox.__init__(self)

		self.setIcon(AppBox.Question)

		self.yesAnswer = AppButton("Ja")
		self.addButton(self.yesAnswer, AppBox.YesRole)
		self.noAnswer = ImportantButton("Nein")
		self.addButton(self.noAnswer, AppBox.NoRole)
		self.setDefaultButton(self.noAnswer)

	def display(self, title, message):
		super().display(title, message)
		return self.clickedButton() == self.yesAnswer

class AppWarningBox(AppBox):
	def __init__(self):
		AppBox.__init__(self)

		self.setIcon(AppBox.Warning)

		self.okAnswer = AppButton("Ok")
		self.addButton(self.okAnswer, AppBox.YesRole)
		self.cancelAnswer = AppButton("Abbrechen")
		self.addButton(self.cancelAnswer, AppBox.NoRole)

	def display(self, title, message):
		super().display(title, message)
		return self.clickedButton() == self.okAnswer

class AppCriticalBox(AppBox):
	def __init__(self):
		AppBox.__init__(self)

		self.setIcon(AppBox.Critical)

		self.okAnswer = AppButton("Ok")
		self.addButton(self.okAnswer, AppBox.YesRole)

	def display(self, title, message):
		super().display(title, message)
		return self.clickedButton() == self.okAnswer



class AppDialogBox:
	questionBox = None
	warningBox = None
	criticalBox = None

	def init():
		AppDialogBox.questionBox = AppQuestionBox()
		AppDialogBox.warningBox = AppWarningBox()
		AppDialogBox.criticalBox = AppCriticalBox()
		
	def question(title, message):
		return AppDialogBox.questionBox.display(title, message)

	def warning(title, message):
		return AppDialogBox.warningBox.display(title, message)

	def critical(title, message):
		return AppDialogBox.criticalBox.display(title, message)