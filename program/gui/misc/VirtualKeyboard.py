# -*- coding: utf-8 -*-
#source: http://forge.doumenc.org/svn/pyqtradio/trunk/gui/compact/widgets/virtualKeyboard.py

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import style
import settings

from gui.misc.Buttons import AppButton

from gui.misc.Forms import MyTextEdit

class Case:
	LOWER = 1
	UPPER = 2
	NUM = 0


class KeyButton(AppButton):
	def __init__(self, key):
		super(KeyButton, self).__init__()

		self.key = key

		self.setFont(style.Fonts.h1)

	# def sizeHint(self):
	#     return QSize(70, 55)

class VirtualKeyboard(QWidget):
	change = pyqtSignal()
	instance = None

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.mainLayout = QVBoxLayout(self)
		self.keyboardView = QWidget()
		self.keyboardLayout = QHBoxLayout()

		self.upperKeyWidget = QWidget()
		self.lowerKeyWidget = QWidget()
		self.numPadWidget = QWidget()
		keysLayoutLower = QGridLayout(self.lowerKeyWidget)
		keysLayoutUpper = QGridLayout(self.upperKeyWidget)
		keysLayoutNumpad = QGridLayout(self.numPadWidget)

		self.controlButtonLayout = QVBoxLayout()
		self.controlButtonLayout.addItem(style.Layout.vSpacer)

		self.field = None

		self.keyListByLinesNumber = [
					['7', '8', '9'],
					['4', '5', '6'],
					['1', '2', '3'],
					['0']
		]

		self.keyListByLinesLower = [
					['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'ẞ'],
					['q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'ü', '+'],
					['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ö', 'ä', '#'],
					['y', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '-'],
					[' ']
				]
		self.keyListByLinesUpper = [
					['!', '"', '§', '$', '%', '&&', '/', '(', ')', '=', '?'],
					['Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O', 'P', 'Ü', '*'],
					['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Ö', 'Ä', "'"],
					['Y', 'X', 'C', 'V', 'B', 'N', 'M', ';', ':', '_'],
					[' ']
				]

		self.inputString = ""
		self.cursorPosition = 0
		
		self.state = Case.UPPER

		self.stateButton = AppButton('A/a')
		self.stateButton.setFont(style.Fonts.h2)
		self.numButton = AppButton('NUM')
		self.numButton.setFont(style.Fonts.h2)
		self.backButton = AppButton('Löschen')
		self.backButton.setFont(style.Fonts.h2)
		self.delAllButton = AppButton('Alles\nLöschen')
		self.delAllButton.setFont(style.Fonts.h2)

		self.messageLine = MyTextEdit()
		self.messageLine.cursorPositionChanged.connect(self.cursorPosChanged)
		self.messageLine.setFont(style.Fonts.h1)
		self.messageLine.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed))
		self.messageLine.setBaseSize(QSize(300, 60))

		self.mainLayout.addWidget(self.messageLine)

		self.createKeyboard(keysLayoutNumpad, self.keyListByLinesNumber)
		self.createKeyboard(keysLayoutLower, self.keyListByLinesLower)
		self.createKeyboard(keysLayoutUpper, self.keyListByLinesUpper)

		self.stateButton.clicked.connect(self.switchState)
		self.numButton.clicked.connect(self.switchNum)
		self.backButton.clicked.connect(self.backspace)
		self.delAllButton.clicked.connect(self.deleteAll)

		self.controlButtonLayout.addWidget(self.delAllButton)
		self.controlButtonLayout.addWidget(self.backButton)
		self.controlButtonLayout.addWidget(self.stateButton)
		self.controlButtonLayout.addWidget(self.numButton)

		self.keyBoard = QStackedWidget()
		self.keyBoard.insertWidget(Case.NUM, self.numPadWidget)
		self.keyBoard.insertWidget(Case.LOWER, self.lowerKeyWidget)
		self.keyBoard.insertWidget(Case.UPPER, self.upperKeyWidget)
		self.keyBoard.setCurrentIndex(self.state)


		self.keyboardLayout.addWidget(self.keyBoard)
		self.controlButtonLayout.addItem(style.Layout.vSpacer)
		self.keyboardLayout.addLayout(self.controlButtonLayout)
		self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))

		self.keyboardView.setLayout(self.keyboardLayout)


		self.mainLayout.addWidget(self.keyboardView)

		self.setLayout(self.mainLayout)

	def getInstance():
		if VirtualKeyboard.instance == None:
			VirtualKeyboard.instance = VirtualKeyboard()
		return VirtualKeyboard.instance

	def showNumPad(self):
		self.state = Case.NUM
		self.keyBoard.setCurrentIndex(self.state)

	def showNormal(self):
		self.state = Case.UPPER
		self.keyBoard.setCurrentIndex(self.state)

	def createKeyboard(self, layout, data):
		for lineIndex, line in enumerate(data):
			lastLine = (lineIndex+1 == data.__len__())
			if lastLine:
				layout.addItem(style.Layout.vSpacer, lineIndex+1, 0)

			for keyIndex, key in enumerate(line):
				buttonName = "keyButton" + key
				self.__setattr__(buttonName, KeyButton(key))
				if lastLine:
					if key == " ":
						layout.addWidget(self.getButtonByKey(key), data.index(line)+1, 3, 1, 5)
					if key == "0":
						layout.addWidget(self.getButtonByKey(key), data.index(line)+1, 1)

				else:
					layout.addWidget(self.getButtonByKey(key), data.index(line)+1, line.index(key))
				self.getButtonByKey(key).setText(key)
				self.getButtonByKey(key).clicked.connect(self.addInputByKey)


	def connectToTextField(self, field):
		self.field = field
		if isinstance(field, QTextEdit):
			self.inputString = field.toPlainText()
		else: #QLineEdit
			self.inputString = field.text()

		self.messageLine.setText(self.inputString)
		self.cursorPosition = self.inputString.__len__()

	def sync(self):
		self.inputString = self.messageLine.toPlainText()
		self.field.setText(self.inputString)

	def getButtonByKey(self, key):
		return getattr(self, "keyButton" + key)

	def getLineForButtonByKey(self, key):
		return [key in keyList for keyList in self.keyListByLines].index(True)

	def switchNum(self):
		if self.state == Case.NUM:
			self.state = Case.UPPER
		else:
			self.state = Case.NUM

		self.keyBoard.setCurrentIndex(self.state)

	def switchState(self):
		if self.state == Case.UPPER:
			self.state = Case.LOWER
		else:
			self.state = Case.UPPER
		self.keyBoard.setCurrentIndex(self.state)

	def cursorPosChanged(self):
		self.cursorPosition = self.messageLine.textCursor().position()

	def addInputByKey(self):
		self.sync()
		key = ''
		if self.sender().key == '&&':
			key = '&'
		else:
			key = self.sender().key

		self.inputString = self.inputString[:self.cursorPosition] + key + self.inputString[self.cursorPosition:]
		tempCursor = self.cursorPosition + 1

		self.field.setText(self.inputString)
		self.messageLine.setText(self.inputString)

		self.cursorPosition = tempCursor

		if self.state == Case.UPPER:
			self.switchState()
		self.change.emit()


	def backspace(self):
		self.inputString = self.inputString[:self.cursorPosition -1] + self.inputString[self.cursorPosition:]
		tempCursor = self.cursorPosition - 1

		self.field.setText(self.inputString)
		self.messageLine.setText(self.inputString)
		self.cursorPosition = tempCursor
		self.change.emit()

	def deleteAll(self):
		self.inputString = ""
		self.field.setText(self.inputString)
		self.messageLine.setText(self.inputString)
		self.change.emit()

