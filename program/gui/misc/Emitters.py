from PyQt5.QtCore import QObject, pyqtSignal


class Emitter(QObject):
	trigger = pyqtSignal()

	instance = {}

	def getInstance(inst):
		if inst not in Emitter.instance:
			Emitter.instance[inst] = Emitter()

		return Emitter.instance[inst]

	def emit(self):
		self.trigger.emit()

	def connect(self, slot):
		self.trigger.connect(slot)

	def disconnect(self, slot):
		try:
			self.trigger.disconnect(slot)
		except TypeError:
			# print("not connected")
			pass


from models.base import BaseModel

class ModelEmitter(Emitter):
	trigger = pyqtSignal(BaseModel)

	instance = {}

	def getInstance(inst):
		if inst not in ModelEmitter.instance:
			ModelEmitter.instance[inst] = ModelEmitter()

		return ModelEmitter.instance[inst]

	def emit(self, m):
		self.trigger.emit(m)

class ObjectEmitter(Emitter):
	trigger = pyqtSignal(QObject)

	instance = {}

	def getInstance(inst):
		if inst not in ObjectEmitter.instance:
			ObjectEmitter.instance[inst] = ObjectEmitter()

		return ObjectEmitter.instance[inst]

	def emit(self, o):
		self.trigger.emit(o)



class StringEmitter(Emitter):
	trigger = pyqtSignal(str)

	instance = {}

	def getInstance(inst):
		if inst not in StringEmitter.instance:
			StringEmitter.instance[inst] = StringEmitter()

		return StringEmitter.instance[inst]

	def emit(self, s):
		self.trigger.emit(s)


class IntegerEmitter(Emitter):
	trigger = pyqtSignal(int)

	instance = {}

	def getInstance(inst):
		if inst not in IntegerEmitter.instance:
			IntegerEmitter.instance[inst] = IntegerEmitter()

		return IntegerEmitter.instance[inst]

	def emit(self, s):
		self.trigger.emit(s)



