from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import style
from ressources import ressources

from gui.misc.Emitters import ObjectEmitter, Emitter

class TouchCheckBox(QCheckBox):
	def __init__(self, parent=None):
		QCheckBox.__init__(self, parent)

		self.basicStyle = """
			TouchCheckBox:indicator{
				width: 100px;
				height: 100px;
			}
			TouchCheckBox::indicator:checked {
				border-image: url(:/media/images/misc/activate.png) 0 0 0 0 stretch stretch;
			}
			TouchCheckBox::indicator:unchecked {
				border-image: url(:/media/images/misc/deactivate.png) 0 0 0 0 stretch stretch;
			}
		"""
		self.setStyleSheet(self.basicStyle)

	def mousePressEvent(self,event):
		Emitter.getInstance("clickSound").emit()
		super(TouchCheckBox).mousePressEvent(event)



class MyTextEdit(QTextEdit):
	def __init__(self, parent=None):
		QTextEdit.__init__(self, parent)
		self.basicStyle = """
			MyTextEdit{
				background-color: """ + style.Color.scheme.textFieldBackground + """;
				color: """ + style.Color.scheme.textFieldColor + """;
				border-width: 1px;
				border-style: solid;
				border-radius: 15px;
				border-color: """ + style.Color.scheme.textFieldBorder +""";
				padding: 5px;
				}
			"""
		self.setStyleSheet(self.basicStyle)
	def mousePressEvent(self,event):
		Emitter.getInstance("clickSound").emit()

class MyLineEdit(QLineEdit):
	def __init__(self, parent=None):
		QLineEdit.__init__(self, parent)
		self.basicStyle = """
			MyLineEdit{
				background-color: """ + style.Color.scheme.textLineBackground + """;
				color: """ + style.Color.scheme.textLineColor + """;
				border-width: 1px;
				border-style: solid;
				border-radius: 5px;
				border-color: """ + style.Color.scheme.textLineBorder +""";
				padding: 5px;
				}
			"""
		self.setStyleSheet(self.basicStyle)
	def mousePressEvent(self,event):
		Emitter.getInstance("clickSound").emit()

class ClickableTextEdit(MyTextEdit):
	clicked = pyqtSignal()

	def __init__(self, parent=None):
		MyTextEdit.__init__(self, parent)

	def mousePressEvent(self, event):
		self.clicked.emit()
		super(ClickableTextEdit, self).mousePressEvent(event)


class ClickableLineEdit(MyLineEdit):
	clicked = pyqtSignal()

	def __init__(self, parent=None):
		MyLineEdit.__init__(self, parent)

	def mousePressEvent(self, event):
		self.clicked.emit()
		super(ClickableLineEdit, self).mousePressEvent(event)


class TouchTextEdit(ClickableTextEdit):
	okSignal = pyqtSignal()
	backSignal = pyqtSignal()

	def __init__(self, parent=None):
		ClickableTextEdit.__init__(self, parent)

		super(TouchTextEdit, self).clicked.connect(self.raiseKeyboard)

	def raiseKeyboard(self):
		Emitter.getInstance("KeyboardOk").connect(self.ok)
		Emitter.getInstance("KeyboardBack").connect(self.back)
		ObjectEmitter.getInstance("raiseKeyboard").emit(self)

	def ok(self):
		Emitter.getInstance("KeyboardOk").disconnect(self.ok)
		Emitter.getInstance("KeyboardBack").disconnect(self.back)
		self.okSignal.emit()

	def back(self):
		self.setText("") #todo check if problems
		Emitter.getInstance("KeyboardOk").disconnect(self.ok)
		Emitter.getInstance("KeyboardBack").disconnect(self.back)
		self.backSignal.emit()



class TouchLineEdit(ClickableLineEdit):
	okSignal = pyqtSignal()
	backSignal = pyqtSignal()

	def __init__(self, parent=None):
		ClickableLineEdit.__init__(self, parent)

		super(TouchLineEdit, self).clicked.connect(self.raiseKeyboard)

	def raiseKeyboard(self):
		Emitter.getInstance("KeyboardOk").connect(self.ok)
		Emitter.getInstance("KeyboardBack").connect(self.back)
		ObjectEmitter.getInstance("raiseKeyboard").emit(self)

	def ok(self):
		Emitter.getInstance("KeyboardOk").disconnect(self.ok)
		Emitter.getInstance("KeyboardBack").disconnect(self.back)
		self.okSignal.emit()

	def back(self):
		self.setText("")
		Emitter.getInstance("KeyboardOk").disconnect(self.ok)
		Emitter.getInstance("KeyboardBack").disconnect(self.back)
		self.backSignal.emit()


class TouchSpinBox(QSpinBox):
	maximumReached = pyqtSignal()
	minimumReached = pyqtSignal()
	def __init__(self, parent=None):
		QSpinBox.__init__(self, parent)

		self.setMaximumWidth(150)
		self.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

		self.findChild(QLineEdit).setReadOnly(True)

		self.setStyleSheet("""
			QSpinBox{
				padding: 100px 0;
				font-size: 40px;
			}
			QSpinBox::down-button {
				subcontrol-position: bottom;
				width: 120px;
				height: 90px;
				background-image: url(:/media/images/misc/arrow_down.png);
				background-repeat: no-repeat;
				background-position: center;
				background-color: """ + style.Color.scheme.appButtonBackground + """;
				border-radius: 15px;
			}
			QSpinBox::up-button {
				subcontrol-position: top;
				width: 120px;
				height: 90px;
				background-image: url(:/media/images/misc/arrow_up.png);
				background-repeat:no-repeat;
				background-position: center;
				background-color: """ + style.Color.scheme.appButtonBackground + """;
				border-radius: 15px;
			}
			""")


	def mousePressEvent(self, event):
		Emitter.getInstance("clickSound").emit()
		if event.pos().y() > 100: #do i need to change this sometime??
			if self.value() -1 < self.minimum():
				self.setValue(self.maximum())
				self.minimumReached.emit()
				return
		else:
			if self.value() +1 > self.maximum():
				self.setValue(self.minimum())
				self.maximumReached.emit()
				return
		super(TouchSpinBox, self).mousePressEvent(event)

	def incrementValue(self):
		if self.value() +1 > self.maximum():
			self.setValue(self.minimum())
			self.maximumReached.emit()
		else:
			self.setValue(self.value()+1)

	def decrementValue(self):
		if self.value() -1 < self.minimum():
			self.setValue(self.maximum())
			self.minimumReached.emit()
		else:
			self.setValue(self.value()-1)



class TouchComboBox(QComboBox):
	def __init__(self, parent=None):
		QComboBox.__init__(self, parent)
		self.setFont(style.Fonts.h3)
		self.setItemDelegate(QStyledItemDelegate())
		self.setStyleSheet("""
			QComboBox{
				border-radius: 10px;
				padding: 20px;
				background-color: rgba(0,0,0,1.0);
				color: white;
			}
			QComboBox::drop-down {
				width: 0;
				border: none;
			}
			QComboBox QAbstractItemView {
				background-color: black;
				padding: 0px;
				margin: 0px;
				min-height: 0px;
				border-radius: 10px;
			}
			QComboBox QAbstractItemView::item {
				margin: 5px 0;
				padding: 20px;
				color: white;
				background: #111111;
			}
			""")

	# def mousePressEvent(self,event):
	# 	Emitter.getInstance("clickSound").emit()
	# 	QComboBox.mousePressEvent(event)


