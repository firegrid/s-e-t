from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import math

import settings
import style
from general.functions import writeLog

from gui.misc.Layout import ColLayout
from gui.misc.Buttons import ImportantButton, AppButton, RequiredButton, OptionalButton
from gui.misc.Animations import BorderPulse
from gui.misc.VirtualKeyboard import VirtualKeyboard
from gui.misc.Dialogs import AppDialogBox
from gui.misc.Forms import ClickableTextEdit
from gui.misc.Emitters import ObjectEmitter, Emitter

from models.base import Database
from models.tour import TextSnippets




class MessageInputWidget(QWidget):
	okSignal = pyqtSignal()

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.expert = False
		self.selectedMainSnippet = None
		self.selectedComSnippet = None

		self.mainLayout = QVBoxLayout()

		self.stackedWidget = QStackedWidget()

		#setup controls
		self.controlWidget = QWidget()
		self.controlLayout = QHBoxLayout()

		self.controlButton = OptionalButton("Manuelle Eingabe")
		self.controlButton.clicked.connect(self.changeControl)
		self.controlLayout.addWidget(self.controlButton)

		self.okButton = ImportantButton("Übernehmen")
		self.okButton.clicked.connect(self.okSlot)
		self.controlLayout.addWidget(self.okButton)

		self.controlWidget.setLayout(self.controlLayout)



		#setup snippet view
		self.snippetView = QWidget()
		snippetLayout = QHBoxLayout(self.snippetView)

		self.mainSnippetClicked = ObjectEmitter.getInstance("snippetClicked1")
		self.mainSnippetClicked.connect(self.mainSnippetClick)

		self.comSnippetClicked = ObjectEmitter.getInstance("snippetClicked2")
		self.comSnippetClicked.connect(self.comSnippetClick)

		Database.connectDB()
		mainSnippetList = [sn for sn in TextSnippets.select().where(TextSnippets.group>10, TextSnippets.active == True).order_by(TextSnippets.title.asc())]
		self.mainSnippets = ColLayout(2, mainSnippetList, TextSnippetItem)

		comSnippetList = [sn for sn in TextSnippets.select().where(TextSnippets.group<=10, TextSnippets.active == True)]
		self.comSnippets = ColLayout(1, comSnippetList, TextSnippetItem)
		
		Database.closeDB()

		snippetLayout.addWidget(self.mainSnippets)
		snippetLayout.addWidget(self.comSnippets)

	
		self.stackedWidget.insertWidget(0, self.snippetView)
			

		#setup keyboard view
		self.keyboardView = QWidget()
		self.keyboardLayout = QVBoxLayout(self.keyboardView)

		VirtualKeyboard.getInstance()

		self.stackedWidget.insertWidget(1, self.keyboardView)

		self.stackedWidget.setCurrentIndex(0)

		self.mainLayout.addWidget(self.stackedWidget)
		self.mainLayout.addWidget(self.controlWidget)

		self.setLayout(self.mainLayout)

	def connectToTextField(self, field):
		self.inputField = field
		kv = VirtualKeyboard.getInstance()
		if(self.keyboardLayout.count() == 0):
			self.keyboardLayout.insertWidget(0, kv)
		kv.showNormal()
		try: kv.change.disconnect() 
		except Exception: pass
		kv.change.connect(self.checkButtonAnimation)
		kv.connectToTextField(field)

	def changeControl(self):
		self.expert = not self.expert

		if self.expert:
			self.controlButton.setText("Text Vorlagen")
			self.stackedWidget.setCurrentIndex(1)
		else:
			self.controlButton.setText("Manuelle Eingabe")
			self.stackedWidget.setCurrentIndex(0)

	def comSnippetClick(self, sItem):
		if self.selectedComSnippet != None:
			if self.selectedComSnippet.snippet.identifier != sItem.snippet.identifier:
				self.selectedComSnippet.deactivate()
				self.selectedComSnippet = sItem
			else:
				self.selectedComSnippet = None
				self.checkButtonAnimation()
		else:
			self.selectedComSnippet = sItem
		
		self.checkButtonAnimation()


	def mainSnippetClick(self, sItem):
		if self.selectedMainSnippet != None:
			if self.selectedMainSnippet.snippet.identifier != sItem.snippet.identifier:
				self.selectedMainSnippet.deactivate()
				self.selectedMainSnippet = sItem
			else:
				self.selectedMainSnippet = None
		else:
			self.selectedMainSnippet = sItem

		self.checkButtonAnimation()

	def checkButtonAnimation(self):
		if self.expert and self.keyboardView.findChild(VirtualKeyboard).messageLine.toPlainText().__len__() > 0:
			self.okButton.startAnimation()
			self.controlButton.stopAnimation()
		elif not self.expert and self.selectedMainSnippet != None and self.selectedComSnippet != None:
			self.inputField.inputSnippets([self.selectedMainSnippet.snippet, self.selectedComSnippet.snippet])
			self.okButton.startAnimation()
			self.controlButton.stopAnimation()
		else:
			self.okButton.stopAnimation()
			self.controlButton.startAnimation()
			

	def okSlot(self):
		if self.expert == False:
			if self.selectedMainSnippet == None or self.selectedComSnippet == None:
				answer = AppDialogBox.question("Ein Feld wurde nicht ausgewählt.", "Es muss sowohl ein Grund, als auch eine Erreichbarkeit ausgewählt sein, um fortzufahren.\n\nWollen Sie trotzdem zurück und die Zusatzinformationen zunächst leer lassen?")
				if answer:
					self.okSignal.emit()
				return
			self.inputField.inputSnippets([self.selectedMainSnippet.snippet, self.selectedComSnippet.snippet])
		else:
			self.inputField.inputMessage(self.inputField.toPlainText())
		self.okSignal.emit()

	def emptySelection(self):
		if self.selectedMainSnippet != None:
			self.selectedMainSnippet.deactivate()
		if self.selectedComSnippet != None:
			self.selectedComSnippet.deactivate()

		self.selectedMainSnippet = None
		self.selectedComSnippet = None

		Emitter.getInstance("snippetReset").emit()

		self.stackedWidget.setCurrentIndex(0)
		self.expert = False
		# self.keyboardView.findChild(VirtualKeyboard).deleteAll() # neccessary?? # doesnt look so but i keep it here if any errors should decide to show

		self.okButton.stopAnimation()
		self.controlButton.startAnimation()



class ManualInputWidget(QWidget):

	def __init__(self, type = 1, parent=None):
		QWidget.__init__(self, parent)

		self.mainLayout = QVBoxLayout()
		self.type = type

		#setup controls
		self.controlWidget = QWidget()
		self.controlLayout = QHBoxLayout()

		self.backButton = RequiredButton("Zurück")
		self.backButton.clicked.connect(self.backSlot)
		self.controlLayout.addWidget(self.backButton)

		self.okButton = ImportantButton("Eingeben")
		self.okButton.clicked.connect(self.okSlot)
		self.controlLayout.addWidget(self.okButton)

		self.controlWidget.setLayout(self.controlLayout)

		#setup keyboard view
		self.keyboardView = QWidget()
		self.keyboardLayout = QVBoxLayout(self.keyboardView)

		self.mainLayout.addWidget(self.keyboardView)
		self.mainLayout.addWidget(self.controlWidget)

		self.setLayout(self.mainLayout)

	def connectToTextField(self, field):
		self.inputField = field
		kv = VirtualKeyboard.getInstance()
		if(self.keyboardLayout.count() == 0):
			self.keyboardLayout.insertWidget(0, kv)
		if self.type == 1:
			kv.showNormal()
		elif self.type == 2:
			kv.showNumPad()
		try: kv.change.disconnect() 
		except Exception: pass
		kv.connectToTextField(field)

	
	def okSlot(self):
		kv = VirtualKeyboard.getInstance()
		kv.sync()
		Emitter.getInstance("KeyboardOk").emit()

	def backSlot(self):
		Emitter.getInstance("KeyboardBack").emit()






class TextSnippetItem(QWidget):
	clicked = pyqtSignal()
	def __init__(self, snippet, parent=None):
		QWidget.__init__(self, parent)

		self.active = snippet.default
		self.snippet = snippet

		self.mainLayout = QHBoxLayout(self)
		self.mainLayout.setContentsMargins(0,0,0,0)
		self.mainLayout.setSpacing(0)

		self.mainLayout.addWidget(QLabel(snippet.message))

		self.updateStyle()

		self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed))

		self.snippetReset = Emitter.getInstance("snippetReset").connect(self.reset)

		if snippet.default:
			if self.snippet.group > 10:
				g = 1
			else:
				g = 2
			ObjectEmitter.getInstance("snippetClicked" + str(g)).emit(self)
		


	def mousePressEvent(self, event):
		Emitter.getInstance("clickSound").emit()
		self.active = not self.active

		self.updateStyle()

		self.clicked.emit()

		if self.snippet.group > 10:
			g = 1
		else:
			g = 2

		ObjectEmitter.getInstance("snippetClicked" + str(g)).emit(self)

		super(TextSnippetItem, self).mousePressEvent(event)

	def updateStyle(self):
		if self.active:
			self.setStyleSheet("""
				background-color: """ + style.Color.scheme.activatedTextSnippet + """;
				border-radius: 5px;
			""")
		else:
			self.setStyleSheet("""
				background-color: """ + style.Color.scheme.deactivatedTextSnippet + """;
				border-radius: 5px;
			""")

	def deactivate(self):
		self.active = False
		self.updateStyle()

	def activate(self):
		self.active = True
		self.updateStyle()

	def reset(self):
		if self.active:
			self.deactivate()

		if self.snippet.default:
			self.activate()
			if self.snippet.group > 10:
				g = 1
			else:
				g = 2
			ObjectEmitter.getInstance("snippetClicked" + str(g)).emit(self)
	
	def sizeHint(self):
		return QSize(300, 45)




class TourMessageBox(ClickableTextEdit):
	readySignal = pyqtSignal()
	def __init__(self, parent=None):
		ClickableTextEdit.__init__(self, parent)

		self.snippets = []
		self.separater = " - "
		self.type = -1

		self.setFont(style.Fonts.h4)

		self.animation = BorderPulse(self.animationUpdate, 1500, 1.0, 0.3)

		self.setPlaceholderText("Zusatzinformation (Pflichtfeld)")#TODO besseres wort
		self.setMaximumHeight(100)
		self.setStyleSheet(self.basicStyle)

		self.startAnimation()

	def animationUpdate(self):
		self.setStyleSheet(self.basicStyle + """ClickableTextEdit{""" + self.animation.animationStyle + """}""")

	def startAnimation(self):
		self.animation.startAnimation()


	def stopAnimation(self):
		self.animation.stopAnimation()
		self.setStyleSheet(self.basicStyle)


	def inputSnippets(self, snippets):
		self.snippets = snippets
		message = ""
		for sn in self.snippets:
			if sn.group > 10:
				self.type = sn.group
			message = message + sn.message + self.separater

		message = message[:-self.separater.__len__()]
		self.setText(message)
		self.stopAnimation()
		self.readySignal.emit()


	def inputMessage(self, message):
		self.snippets = []
		self.type = 11 #simple Dienstfahrt
		self.setText(message)
		self.stopAnimation()
		self.readySignal.emit()

	def clearWidget(self):
		self.snippets = []
		self.setText("")
		self.type = -1
		self.startAnimation()

	def isFilledOut(self):
		if self.toPlainText().__len__() > 0:
			return True
		else:
			return False




