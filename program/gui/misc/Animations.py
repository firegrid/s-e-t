from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings


class BasicAnimation(QObject):
	def __init__(self, callBack):
		QObject.__init__(self, None)
		self.animationValue = 0.0
		self.animationStyle = ""
		self.animationCallBack = callBack
		self.animation = QSequentialAnimationGroup()

	def startAnimation(self):
		if settings.Settings.get("Window", "animation", "bool"):
			self.animation.start()
		else:
			self.animation.start()
			self.animation.stop()

	def stopAnimation(self):
		if settings.Settings.get("Window", "animation", "bool"):
			self.animation.stop()

	def getAnimationAttribute(self):
		return self.animationValue;

	def setAnimationAttribute(self, animationValue):
		self.animationValue = animationValue
		self.animationCallBack()




class PulseAnimation(BasicAnimation):
	def __init__(self, callBack, duration = 1500, startValue = 0.3, endValue = 1.0):
		BasicAnimation.__init__(self, callBack)
		anim1 = QPropertyAnimation(self, b"AnimationAttribute")
		anim1.setDuration(duration)
		anim1.setStartValue(startValue)
		anim1.setEndValue(endValue)
		
		anim2 = QPropertyAnimation(self, b"AnimationAttribute")
		anim2.setDuration(duration)
		anim2.setStartValue(endValue)
		anim2.setEndValue(startValue)

		self.animation.addAnimation(anim1)
		self.animation.addAnimation(anim2)
		self.animation.setLoopCount(-1)



class BackgroundPulse(PulseAnimation):
	def __init__(self, callBack, duration = 1000, startValue = 0.0, endValue = 1.0):
		PulseAnimation.__init__(self, callBack, duration, startValue, endValue)

	def setAnimationAttribute(self, animationValue):
		self.animationStyle = """
			background-color: rgba(240, 50, 50, """ + str(animationValue) + """);
		"""
		BasicAnimation.setAnimationAttribute(self, animationValue)

	AnimationAttribute = pyqtProperty(float, BasicAnimation.getAnimationAttribute, setAnimationAttribute)


class BorderPulse(PulseAnimation):
	def __init__(self, callBack, duration = 1000, startValue = 0.0, endValue = 1.0, color = "240, 50, 50"):
		PulseAnimation.__init__(self, callBack, duration, startValue, endValue)
		self.color = color

	def setAnimationAttribute(self, animationValue):
		self.animationStyle = """
			border-width: 2px;
			border-style: solid;
			border-color: rgba(""" + self.color + """, """ + str(animationValue) + """);
		"""
		BasicAnimation.setAnimationAttribute(self, animationValue)

	AnimationAttribute = pyqtProperty(float, BasicAnimation.getAnimationAttribute, setAnimationAttribute)
