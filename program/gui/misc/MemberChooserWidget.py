from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import math

import settings
import style
from general.functions import writeLog

from models.base import Database
from models.member import Member, VehiclePermission
from models.vehicle import Vehicle

from gui.misc.Dialogs import AppDialogBox
from gui.misc.Emitters import ModelEmitter, Emitter


class MemberChooserWidget(QWidget):
	#TODO add keyboard for number input (not choose from list)
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.colCount = settings.Settings.get("Layout", "memberColumns", "int")

		self.mainLayout = QVBoxLayout()
		self.mainLayout.setContentsMargins(0,0,0,0)
		self.mainLayout.setSpacing(0)

		self.stackedWidget = QStackedWidget()
		self.stackedWidget.setContentsMargins(0,0,0,0)
		
		Database.connectDB()

		self.memberList = [m for m in Member.select().where(Member.status << [1]).order_by(Member.stbn.asc())]
		
		memberLen = self.memberList.__len__()
		self.oneColumn = math.ceil(memberLen / self.colCount)

		self.vehicleStackeWidgetDict = {}

		for v in [v for v in Vehicle.select().where(Vehicle.active == True)]:
			self.addList(v)


		Database.closeDB()

		self.addList()
		
		self.mainLayout.addWidget(self.stackedWidget)


		self.setLayout(self.mainLayout)

	def addList(self, vehicle = None):
		memberAreaWidget = QWidget()
		memberArea = QHBoxLayout()
		memberArea.setContentsMargins(0,0,0,0)

		start = 0

		for colIndex in range(self.colCount):
			colWidget = QWidget()
			col = QVBoxLayout()

			for m in self.memberList[start:start+self.oneColumn]:
				
				canDriveVehicle = True

				if vehicle is not None:
					try:
						VehiclePermission.get(VehiclePermission.member == m.globalIdentifier, VehiclePermission.vehicle == vehicle.identifier)
					except VehiclePermission.DoesNotExist:
						canDriveVehicle = False

				memberItem = MemberItem(m, canDriveVehicle)
				memberItem.clicked.connect(self.memberClick)

				col.addWidget(memberItem)

			col.addItem(style.style.Layout.vSpacer)


			colWidget.setLayout(col)
			memberArea.addWidget(colWidget)
			start = start+self.oneColumn

		memberAreaWidget.setLayout(memberArea)
		if vehicle == None:
			self.vehicleStackeWidgetDict["Normal"] = self.stackedWidget.addWidget(memberAreaWidget)
		else:
			self.vehicleStackeWidgetDict[vehicle.identifier] = self.stackedWidget.addWidget(memberAreaWidget)

	def showMembers(self, vehicle=None):
		if vehicle == None:
			self.stackedWidget.setCurrentIndex(self.vehicleStackeWidgetDict["Normal"])
		else:
			self.stackedWidget.setCurrentIndex(self.vehicleStackeWidgetDict[vehicle.vehicle.model.identifier])

		self.vehicle = vehicle


	def memberClick(self):
		mItem = self.sender()

		if mItem.allowed == False:
			answer = AppDialogBox.question("Keine Fahrerlaubnis", "%s besitzt keine Fahrerlaubnis für dieses Fahrzeug!\n\nBitte wenden Sie sich an den Administrator.\n\nWollen Sie trotzdem diesen Fahrer auswählen?" % (mItem.member.firstname + " " + mItem.member.lastname))
			if not answer:
				return

		
		ModelEmitter.getInstance("MemberChosen").emit(mItem.member)



class MemberItem(QFrame):
	clicked = pyqtSignal()
	def __init__(self, member, permission, parent=None):
		QWidget.__init__(self, parent)

		self.mainLayout = QHBoxLayout(self)
		self.mainLayout.setContentsMargins(0,0,0,0)
		self.member = member
		self.allowed = permission

		self.mainLayout.addWidget(QLabel(str(self.member.stbn)))
		self.mainLayout.addWidget(QLabel(self.member.rank))
		self.mainLayout.addWidget(QLabel(self.member.firstname))
		self.mainLayout.addWidget(QLabel(self.member.lastname))
		self.mainLayout.addItem(style.Layout.hSpacer)

		self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed))

		if self.allowed:
			self.setStyleSheet("""
				MemberItem{
					background-color: """ + style.Color.scheme.allowedMemberColor + """;
					border-radius: 5px;
					margin: 0px;
					padding: 0px 20px;
				}
				""")
		else:
			self.setStyleSheet("""
				MemberItem{
					background-color: """ + style.Color.scheme.notAllowedMemberColor + """;
					border-radius: 5px;
					margin: 0px;
					padding: 0px 20px;
				}
				""")

		self.setLayout(self.mainLayout)

	def sizeHint(self):
		return QSize(300, 45)

	def mousePressEvent(self, event):
		Emitter.getInstance("clickSound").emit()
		self.clicked.emit()
		super(MemberItem, self).mousePressEvent(event)
			

