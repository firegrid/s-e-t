import pyaudio
import wave

from gui.misc.Emitters import Emitter



# loads file into its list
# plays the file when needed

# sound files are preloaded at program start
# sounds can be played anytime during the execution of the program

# pyaudio needs to open a new stream everytime a sound is played
# for improved performance this stream is initialized after the previous audiostream ended.

# various error messages appear on program start... dont really know why but it does work... so...
class Sound():
	instance = {}
	player = None
	chunk = 1024

	def __init__(self, file):
		if Sound.player == None:
			Sound.player = pyaudio.PyAudio()
			Emitter.getInstance("closeApplication").connect(Sound.close)
		
		self.file = wave.open("media/audio/" + file, 'rb')

		self.openStream()
		

	def openStream(self):
		self.stream = Sound.player.open(
			format = Sound.player.get_format_from_width(self.file.getsampwidth()),
			channels = self.file.getnchannels(),
			rate = self.file.getframerate(),
			output = True)
		

	def setInstance(name, file):
		Sound.instance[name] = Sound(file)

	def play(name):
		if name not in Sound.instance:
			print(name + " was not found")
			return

		s = Sound.instance[name]

		soundData = s.file.readframes(Sound.chunk)

		while soundData != '':
			s.stream.write(soundData)
			soundData = s.file.readframes(Sound.chunk)

		# rewind the file for the next time this audio is played
		s.file.rewind()

		#open new stream for the next time the audio is played
		s.openStream()

		

	def close():		
		Sound.player.terminate()
