'''
# CODE BY Lucas Dworschak
# FF Achau
# A-2481 Achau
# lucas.dworschak@gmx.at
'''

from time import strftime

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style
from ressources import ressources


from gui.vehicle.VehicleView import VehicleView
from gui.admin.AdminView import AdminView
from gui.misc.General import PictureWidget
from gui.misc.Dialogs import AppDialogBox

from gui.misc.Emitters import Emitter, IntegerEmitter


class MainWindow(QWidget):
	def __init__(self, parent=None):
		super(MainWindow, self).__init__(parent)

		self.mainLayout = QGridLayout(self)

		self.mainLayout.setSpacing(0)
		self.mainLayout.setContentsMargins(0,0,0,0)

		self.statusBar = StatusBar()
		self.statusBar.needAdminSignal.connect(self.changeToAdmin)
		self.mainLayout.addWidget(self.statusBar, 0, 0)

		self.vehicleView = VehicleView()
		self.adminView = AdminView()

		self.stackedWidget = QStackedWidget()
		self.stackedWidget.insertWidget(0, self.vehicleView)
		self.stackedWidget.insertWidget(1, self.adminView)
		self.mainLayout.addWidget(self.stackedWidget, 1, 0)

		self.stackedWidget.setCurrentIndex(0)

		self.adminView.toVehicleView.connect(self.changeToVehicle)

		self.setWindowTitle("Status-Erfassungs-Terminal" + " - " + settings.Settings.get("Feuerwehr", "name"))
		self.setWindowIcon(QIcon(':/media/images/application.png'))

	def changeToAdmin(self):
		self.adminView.showLoginView()
		self.stackedWidget.setCurrentIndex(1)

	def changeToVehicle(self):
		Emitter.getInstance("ShowNormalView").emit()
		self.stackedWidget.setCurrentIndex(0)


	#inform other modules, that the application is closing
	def closeEvent(self, event):
		Emitter.getInstance("closeApplication").emit()


class StatusBar(QFrame):
	needAdminSignal = pyqtSignal()
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QHBoxLayout(self)
		self.mainLayout.setSpacing(20)
		self.mainLayout.setContentsMargins(25, 15,25,15)
		self.errors = 0

		

		header = QLabel()
		header.setFont(style.Fonts.h0)
		header.setText("Status-Erfassungs-Terminal" + " - " + settings.Settings.get("Feuerwehr", "name"))
		self.mainLayout.addWidget(header)

		if settings.Settings.get("Application", "training", "bool"):
			trainingsLabel = QLabel("!! Trainings Modus !!")
			trainingsLabel.setStyleSheet("""color: red;""")
			trainingsLabel.setFont(style.Fonts.h1)
			trainingsLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
			self.mainLayout.addWidget(trainingsLabel)

		self.timer = QTimer(self)
		self.timer.timeout.connect(lambda:self.clock.setText(strftime("%d.%m.%Y %H:%M:%S")))
		self.timer.start(100)

		self.clock = QLabel(strftime("%H"+":"+"%M"+":"+"%S"))
		self.clock.setFont(style.Fonts.h0)
		self.clock.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

		self.mainLayout.addWidget(self.clock)

		self.fglogo = PictureWidget("firegrid_logo.png")
		self.attention = PictureWidget("misc/attention.png")
		self.fglogo.clicked.connect(self.needAdminSignal.emit)
		self.mainLayout.addWidget(self.attention)
		self.attention.hide()
		self.attention.clicked.connect(self.showError)
		self.mainLayout.addWidget(self.fglogo)

		IntegerEmitter.getInstance("DistributionErrorLength").connect(self.error)

		# for testing events
		# from gui.misc.Emitters import Emitter
		# testbutton = QPushButton("test")
		# testbutton.clicked.connect(lambda:Emitter.getInstance("testing").emit())
		# self.mainLayout.addWidget(testbutton)



		self.setStyleSheet("""
			QFrame{
				background-color: """ + style.Color.scheme.statusBarBackground + """;
			}
			""")


	def error(self, errorNumber):
		self.errors = errorNumber
		if errorNumber > 0:
			self.attention.show()
			self.errors = errorNumber
		else:
			self.attention.hide()



	def showError(self):
		AppDialogBox.warning("Netzwerk Error", str(self.errors) + " Nachrichten konnten nicht übermittelt werden. Grund dafür dürfte eine fehlende Internetverbindung sein.\nDie eingegebenen Nachrichten werden automatisch an die zuständige Stelle übermittelt, sobald der Service wieder bereit steht. Sie müssen also nix mehr machen\nBedenken Sie jedoch dass die gesendeten Daten womöglich nicht an die BAZ gesendet werden konnten.\nFalls diese Fehlermeldung öfters auftaucht, melden Sie sich bitte bei den zuständigen Betreuern dieses Systems.")