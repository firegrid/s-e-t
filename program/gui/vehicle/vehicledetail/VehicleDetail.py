from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style
from general.functions import writeLog

from logic.VehicleDistributionServices.ServiceData.Vehicles import VehicleStatus
from logic.VehicleAdministration import VehicleAdministration as VA

from gui.vehicle.vehicledetail.TourList import TourListWidget
from gui.vehicle.vehicledetail.NewTour import NewTourWidget
from gui.vehicle.vehicledetail.EquipmentScreen import EquipmentScreenWidget
from gui.vehicle.vehicledetail.StatusScreen import StatusScreenWidget

from gui.misc.Emitters import ModelEmitter, Emitter


class VehicleDetailMode:
	TourModus = 0
	NewTourModus = 1
	EquipmentModus = 2
	StatusModus = 3


class VehicleDetail(QWidget):
	def __init__(self, parent=None):
		super(VehicleDetail, self).__init__(parent)
		self.vehicleView = parent

		self.modus = VehicleDetailMode.TourModus

		self.mainLayout = QVBoxLayout()
		self.mainLayout.setContentsMargins(0,0,0,0)
		self.mainLayout.setSpacing(0)

		self.stackedWidget = QStackedWidget()
		self.mainLayout.addWidget(self.stackedWidget)

		self.tourListWidget = TourListWidget()
		self.newTourWidget = NewTourWidget(self.vehicleView)
		self.equipmentScreenWidget = EquipmentScreenWidget()
		self.statusScreenWidget = StatusScreenWidget()
		
		self.stackedWidget.insertWidget(VehicleDetailMode.TourModus, self.tourListWidget)
		self.stackedWidget.insertWidget(VehicleDetailMode.NewTourModus, self.newTourWidget)
		self.stackedWidget.insertWidget(VehicleDetailMode.EquipmentModus, self.equipmentScreenWidget)
		self.stackedWidget.insertWidget(VehicleDetailMode.StatusModus, self.statusScreenWidget)



		ModelEmitter.getInstance("EquipmentScreen").connect(self.showEquipmentScreen)
		ModelEmitter.getInstance("StatusScreen").connect(self.showStatusScreen)


		self.newTourWidget.emptyTourSignal.connect(self.emptyTour)
		self.tourListWidget.changeTourSignal.connect(lambda m:(self.stackedWidget.setCurrentIndex(VehicleDetailMode.NewTourModus),self.newTourWidget.changeExistingTour(m)))

		Emitter.getInstance("ShowNormalView").connect(self.changeDetailScreen)

		self.stackedWidget.setCurrentIndex(VehicleDetailMode.TourModus)

		self.setLayout(self.mainLayout)

	def vehicleClicked(self, clickedVehicle):
		prevStatus = self.modus
		if clickedVehicle.vehicle.selected:
			self.modus = VehicleDetailMode.NewTourModus
		else:
			if VA.instance.vehicles.selectedVehicleList.__len__() == 0:
				self.modus = VehicleDetailMode.TourModus
			else:
				self.modus = VehicleDetailMode.NewTourModus

		self.stackedWidget.setCurrentIndex(self.modus)

		if self.modus == VehicleDetailMode.NewTourModus or (prevStatus == VehicleDetailMode.NewTourModus and self.modus == VehicleDetailMode.TourModus):
			self.newTourWidget.vehicleList.updateVehicleList()

	def emptyTour(self):
		self.tourListWidget.update()
		self.changeDetailScreen()

	def changeDetailScreen(self):		
		if VA.instance.vehicles.selectedVehicleList.__len__() == 0:
			self.modus = VehicleDetailMode.TourModus
		else:
			self.modus = VehicleDetailMode.NewTourModus

		self.stackedWidget.setCurrentIndex(self.modus)

	def showEquipmentScreen(self, model):
		self.modus = VehicleDetailMode.EquipmentModus
		self.equipmentScreenWidget.display(model)
		self.stackedWidget.setCurrentIndex(self.modus)

	def showStatusScreen(self, model):
		self.modus = VehicleDetailMode.StatusModus
		self.statusScreenWidget.display(model)
		self.stackedWidget.setCurrentIndex(self.modus)
