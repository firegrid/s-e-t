from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

from logic.VehicleDistributionServices.ServiceData.Item import Item
from logic.VehicleAdministration import VehicleAdministration as VA

import settings
import style
from general.functions import writeLog
from gui.QtFunctions import *

from gui.misc.Buttons import PictureButton, ToggleButton
from gui.misc.Layout import ScrollableArea
from gui.misc.Emitters import Emitter, ObjectEmitter, ModelEmitter
from gui.misc.Forms import TouchLineEdit
from gui.misc.General import StyledToggleWidget


from models.base import Database
from models.equipment import Equipment

class EquipmentScreenWidget(QWidget):	
	def __init__(self, parent=None):
		super(EquipmentScreenWidget, self).__init__(parent)
		self.mainLayout = QVBoxLayout(self)

		self.currentEquipmentList = []


		self.scroll = ScrollableArea()
		self.equipment = QWidget()
		self.equipmentLayout = QVBoxLayout(self.equipment)

		self.titleLayout = QHBoxLayout()

		self.titleLabel = QLabel("Geräte") #TODO wording
		self.titleLabel.setFont(style.Fonts.h2)
		self.titleLayout.addWidget(self.titleLabel)

		self.titleLayout.addItem(style.Layout.hSpacer)

		self.closeButton = PictureButton(":/media/images/misc/close.png", "misc")
		self.closeButton.setStyleSheet("""
			border-radius: 10px;
			background-color: #000000;
			padding: 10px 20px;
			""")
		self.closeButton.clicked.connect(self.close)
		self.titleLayout.addWidget(self.closeButton)

		self.equipmentList = QVBoxLayout()


		self.equipmentLayout.addLayout(self.titleLayout)
		self.equipmentLayout.addLayout(self.equipmentList)

		self.scroll.setInnerWidget(self.equipment)

		self.mainLayout.addWidget(self.scroll)

	def close(self):
		ModelEmitter.getInstance("MemberChosen").disconnect(self.addTempItem)
		ModelEmitter.getInstance("MemberChosen").disconnect(self.changeEquipment)
		Emitter.getInstance("ShowNormalView").emit()

	def changeEquipmentMode(self):
		self.mode = 2

		self.currentEquipmentId = self.sender().property("id")		

		if self.sender().open == False:

			self.currentEquipmentList.append(self.currentEquipmentId)
			self.sender().closedWidget.setText("")

			self.askResponsiblePerson()
		else:
			if self.currentEquipmentId in self.currentEquipmentList:
				self.currentEquipmentList.remove(self.currentEquipmentId)
				if self.currentEquipmentList.__len__() == 0:
					self.removeAskResponsiblePerson()
			else:
				self.readyEquipment()

			


	#only used to change one equipment status to ready

	def readyEquipment(self):
		item = Item(self.currentEquipmentId, self.model.identifier, "", True, None)
		self.currentEquipmentId = None

		VA.instance.changeItem(item)
		Emitter.getInstance("UpdateVehicleList").emit()

		self.renewCurrentView()

	#only really used for changing (multiple) equipment not ready

	def changeEquipment(self, member=None):
		for e in self.currentEquipmentList:
			item = Item(e, self.model.identifier, "", False, member)

			VA.instance.changeItem(item)
		
		self.currentEquipmentId = None
		self.currentEquipmentList = []

		ModelEmitter.getInstance("MemberChosen").disconnect(self.changeEquipment)

		Emitter.getInstance("UpdateVehicleList").emit()
		Emitter.getInstance("ShowVehicleView").emit()

		self.renewCurrentView()


	def cancelAddTempItem(self):
		Emitter.getInstance("ShowVehicleView").emit()

	def addTempItemMode(self):
		self.mode = 1

		self.askResponsiblePerson()

	def addTempItem(self, member):
		self.tempItem = self.addItemEdit.text()

		item = Item(-1, self.model.identifier, self.tempItem, False, member, True)

		ModelEmitter.getInstance("MemberChosen").disconnect(self.addTempItem)
		VA.instance.addTempItem(item)


		Emitter.getInstance("UpdateVehicleList").emit()
		Emitter.getInstance("ShowVehicleView").emit()


		self.renewView()


	def askResponsiblePerson(self):
		# ask for the person responsible for entry
		ObjectEmitter.getInstance("ShowMemberChooserView").emit(None)

		if self.mode == 1:
			ModelEmitter.getInstance("MemberChosen").connect(self.addTempItem)
		else:
			ModelEmitter.getInstance("MemberChosen").connect(self.changeEquipment)

	def removeAskResponsiblePerson(self):
		Emitter.getInstance("ShowVehicleView").emit()

		if self.mode == 1:
			ModelEmitter.getInstance("MemberChosen").disconnect(self.addTempItem)
		else:
			ModelEmitter.getInstance("MemberChosen").disconnect(self.changeEquipment)




	def display(self, model):
		self.model = model

		self.renewView()

	def renewCurrentView(self):
		for i in range(self.equipmentList.count()):
			item = self.equipmentList.itemAt(i)
			if isinstance(item, QWidgetItem) and isinstance(item.widget(), StyledToggleWidget):
				if not item.widget().open:
					e = Equipment().get(Equipment.identifier == item.widget().property("id"))
					if e.responsiblePerson != None:
						item.widget().closedWidget.setText(e.responsiblePerson.fullFWName())
				elif item.widget().property("temp") == 1:
					# e = Equipment().get(Equipment.identifier == item.widget().property("id"))
					Equipment.delete().where(Equipment.identifier == item.widget().property("id")).execute()
					item.widget().close()
					# print("delete")
					# print(e.name)



		

	def renewView(self):
		clearLayout(self.equipmentList)

		empty = True

		self.addItemEdit = TouchLineEdit()
		self.addItemEdit.setPlaceholderText("Nicht erfasstes Gerät hinzufügen")
		self.addItemEdit.okSignal.connect(self.addTempItemMode)
		self.addItemEdit.backSignal.connect(self.cancelAddTempItem)

		self.equipmentList.addWidget(self.addItemEdit)

		Database.connectDB()
		for e in Equipment.select().where(Equipment.vehicle == self.model.identifier).order_by(Equipment.ready.asc(), Equipment.name.asc()):
			empty = False
			ready = True if e.ready == 1 else False


			permaWidget = QLabel(e.name)
			permaWidget.setFont(style.Fonts.h3)
			permaWidget.setAlignment(Qt.AlignCenter)

			statusContentOpen = None

			# statusContentOpen = QLabel("Einsatzbereit")
			# statusContentOpen.setFont(style.Fonts.bolder)
			# statusContentOpen.setAlignment(Qt.AlignCenter)

			statusContentClosed = QLabel("")
			statusContentClosed.setFont(style.Fonts.bold)
			statusContentClosed.setAlignment(Qt.AlignCenter)

			statusbutton = StyledToggleWidget(permaWidget, statusContentOpen, statusContentClosed, ready, style.Color.scheme.appToggleImportantCheck, style.Color.scheme.appToggleImportantUncheck)
			
			if not ready:
				statusbutton.closedWidget.setText(e.responsiblePerson.fullFWName())

			statusbutton.setProperty("id", e.identifier)
			statusbutton.setProperty("temp", e.temporary)

			statusbutton.clicked.connect(self.changeEquipmentMode)
			self.equipmentList.addWidget(statusbutton)
		Database.closeDB()

		if empty:
			self.equipmentList.addWidget(QLabel("Keine Geräte gefunden."))

		self.equipmentList.addItem(style.Layout.vSpacer)

	