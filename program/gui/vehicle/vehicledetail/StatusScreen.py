from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

from logic.VehicleDistributionServices.ServiceData.VehicleStatus import VehicleStatus
from logic.VehicleAdministration import VehicleAdministration as VA


import settings
import style

from general.functions import writeLog
from gui.QtFunctions import *

from gui.misc.Buttons import PictureButton
from gui.misc.General import StyledToggleWidget
from gui.misc.Layout import ScrollableArea
from gui.misc.Emitters import Emitter

from models.base import Database
from models.vehicle import Vehicle


class StatusScreenWidget(QWidget):	
	def __init__(self, parent=None):
		super(StatusScreenWidget, self).__init__(parent)
		self.mainLayout = QVBoxLayout(self)

		self.scroll = ScrollableArea()
		self.statusWidget = QWidget()
		self.statusLayout = QVBoxLayout(self.statusWidget)

		self.titleLayout = QHBoxLayout()

		self.titleLabel = QLabel("Fahrzeugstatus") #TODO wording
		self.titleLabel.setFont(style.Fonts.h2)
		self.titleLayout.addWidget(self.titleLabel)

		self.titleLayout.addItem(style.Layout.hSpacer)


		self.closeButton = PictureButton(":/media/images/misc/close.png", "misc")
		self.closeButton.setStyleSheet("""
			border-radius: 10px;
			background-color: #000000;
			padding: 10px 20px;
			""")
		self.closeButton.clicked.connect(self.close)
		self.titleLayout.addWidget(self.closeButton)

		self.statustList = QVBoxLayout()


		self.statusLayout.addLayout(self.titleLayout)
		self.statusLayout.addLayout(self.statustList)

		self.scroll.setInnerWidget(self.statusWidget)

		self.mainLayout.addWidget(self.scroll)

	def changeStatus(self):
		QApplication.processEvents()
		status = VehicleStatus(self.model.identifier, self.sender().open)
		VA.instance.changeVehicleStatus(status)

		Emitter.getInstance("UpdateVehicleList").emit()


	def close(self):
		Emitter.getInstance("ShowNormalView").emit()
		


	def display(self, model):
		#renew the current vehicle model (if a status change happened it needs to get the current status..)
		Database.connectDB()
		self.model = model.get(identifier = model.identifier)
		Database.closeDB()

		self.renewView()
		

	def renewView(self):
		clearLayout(self.statustList)

		
		ready = True if self.model.ready == 1 else False
		
		permaWidget = QLabel(self.model.longname)
		permaWidget.setFont(style.Fonts.h2)
		permaWidget.setAlignment(Qt.AlignCenter)

		statusContentOpen = QLabel("Einsatzbereit")
		statusContentOpen.setFont(style.Fonts.bolder)
		statusContentOpen.setAlignment(Qt.AlignCenter)

		statusContentClosed = QLabel("nicht Einsatzbereit")
		statusContentClosed.setFont(style.Fonts.bolder)
		statusContentClosed.setAlignment(Qt.AlignCenter)

		statusbutton = StyledToggleWidget(permaWidget, statusContentOpen, statusContentClosed, ready, style.Color.scheme.appToggleImportantCheck, style.Color.scheme.appToggleImportantUncheck)
		statusbutton.clicked.connect(self.changeStatus)
		self.statustList.addWidget(statusbutton)
		

		self.statustList.addItem(style.Layout.vSpacer)

	