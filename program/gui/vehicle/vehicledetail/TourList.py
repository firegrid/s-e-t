from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import datetime

import settings
import style
from general.functions import writeLog

from models.base import Database
from models.tour import Tour, TourDriver

from gui.misc.Layout import ScrollableArea
from gui.misc.Dialogs import AppDialogBox
from gui.misc.Emitters import Emitter

from logic.VehicleDistributionServices.ServiceData.Mission import Mission




#Widget that shows you the currently active tours and reservations
class TourListWidget(QWidget):
	changeTourSignal = pyqtSignal(Mission)

	def __init__(self, parent=None):
		super(TourListWidget, self).__init__(parent)
		self.mainLayout = QVBoxLayout()

		self.scroll = ScrollableArea()

		self.tourList = QWidget()
		self.tourListLayout = QVBoxLayout(self.tourList)

		sp = QSizePolicy()
		sp.setVerticalPolicy(QSizePolicy.Maximum)
		sp.setHorizontalPolicy(QSizePolicy.Expanding)

		self.activeLabel = QLabel("Aktive Touren:")
		self.activeLabel.setFont(style.Fonts.h1)
		self.activeLabel.setSizePolicy(sp)
		self.tourListLayout.addWidget(self.activeLabel)

		self.activeWidget = QWidget()
		self.activeWidget.setSizePolicy(sp)
		self.activeWidget.setLayout(self.getActiveTours())
		self.tourListLayout.addWidget(self.activeWidget)

		self.reservedLabel = QLabel("Reservierte Touren:")
		self.reservedLabel.setFont(style.Fonts.h1)
		self.reservedLabel.setSizePolicy(sp)
		self.tourListLayout.addWidget(self.reservedLabel)

		self.reservedWidget = QWidget()
		self.reservedWidget.setSizePolicy(sp)
		self.reservedWidget.setLayout(self.getReservedTours())
		self.tourListLayout.addWidget(self.reservedWidget)

		self.tourListLayout.addItem(style.Layout.vSpacer)

		self.scroll.setInnerWidget(self.tourList)

		self.mainLayout.addWidget(self.scroll)

		self.setLayout(self.mainLayout)

	def getActiveTours(self):
		self.activeTourReturnList = QVBoxLayout()

		i = 0
		Database.connectDB()
		for t in Tour.select().where(Tour.active == True).order_by(Tour.begin.asc()):
			i = i + 1
			tw = TourWidget(True, t, i)
			tw.renewReserved.connect(self.updateReserved)
			tw.removeFromReserved.connect(self.removeFromReserved)
			tw.renewActive.connect(self.updateActive)
			tw.removeFromActive.connect(self.removeFromActive)
			self.activeTourReturnList.addWidget(tw)
		
		Database.closeDB()
			

		if i == 0:
			self.activeTourReturnList.addWidget(QLabel("Keine Aktiven Touren"))
		return self.activeTourReturnList

	def getReservedTours(self):
		returnView = QVBoxLayout()
		i = 0
		Database.connectDB()
		for t in Tour.select().where(Tour.reserved == True).order_by(Tour.begin.asc()):
			i = i + 1
			tw = TourWidget(False, t, i)
			tw.renewReserved.connect(self.updateReserved)
			tw.removeFromReserved.connect(self.removeFromReserved)
			tw.renewActive.connect(self.updateActive)
			tw.removeFromActive.connect(self.removeFromActive)
			tw.changeTourSignal.connect(lambda m: self.changeTourSignal.emit(m))
			returnView.addWidget(tw)
		
		Database.closeDB()
			

		if i == 0:
			returnView.addWidget(QLabel("Keine Reservierten Touren"))
		
		return returnView

	def removeFromReserved(self, index):
		fromIndex = index
		count = self.reservedWidget.layout().count()
		if count < index:
			fromIndex = self.reservedWidget.layout().count()
		for i in reversed(range(0, fromIndex)):
			currentWidget = self.reservedWidget.layout().itemAt(i).widget()
			if currentWidget.number == index:
				currentWidget.deleteLater()
				if count-1 == 0:
					self.reservedWidget.layout().addWidget(QLabel("Keine Reservierten Touren"))
				return
		writeLog("error.txt", "Couldnt remove Tour from Reserved, No Such Tour")


	def removeFromActive(self, index):
		fromIndex = index
		count = self.activeWidget.layout().count()
		if count < index:
			fromIndex = self.activeWidget.layout().count()
		for i in reversed(range(0, fromIndex)):
			currentWidget = self.activeWidget.layout().itemAt(i).widget()
			if currentWidget.number == index:
				currentWidget.deleteLater()
				if count-1 == 0:
					self.activeWidget.layout().addWidget(QLabel("Keine Aktiven Touren"))
				return
		writeLog("error.txt", "Couldnt remove Tour from Active, No Such Tour")

	def updateReserved(self):
		tempWidget = QWidget()
		tempWidget.setLayout(self.reservedWidget.layout())
		tempWidget.deleteLater()
		self.reservedWidget.setLayout(self.getReservedTours())

	def updateActive(self):
		tempWidget = QWidget()
		tempWidget.setLayout(self.activeWidget.layout())
		tempWidget.deleteLater()
		self.activeWidget.setLayout(self.getActiveTours())

	def update(self):
		self.updateReserved()
		self.updateActive()

		


		
from logic.VehicleDistributionServices.ServiceData.Vehicles import Vehicle, VehicleStatus
from logic.VehicleDistributionServices.ServiceData.Members import Member

from logic.VehicleAdministration import VehicleAdministration as VA

from gui.misc.General import StyledToggleWidget
from gui.misc.Buttons import ImportantButton, AppButton


class TourOverview(QWidget):
	def __init__(self, mission, parent=None):
		super(TourOverview, self).__init__(parent)
		self.mainLayout = QVBoxLayout(self)
		self.setContentsMargins(0,0,0,0)
		self.mainLayout.setSpacing(10)

		# messageLabel = QLabel(mission.message)
		# messageLabel.setFont(style.Fonts.h3)
		# messageLabel.setWordWrap(True)

		beginTitle = QLabel("Beginn:")
		beginTime = QLabel(mission.timestart.strftime("%d.%m.%Y %H:%M"))

		endTitle = QLabel("Erwartetes Ende:")
		endTime = QLabel(mission.timeend.strftime("%d.%m.%Y %H:%M"))
		endTimeNA = QLabel("Keine Angabe")

		beginTitle.setFont(style.Fonts.bold)
		beginTime.setFont(style.Fonts.bold)
		endTitle.setFont(style.Fonts.bold)
		endTime.setFont(style.Fonts.bold)
		endTimeNA.setFont(style.Fonts.bold)


		timeLayout = QGridLayout()
		timeLayout.addWidget(beginTitle, 0, 0)
		timeLayout.addWidget(beginTime, 0, 1)
		timeLayout.addWidget(endTitle, 1, 0)
		if mission.timestart != mission.timeend:
			timeLayout.addWidget(endTime, 1, 1)
		else:
			timeLayout.addWidget(endTimeNA, 1, 1)


		# self.mainLayout.addWidget(messageLabel)

		self.mainLayout.addLayout(timeLayout)


class VehicleReturnFromMissionWidget(QWidget):
	clicked = pyqtSignal()
	def __init__(self, vehicle, driver, parent=None):
		QWidget.__init__(self, parent)
		self.setContentsMargins(0,0,0,0)

		self.vehicle = vehicle
		self.mainLayout = QVBoxLayout(self)

		self.title = QLabel(vehicle.name)
		self.driverInfo = QLabel("Fahrer: " + driver.fullname)
		self.returnButton = ImportantButton(vehicle.name + "\nEinsatzbereit melden")
		self.returnButton.setStyle("background-color: " + style.Color.scheme.returnButton + ";")

		self.title.setFont(style.Fonts.h4)
		self.driverInfo.setFont(style.Fonts.bold)
		self.returnButton.setFont(style.Fonts.bolder)
		self.returnButton.clicked.connect(self.clicked.emit)

		self.mainLayout.addWidget(self.title)
		self.mainLayout.addWidget(self.driverInfo)
		self.mainLayout.addWidget(self.returnButton)



#todo since this widget is deleted and a new is created, there is no need to make assign it both: a reserved and an active widget... and hide the one thats not used...
class TourWidget(QWidget):
	clicked = pyqtSignal()

	renewReserved = pyqtSignal()
	removeFromReserved = pyqtSignal(int)
	renewActive = pyqtSignal()
	removeFromActive = pyqtSignal(int)

	changeTourSignal = pyqtSignal(Mission)

	def __init__(self, active, tour, number, parent=None):
		QWidget.__init__(self, parent)
		self.active = active
		self.number = number

		self.vehicleList = []
		self.setContentsMargins(0,10,0,0)


		Database.connectDB()
		tourDriverList = [vt for vt in TourDriver.select().where(TourDriver.tour == tour.identifier, TourDriver.end == "0000-00-00 00:00:00")]
		Database.closeDB()

		for td in tourDriverList:
			v = Vehicle()
			v.model = td.vehicle
			v.vID = v.model.identifier
			v.name = v.model.longname

			#doesnt call 122 to check if tour is correct (only check it if you send it..)
			driver = Member()
			driver.loadFromDB(td.driver)
			v.driver = driver
			self.vehicleList.append(v)
		


		self.mission = Mission(self.vehicleList, tour.title, tour.message, tour.begin, tour.expectedEnd, tour.type, tour.type)
		self.mission.model = tour
		
		self.mainLayout = QVBoxLayout()
		
		# permaWidget = QLabel(self.mission.title)
		permaWidget = QLabel(self.mission.message)
		permaWidget.setWordWrap(True)
		permaWidget.setFont(style.Fonts.h2)

		openWidget = QWidget()
		openLayout = QVBoxLayout(openWidget)
		openWidget.setContentsMargins(0,0,0,0)

		closedWidget = QWidget()
		closedLayout = QVBoxLayout(closedWidget)
		closedWidget.setContentsMargins(0,0,0,0)


		openLayout.addWidget(TourOverview(self.mission))
		
				

		if self.mission.timestart == self.mission.timeend:
			closedTimeWidget = QLabel("<strong>" + self.mission.timestart.strftime("%d.%m.%Y") + "</strong>: " + self.mission.timestart.strftime("%H:%M"))
		elif self.mission.timestart.date() == self.mission.timeend.date():
			closedTimeWidget = QLabel("<strong>" + self.mission.timestart.strftime("%d.%m.%Y") + "</strong>: " + self.mission.timestart.strftime("%H:%M") + " - " + self.mission.timeend.strftime("%H:%M"))
		else:
			closedTimeWidget = QLabel("<strong>" + self.mission.timestart.strftime("%d.%m.%Y") + " - " + self.mission.timeend.strftime("%d.%m.%Y") + "</strong>: " + self.mission.timestart.strftime("%H:%M") + " - " + self.mission.timeend.strftime("%H:%M"))
		closedLayout.addWidget(closedTimeWidget)


		openVehicleLayout = QVBoxLayout()
		openVehicleLayout.setSpacing(0)

		for v in self.mission.vehicleList:
			
			vehicleWA = VehicleReturnFromMissionWidget(v, v.driver)
			vehicleWA.clicked.connect(self.returnFromTour)
			vehicleWA.setContentsMargins(0,0,0,0)

			vehicleWR = QLabel(v.name)
			vehicleWR.setFont(style.Fonts.bold)
			vehicleWR.setContentsMargins(10,5,0,5)

			openVehicleLayout.addWidget(vehicleWA)
			openVehicleLayout.addWidget(vehicleWR)

			if self.active:
				vehicleWA.show()
				vehicleWR.hide()
			else:
				vehicleWA.hide()
				vehicleWR.show()


		openLayout.addLayout(openVehicleLayout)

		closedVehicleList = QLabel(", ".join(v.name for v in self.mission.vehicleList))
		closedLayout.addWidget(closedVehicleList)


		activeControl = QWidget()
		activeControlLayout = QHBoxLayout(activeControl)
		returnButton = ImportantButton("Alle Fahrzeuge\nEinsatzbereit melden")
		returnButton.setStyle("background-color: " + style.Color.scheme.returnButton + ";")
		returnButton.clicked.connect(self.returnAllFromTour)
		activeControlLayout.addWidget(returnButton)

		reservedControl = QWidget()
		reservedControlLayout = QGridLayout(reservedControl)
		sendButton = ImportantButton("Statuswechsel Durchführen")
		sendButton.setFont(style.Fonts.bolder)
		sendButton.clicked.connect(self.sendToTour)
		cancelButton = AppButton("Tour löschen")
		cancelButton.setFont(style.Fonts.bold)
		cancelButton.clicked.connect(self.cancelTour)
		changeButton = AppButton("Tour ändern")
		changeButton.setFont(style.Fonts.bold)
		changeButton.clicked.connect(self.changeTour)

		reservedControlLayout.addWidget(sendButton, 0, 0, 1,-1)
		reservedControlLayout.addWidget(changeButton, 1, 0)
		reservedControlLayout.addWidget(cancelButton, 1, 1)


		openLayout.addWidget(activeControl)
		openLayout.addWidget(reservedControl)

		if self.active:
			activeControl.show()
			reservedControl.hide()
		else:
			activeControl.hide()
			reservedControl.show()

		if self.active:
			self.tourView = StyledToggleWidget(permaWidget, openWidget, closedWidget, False, style.Color.scheme.activeTourBackground, style.Color.scheme.activeTourBackground)
		else:
			self.tourView = StyledToggleWidget(permaWidget, openWidget, closedWidget, False, style.Color.scheme.reservedTourBackground, style.Color.scheme.reservedTourBackground)


		self.mainLayout.addWidget(self.tourView)
		#self.update()

		self.setLayout(self.mainLayout)

		
		
	def returnFromTour(self):
		Emitter.getInstance("loading").emit()
		vehicleList = [self.sender().vehicle]
		m = Mission(vehicles = vehicleList)
		VA.instance.returnFromTour(m)
		self.renewActive.emit()
		Emitter.getInstance("loadingReady").emit()


	def returnAllFromTour(self):
		Emitter.getInstance("loading").emit()
		VA.instance.returnFromTour(self.mission)
		self.removeFromActive.emit(self.number)
		Emitter.getInstance("loadingReady").emit()

	def sendToTour(self):
		vlist = VA.instance.vehicles.getList(self.mission.vehicleList)
		notReady = []
		for v in vlist:
			if v.status != VehicleStatus.ready:
				notReady.append(v.name)

		if notReady.__len__() != 0:
			AppDialogBox.critical("Errors wurden entdeckt", "Folgende Fahrzeuge sind noch nicht als Einsatzbereit gemeldet. Melden Sie diese zuerst zurück oder wählen Sie sie ab:\n" + ", ".join(notReady))
			return

		timeDelta = self.mission.timeend - self.mission.timestart
		self.mission.timestart = datetime.datetime.now()
		self.mission.timeend = self.mission.timestart + timeDelta

		Emitter.getInstance("loading").emit()

		self.mission.reason = self.mission.type
		for v in self.mission.vehicleList:
			VA.instance.getMember(v.driver.stbn)
			v.driver = VA.instance.member

		VA.instance.sendToTour(self.mission)
		self.removeFromReserved.emit(self.number)
		self.renewActive.emit()
		Emitter.getInstance("loadingReady").emit()

	def cancelTour(self):
		Emitter.getInstance("loading").emit()
		VA.instance.cancelTour(self.mission)
		self.removeFromReserved.emit(self.number)
		Emitter.getInstance("loadingReady").emit()

	def changeTour(self):
		VA.instance.vehicles.selectList(self.mission.vehicleList)
		self.changeTourSignal.emit(self.mission)
