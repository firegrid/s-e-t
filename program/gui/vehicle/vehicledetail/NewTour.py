from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import datetime

import threading

import settings
import style
from general.functions import writeLog

from gui.QtFunctions import clearLayout

from gui.misc.General import OptionalQWidget, PictureWidget
from gui.misc.MessageInputWidget import TourMessageBox
from gui.misc.Buttons import DescriptiveButton, ImportantButton, AppButton, ToggleButton, PictureButton
from gui.misc.Dialogs import AppDialogBox
from gui.misc.Forms import ClickableTextEdit
from gui.misc.Layout import ScrollableArea
from gui.misc.Emitters import ModelEmitter, Emitter, ObjectEmitter


from logic.VehicleAdministration import VehicleAdministration as VA
from logic.VehicleDistributionServices.ServiceData.Mission import Mission, MissionStatus
from logic.VehicleDistributionServices.ServiceData.Vehicles import VehicleStatus

# from models.base import Database
# from models.tour import Tour as TourModel

#Widget that allows you to create a new tour
class NewTourWidget(QWidget):
	needKeyBoardSignal = pyqtSignal(QTextEdit)
	emptyTourSignal = pyqtSignal()
	sendToTourSignal = pyqtSignal()

	def __init__(self, vehicleView, parent=None):
		super(NewTourWidget, self).__init__(parent)
		self.mainLayout = QVBoxLayout()
		self.mainLayout.setSpacing(25)

		self.existingTour = False
		self.reservation = False
		self.tour = None

		self.vehicleView = vehicleView

		self.titleLayout = QHBoxLayout()

		self.titleLabel = QLabel("Neue Tour")
		self.titleLabel.setFont(style.Fonts.h2)
		self.titleLayout.addWidget(self.titleLabel)

		self.titleLayout.addItem(style.Layout.hSpacer)

		self.clearButton = PictureButton(":/media/images/misc/close.png", "misc")
		self.clearButton.setStyleSheet("""
			border-radius: 10px;
			background-color: #000000;
			padding: 10px 20px;
			""")
		self.clearButton.clicked.connect(self.clear)
		self.titleLayout.addWidget(self.clearButton)

		self.mainLayout.addLayout(self.titleLayout)

		scrollArea = ScrollableArea()
		scrollWidget = QWidget()
		self.scrollLayout = QVBoxLayout(scrollWidget)
		self.scrollLayout.setContentsMargins(0,0,0,0)
		scrollArea.setInnerWidget(scrollWidget)
		self.mainLayout.addWidget(scrollArea)


		self.vehicleList = TourVehicleList()
		self.vehicleList.ready.connect(self.canSendTour)
		self.scrollLayout.addWidget(self.vehicleList)

		self.messageBox = TourMessageBox()
		self.messageBox.clicked.connect(lambda:self.needKeyBoardSignal.emit(self.messageBox))
		self.messageBox.readySignal.connect(self.canSendTour)
		self.scrollLayout.addWidget(self.messageBox)

		self.scrollLayout.addItem(style.Layout.vSpacer)


		self.timeListView = TourTimeInputWidget(self.vehicleView)
		self.timeListView.uncheckQuickSelection.connect(self.uncheckQuickSelection)
		self.timeListView.needTimeDisplay.connect(lambda time, reason:self.vehicleView.displayChangeTimeView(time, reason))
		self.timeListView.changedStart.connect(self.changedStartTime)

		durationDescriptionLabel = QLabel("Geplante Dauer:")
		durationDescriptionLabel.setFont(style.Fonts.h3bold)
		self.scrollLayout.addWidget(durationDescriptionLabel)

		self.expectedEndNoButton = TourTimeQuickSelectionButton(0)
		self.expectedEnd1HourButton = TourTimeQuickSelectionButton(1)
		self.expectedEnd3HourButton = TourTimeQuickSelectionButton(3)
		self.expectedEnd6HourButton = TourTimeQuickSelectionButton(6)
		self.expectedEndNoButton.chosen.connect(lambda:self.timeListView.hourButtonClicked(self.sender().value))
		self.expectedEnd1HourButton.chosen.connect(lambda:self.timeListView.hourButtonClicked(self.sender().value))
		self.expectedEnd3HourButton.chosen.connect(lambda:self.timeListView.hourButtonClicked(self.sender().value))
		self.expectedEnd6HourButton.chosen.connect(lambda:self.timeListView.hourButtonClicked(self.sender().value))
		self.expectedEndNoButton.setChecked(True)

		self.quickLayout = QHBoxLayout()
		self.quickLayout.setSpacing(5)

		self.quickLayout.addWidget(self.expectedEndNoButton)
		self.quickLayout.addWidget(self.expectedEnd1HourButton)
		self.quickLayout.addWidget(self.expectedEnd3HourButton)
		self.quickLayout.addWidget(self.expectedEnd6HourButton)

		self.scrollLayout.addLayout(self.quickLayout)


		self.optionalWidget = OptionalQWidget()

		
		self.optionalWidget.setOptionalContent(self.timeListView)

		self.scrollLayout.addWidget(self.optionalWidget)

		self.sendButton = ImportantButton("Statuswechsel durchführen")
		self.sendButton.clicked.connect(self.sendTour)

		self.mainLayout.addWidget(self.sendButton)

		self.setLayout(self.mainLayout)


	def sendTour(self):

		vehicles = []
		title = self.messageBox.toPlainText()
		if self.messageBox.snippets.__len__() != 0:
			title = self.messageBox.snippets[0].title

		message = self.messageBox.toPlainText()

		noDriver = []
		notReady = []

		reason = tourType = self.messageBox.type

		startTime = self.timeListView.startTime
		endTime = self.timeListView.endTime


		if self.reservation:
			reason = MissionStatus.Reservation

		for i in range(self.vehicleList.mainLayout.count()):
			v = self.vehicleList.mainLayout.itemAt(i).widget().vehicle
			d = self.vehicleList.mainLayout.itemAt(i).widget().driver

			if v.status != VehicleStatus.ready:
				notReady.append(v.name)

			if d != None:
				vehicles.append(v)
			else:
				noDriver.append(v.name)


		#check for errors
		errormsg = []

		if message.__len__() == 0:
			errormsg.append("Zusatzinformation ist leer")

		if vehicles.__len__() == 0:
			errormsg.append("Keines der ausgewählten Fahrzeuge besitzt Fahrer")

		if startTime > endTime:
			errormsg.append("Die Tour kann nicht Enden bevor sie angefangen hat. Überprüfen Sie Ihren Eintrag bei erwartetes Ende") 

		if self.reservation != True and notReady.__len__() != 0:
			errormsg.append("Folgende Fahrzeuge sind noch nicht als Einsatzbereit gemeldet. Melden Sie diese zuerst zurück oder wählen Sie sie ab:\n" + ", ".join(notReady))

		if errormsg.__len__() != 0:
			AppDialogBox.critical("Errors wurden entdeckt", "Sie müssen nachfolgende Fehler beheben, bevor ihre Daten abgeschickt werden:\n\n - " + "\n - ".join(errormsg))
			return


		if noDriver.__len__() != 0:
			answer = AppDialogBox.question("Fahrzeuge ohne Fahrer", "Die nachfolgenden Fahrzeuge besitzen keine Fahrer und werden deswegen nicht zur Tour gebucht:\n\n - "+ "\n - ".join(noDriver) + "\n\nWollen Sie trotzdem Fortfahren und die restlichen Fahrzeuge abschicken?")
			if not answer:
				return

		Emitter.getInstance("loading").emit()

		if self.existingTour:
			VA.instance.cancelTour(self.tour)

		m = Mission(vehicles, title, message, startTime, endTime, reason, tourType)

		VA.instance.sendToTour(m)
		self.sendToTourSignal.emit()
		self.clear()
		Emitter.getInstance("loadingReady").emit()


	def canSendTour(self):
		if self.messageBox.isFilledOut() and self.vehicleList.isFilledOut():
			self.sendButton.startAnimation()

	def clear(self):
		self.messageBox.clearWidget()
		self.vehicleList.clearWidget()
		self.timeListView.clearWidget()
		self.expectedEndNoButton.setChecked(True)
		self.optionalWidget.reset()
		VA.instance.vehicles.deselectAll()
		self.emptyTourSignal.emit()
		self.reservation = False
		self.sendButton.setText("Statuswechsel durchführen")
		self.existingTour = False
		self.tour = None
		self.titleLabel.setText("Neue Tour")

		self.sendButton.stopAnimation()

	def changedStartTime(self, reservation):
		self.reservation = reservation
		if self.reservation:
			self.sendButton.setText("Reservierung speichern")
		else:
			self.sendButton.setText("Statuswechsel durchführen")

	def changeExistingTour(self, tour):
		self.existingTour = True
		self.tour = tour
		self.changedStartTime(True)
		self.titleLabel.setText("Tour ändern")

		self.vehicleList.updateVehicleList()
		for v in tour.vehicleList:
			for i in range(self.vehicleList.mainLayout.count()):
				if self.vehicleList.mainLayout.itemAt(i).widget().vehicle.vID == v.vID:
					self.vehicleList.mainLayout.itemAt(i).widget().driver = v.driver
					self.vehicleList.mainLayout.itemAt(i).widget().update()

		self.messageBox.inputMessage(tour.message)
		self.timeListView.startTime = tour.timestart
		self.timeListView.endTime = tour.timeend
		self.timeListView.modifiedStart = True
		self.timeListView.modifiedEnd = True

	def uncheckQuickSelection(self):
		self.expectedEndNoButton.setAutoExclusive(False)
		self.expectedEnd1HourButton.setAutoExclusive(False)
		self.expectedEnd3HourButton.setAutoExclusive(False)
		self.expectedEnd6HourButton.setAutoExclusive(False)
		self.expectedEndNoButton.setChecked(False)
		self.expectedEnd1HourButton.setChecked(False)
		self.expectedEnd3HourButton.setChecked(False)
		self.expectedEnd6HourButton.setChecked(False)
		self.expectedEndNoButton.setAutoExclusive(True)
		self.expectedEnd1HourButton.setAutoExclusive(True)
		self.expectedEnd3HourButton.setAutoExclusive(True)
		self.expectedEnd6HourButton.setAutoExclusive(True)



from logic.VehicleAdministration import VehicleAdministration as VA
from logic.VehicleDistributionServices.ServiceData.Vehicles import VehicleStatus



class TourVehicleList(QWidget):
	ready = pyqtSignal()
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QVBoxLayout(self)

		self.currentEdit = None
		
		self.currentlyInList = []

	def driverLineClicked(self):
		self.currentEdit = self.sender()
		ModelEmitter.getInstance("MemberChosen").connect(self.memberChooserCallBack)
		ObjectEmitter.getInstance("ShowMemberChooserView").emit(self.sender())

	def memberChooserCallBack(self, member):
		ModelEmitter.getInstance("MemberChosen").disconnect(self.memberChooserCallBack)
		Emitter.getInstance("ShowVehicleView").emit()


		self.currentEdit.driver = member
		self.currentEdit.update()
		self.currentEdit = None

	def updateVehicleList(self):
		removeElements = set(self.currentlyInList) - set(VA.instance.vehicles.selectedVehicleList)
		checkToIncludeElements = set(VA.instance.vehicles.selectedVehicleList) - set(self.currentlyInList)

		for v in removeElements:
			for i in reversed(range(self.mainLayout.count())):
				if self.mainLayout.itemAt(i).widget().vehicle.vID == v.vID:
					self.mainLayout.itemAt(i).widget().setParent(None)
					self.currentlyInList.remove(v)
					break


		for v in checkToIncludeElements:
			self.currentlyInList.append(v)
			vdcw = VehicleDriverChooserWidget(v)
			vdcw.clicked.connect(self.driverLineClicked)
			vdcw.ready.connect(self.ready.emit)
			self.mainLayout.addWidget(vdcw)
		

	def clearWidget(self):
		clearLayout(self.mainLayout)
		# for i in reversed(range(self.mainLayout.count())):
		# 	self.mainLayout.itemAt(i).widget().setParent(None)

		self.currentlyInList = []

	def isFilledOut(self):
		for i in reversed(range(self.mainLayout.count())):
			if self.mainLayout.itemAt(i).widget().driver == None:
				return False

		return True
		



class TourTimeInputWidget(QWidget):
	needTimeDisplay = pyqtSignal(datetime.datetime, str) #need changing??
	uncheckQuickSelection = pyqtSignal()
	changedStart = pyqtSignal(bool)
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.modifiedStart = False
		self.startTime = datetime.datetime.now()
		self.modifiedEnd = False
		self.endTimeDelta = datetime.timedelta(hours=0)
		self.endTime = datetime.datetime.now() + self.endTimeDelta

		self.changeBegin = False
		self.changeEnd = False

		self.mainLayout = QGridLayout(self)

		self.mainLayout.setVerticalSpacing(20)
		self.mainLayout.setHorizontalSpacing(5)

		begindescriptionLabel = QLabel("Beginn:")
		begindescriptionLabel.setFont(style.Fonts.h3bold)

		self.mainLayout.addWidget(begindescriptionLabel, 0, 0, 1, 3)
		self.changeBeginButton = AppButton("ändern")
		self.changeBeginButton.clicked.connect(lambda:self.changeTime(True))
		self.beginLabel = QLabel(self.startTime.strftime("%d.%m.%Y %H:%M:%S"))
		self.beginLabel.setAlignment(Qt.AlignVCenter | Qt.AlignRight)
		self.beginLabel.setFont(style.Fonts.h4)
		self.mainLayout.addWidget(self.beginLabel, 1, 0, 1, 3)
		self.mainLayout.addWidget(self.changeBeginButton, 0, 3, 2, 1)


		enddescriptionLabel = QLabel("Erwartetes Ende:")
		enddescriptionLabel.setFont(style.Fonts.h3bold)

		self.mainLayout.addWidget(enddescriptionLabel, 2, 0, 1, 3)
		self.endLabel = QLabel(self.endTime.strftime("%d.%m.%Y %H:%M:%S"))
		self.endLabel.setAlignment(Qt.AlignVCenter | Qt.AlignRight)
		self.endLabel.setFont(style.Fonts.h4)
		self.changeEndButton = AppButton("ändern")
		self.changeEndButton.clicked.connect(lambda:self.changeTime(False))

		
		self.mainLayout.addWidget(self.endLabel, 3, 0, 1, 3)
		self.mainLayout.addWidget(self.changeEndButton, 2, 3, 2, 1)


		self.timer = QTimer(self)
		self.timer.timeout.connect(self.calcTime)
		self.timer.start(100)

	def calcTime(self):
		if self.modifiedStart == False:
			self.startTime = datetime.datetime.now()

		if self.modifiedEnd == False:
			self.endTime = self.startTime + self.endTimeDelta

		self.beginLabel.setText(self.startTime.strftime("%d.%m.%Y %H:%M:%S"))
		self.endLabel.setText(self.endTime.strftime("%d.%m.%Y %H:%M:%S"))

	def changeTime(self, begin):
		if begin:
			self.changeBegin = True
			self.needTimeDisplay.emit(self.startTime, "Beginn:")
		else:
			self.changeEnd = True
			self.needTimeDisplay.emit(self.endTime, "Erwartetes Ende:")

	def changeTimeCallBack(self, modified, time):
		if self.changeBegin:
			if modified == 1:
				self.modifiedStart = True
				self.changedStart.emit(True)
			elif modified == 2:
				self.modifiedStart = False
				self.changedStart.emit(False)
			self.startTime = time
			self.changeBegin = False

		elif self.changeEnd:
			if modified == 1:
				self.uncheckQuickSelection.emit()
				
				self.modifiedEnd = True
				self.endTime = time
				self.changeEnd = False


	def hourButtonClicked(self, hours):
		self.modifiedEnd = False
		self.endTimeDelta = datetime.timedelta(hours=hours)
		self.endTime = datetime.datetime.now() + self.endTimeDelta

	def clearWidget(self):
		self.modifiedStart = False
		self.startTime = datetime.datetime.now()
		self.modifiedEnd = False
		self.endTimeDelta = datetime.timedelta(hours=0)
		self.endTime = datetime.datetime.now() + self.endTimeDelta
		#self.expectedEndNoButton.setChecked(True)
		self.changeBegin = False
		self.changeEnd = False


class TourTimeQuickSelectionButton(ToggleButton):
	chosen = pyqtSignal()
	def __init__(self, value, parent=None):
		ToggleButton.__init__(self, str(value) + " h", parent=parent) # + " Stunden"

		self.value = value

		if value == 0:
			self.setText("k.A.")
		# elif value == 1:
		# 	self.setText(str(value) + " Stunde")

	def mousePressEvent(self, event):
		self.chosen.emit()
		super(TourTimeQuickSelectionButton, self).mousePressEvent(event)


from logic.VehicleDistributionServices.ServiceData.Members import Member


class VehicleDriverChooserWidget(DescriptiveButton):
	ready = pyqtSignal()
	def __init__(self, vehicle, parent=None):
		DescriptiveButton.__init__(self, vehicle.name, "Fahrer zuweisen", True, parent)		
		self.vehicle = vehicle
		self.driver = None

		self.mainLayout.setSpacing(5)
		self.mainLayout.setContentsMargins(0,0,0,20)
		self.description.setFont(style.Fonts.VehicleName)


	def update(self):
		if self.driver == None:
			self.button.setText(self.buttonText)
			self.vehicle.driver = None
		else:
			self.button.setText(self.driver.rank + " " + self.driver.firstname + " " + self.driver.lastname)
			self.button.stopAnimation()
			t = threading.Thread(target=self.getDriver)
			t.start()
			self.ready.emit()
			self.button.setFont(style.Fonts.normal)

	def getDriver(self):
			driver = Member()
			VA.instance.getMember(self.driver.stbn, driver)
			self.vehicle.driver = driver