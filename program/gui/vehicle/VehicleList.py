from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style

from general.functions import writeLog

from logic.VehicleDistributionServices.ServiceData.Vehicles import readableStatus, VehicleStatus

from gui.misc.General import PictureWidget, ClickableLabel
from gui.misc.Layout import ScrollableArea
from gui.misc.Buttons import ImportantButton
from gui.misc.Emitters import ModelEmitter

from models.base import Database
from models.equipment import Equipment
from models.vehicle import Vehicle


class VehicleList(QWidget):
	updateActiveTours = pyqtSignal()
	def __init__(self, parent=None):
		super(VehicleList, self).__init__(parent)
		self.vehicleView = parent


		self.vList = []

		mode = settings.Settings.get("Vehicle", "layout", "int")

		columns = settings.Settings.get("Vehicle", "columns", "int")
		currentColumn = 0
		row = 0

		self.mainLayout = QVBoxLayout(self)

		self.scrollableArea = ScrollableArea()

		vehicleView = QWidget()

		if mode == 1:#list layout
			vehicleLayout = QVBoxLayout(vehicleView)
			columns = 1
		else:#grid layout
			vehicleLayout = QGridLayout(vehicleView)

		vehicleLayout.setSpacing(0)
		vehicleLayout.setContentsMargins(0,0,0,0)

		vehicleLayout.setSpacing(settings.Settings.get("Vehicle", "spacing", "int"))

		for v in self.vehicleView.va.vehicles.vehicles:
			vehicleWidget = VehicleWidget(v, mode)
			vehicleWidget.clicked.connect(self.vehicleClicked)

			vehicleWidget.returnVehicle.connect(self.returnVehicle)

			self.vList.append(vehicleWidget)

			if mode == 1:#List Layout
				vehicleLayout.addWidget(vehicleWidget)
			else:#Grid Layout
				vehicleLayout.addWidget(vehicleWidget, row, currentColumn)


			if (currentColumn+1) >= columns:
				currentColumn = 0
				row += 1
			else:
				currentColumn += 1


		self.scrollableArea.setInnerWidget(vehicleView)
		self.mainLayout.addWidget(self.scrollableArea)



	def update(self):
		for w in self.vList:
			w.update()

	def vehicleClicked(self):
		if self.sender().vehicle.selected:
			VA.instance.vehicles.selectedVehicleList.append(self.sender().vehicle)
		else:
			VA.instance.vehicles.selectedVehicleList.remove(self.sender().vehicle)
		self.vehicleView.vehicleClicked(self.sender())

	def returnVehicle(self):
		self.updateActiveTours.emit()



import datetime
from logic.VehicleDistributionServices.ServiceData.Vehicles import readableStatus
from logic.VehicleAdministration import VehicleAdministration as VA
from logic.VehicleDistributionServices.ServiceData.Mission import Mission

class VehicleWidget(QFrame):
	clicked = pyqtSignal()
	returnVehicle = pyqtSignal()
	# requestEquipmentScreen = pyqtSignal(long)
	scrollEvent = pyqtSignal(QEvent)
	def __init__(self, vehicle, mode, parent=None):
		QWidget.__init__(self, parent)

		self.vehicle = vehicle
		self.mode = mode

		self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))

		if mode == 1:
			#list Layout
			self.mainLayout = QHBoxLayout(self)
		else:
			#grid Layout
			self.mainLayout = QVBoxLayout(self)

		self.picture = PictureWidget(settings.Settings.get("Vehicle", "imagePath") + str(self.vehicle.vID) + ".png")

		self.nameLabel = QLabel(self.vehicle.name)
		self.nameLabel.setFont(style.Fonts.VehicleName)
		self.nameLabel.setStyleSheet("color: " + style.Color.scheme.vehicleNameColor + ";")
		self.statusLabel = ClickableLabel()
		self.statusLabel.setFont(style.Fonts.VehicleStatus)
		self.equipmentLabel = None
		self.equipmentReady = True


		if mode == 1:
			#list Layout
			self.pictureLayout = QVBoxLayout()

			self.descriptionLayout = QVBoxLayout()
			
			self.nameLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
			self.statusLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
			
		else:
			#grid Layout
			self.pictureLayout = QHBoxLayout()

			self.descriptionLayout = QVBoxLayout()

			self.nameLabel.setAlignment(Qt.AlignCenter)
			self.statusLabel.setAlignment(Qt.AlignCenter)

		

		self.pictureLayout.addItem(style.Layout.hSpacer)
		self.pictureLayout.addWidget(self.picture)

		self.pictureLayout.addItem(style.Layout.hSpacer)
		self.mainLayout.addLayout(self.pictureLayout)
		

		self.descriptionLayout.addWidget(self.nameLabel)
		self.descriptionLayout.addWidget(self.statusLabel)

		if mode == 2:
			self.statusLabel.clicked.connect(self.requestStatusScreenFun)
			self.equipmentLabel = ClickableLabel()
			self.equipmentLabel.setAlignment(Qt.AlignCenter)
			self.equipmentLabel.setFont(style.Fonts.VehicleEquipment)
			self.equipmentLabel.clicked.connect(self.requestEquipmentScreenFun)
			self.descriptionLayout.addWidget(self.equipmentLabel)

		self.mainLayout.addLayout(self.descriptionLayout)

		self.standardStyle = """
			VehicleWidget{
				border-radius: """ + str(settings.Settings.get("Vehicle", "borderRadius", "int")) + """px;
				border-width: 5px;
				border-style: solid;
			}

			ClickableLabel {
				border-radius: 10px;
				background-color: rgba(0,0,0,0.5);
				
			}
			"""

			# padding: 5px 5px; QLabel, 

		

		self.update()


	def mousePressEvent(self, event):
		self.vehicle.toogleSelected()
		self.clicked.emit()
		self.update()

		super(VehicleWidget, self).mousePressEvent(event)
		

	def update(self):
		Database.connectDB()
		if self.equipmentLabel != None:
			self.vehicle.model
			
			l = [v for v in Equipment.select().where(Equipment.vehicle == self.vehicle.model, Equipment.ready == False)]
			notReadyEquipmentCount = l.__len__()

			if(notReadyEquipmentCount > 1):
				equipmentStatus = str(notReadyEquipmentCount) + " Geräte nicht Einsatzbereit"
				self.equipmentLabel.setStyleSheet("background-color: rgba(150, 0, 0, 1)")
				self.equipmentReady = False
			elif(notReadyEquipmentCount == 1):
				equipmentStatus = str(notReadyEquipmentCount) + " Gerät nicht Einsatzbereit"
				self.equipmentLabel.setStyleSheet("background-color: rgba(150, 0, 0, 1)")
				self.equipmentReady = False
			else:
				equipmentStatus = "Geräte Einsatzbereit"
				self.equipmentReady = True
				self.equipmentLabel.setStyleSheet("")

			
				

			
			self.equipmentLabel.setText(equipmentStatus)


		self.statusLabel.setText(readableStatus[self.vehicle.status])


		if Vehicle.get(identifier = self.vehicle.model).ready:
			self.statusLabel.setStyleSheet("")
		else:
			self.statusLabel.setText(readableStatus[VehicleStatus.locallyNotReady])
			self.statusLabel.setStyleSheet("background-color: rgba(150, 0, 0, 1)")

		Database.closeDB()


		self.setStyleSheet(
			self.standardStyle + """
			VehicleWidget{
				border-color: """ + style.Color.scheme.vehicleBackground[self.vehicle.status] + """;
				background-color:""" + style.Color.scheme.vehicleBackground[self.vehicle.getSelectOrStatus()] + """;
			}
			#picture {
				background-color: """ + style.Color.scheme.vehicleBackground[self.vehicle.getSelectOrStatus()] + """;
			}""")

		

	def returnDriver(self): # deprecated -> no longer a returndriver button on vehicle
		vehicleList = [self.vehicle]
		m = Mission(vehicles = vehicleList)

		self.returnButton.hide()

		VA.instance.returnFromTour(m)

		self.returnVehicle.emit()

	def requestStatusScreenFun(self):
		ModelEmitter.getInstance("StatusScreen").emit(self.vehicle.model)

	def requestEquipmentScreenFun(self):
		ModelEmitter.getInstance("EquipmentScreen").emit(self.vehicle.model)

