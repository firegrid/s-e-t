from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import threading

import settings

from logic.VehicleAdministration import VehicleAdministration

from gui.vehicle.vehicledetail.VehicleDetail import *
from gui.vehicle.VehicleList import *

from gui.misc.TimeChangeWidget import TimeChangeWidget
from gui.misc.MemberChooserWidget import MemberChooserWidget
from gui.misc.MessageInputWidget import MessageInputWidget, ManualInputWidget
from gui.misc.General import LoadingWidget

from gui.misc.Emitters import Emitter, ObjectEmitter


class VehicleView(QWidget):
	def __init__(self, parent=None):
		super(VehicleView, self).__init__(parent)

		self.va = VehicleAdministration(self)
		self.va.getVehicles()


		self.mainLayout = QGridLayout()
		self.mainLayout.setContentsMargins(0,0,0,0)
		self.mainLayout.setSpacing(0)

		self.mainLayout.setColumnStretch(0,settings.Settings.get("Vehicle", "width", "int"))
		self.mainLayout.setColumnStretch(1,100 - settings.Settings.get("Vehicle", "width", "int"))

		self.VehicleAdministrationView = VehicleList(self)
		self.VehicleAdministrationDetailView = VehicleDetail(self)
		self.timeChangeView = TimeChangeWidget()
		self.tourMessageInputView = MessageInputWidget()
		self.memberChooser = MemberChooserWidget()
		self.keyboardView = ManualInputWidget()
		self.loadingView = LoadingWidget()



		self.stackedWidget = QStackedWidget()
		
		self.stackedWidget.insertWidget(0, self.VehicleAdministrationView)
		self.stackedWidget.insertWidget(1, self.timeChangeView)
		self.stackedWidget.insertWidget(2, self.tourMessageInputView)
		self.stackedWidget.insertWidget(3, self.memberChooser)
		self.stackedWidget.insertWidget(4, self.keyboardView)
		self.stackedWidget.insertWidget(5, self.loadingView)
		self.stackedWidget.setCurrentIndex(0)
		self.stackedWidget.setContentsMargins(0,0,0,0)

		self.previousSite = 0


		self.timeChangeView.okSignal.connect(self.changedTime)
		self.tourMessageInputView.okSignal.connect(self.displayVehicleList)
		self.VehicleAdministrationDetailView.newTourWidget.needKeyBoardSignal.connect(self.raiseTourMessageInput)
		self.VehicleAdministrationDetailView.newTourWidget.emptyTourSignal.connect(self.emptyNewTour)
		self.VehicleAdministrationDetailView.newTourWidget.sendToTourSignal.connect(self.updateVehicles)

		self.VehicleAdministrationView.updateActiveTours.connect(self.VehicleAdministrationDetailView.tourListWidget.updateActive)
		self.VehicleAdministrationDetailView.tourListWidget.changeTourSignal.connect(self.updateView)


		self.mainLayout.addWidget(self.stackedWidget, 1, 0)
		self.mainLayout.addWidget(self.VehicleAdministrationDetailView, 1, 1)

		self.setLayout(self.mainLayout)

		Emitter.getInstance("UpdateVehicleList").connect(self.updateView)
		ObjectEmitter.getInstance("raiseKeyboard").connect(self.raiseKeyboard)

		Emitter.getInstance("KeyboardOk").connect(self.previousView)
		Emitter.getInstance("KeyboardBack").connect(self.previousView)
		Emitter.getInstance("ShowVehicleView").connect(self.displayVehicleList)
		ObjectEmitter.getInstance("ShowMemberChooserView").connect(self.showMemberChooser)
		Emitter.getInstance("loading").connect(self.showLoadingScreen)
		Emitter.getInstance("loadingReady").connect(self.displayVehicleList)

		Emitter.getInstance("ShowNormalView").connect(self.displayVehicleList)


		self.timer = QTimer(self)
		self.timer.timeout.connect(self.updateVehicles)
		self.timer.start(30000) #TODO make customizable

		self.updateVehicles()

	def updateVehicles(self):
		# t = threading.Thread(target=self.va.update)
		# t.start()
		self.va.update()
		self.updateView()

	def updateView(self):
		self.VehicleAdministrationView.update()

	def displayChangeTimeView(self, time, reason):
		self.timeChangeView.changeParams(time, reason)
		self.previousSite = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(1)


	def changedTime(self):
		self.displayVehicleList()
		self.VehicleAdministrationDetailView.newTourWidget.timeListView.changeTimeCallBack(self.timeChangeView.modified, self.timeChangeView.time)

	def raiseTourMessageInput(self, qTextField):
		self.tourMessageInputView.connectToTextField(qTextField)
		self.previousSite = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(2)

	def raiseKeyboard(self, qTextField):
		self.keyboardView.connectToTextField(qTextField)
		self.previousSite = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(4)

	def showMemberChooser(self, vehicle=None):
		self.memberChooser.showMembers(vehicle)
		self.previousSite = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(3)

	def memberChosen(self, member, vehicleLine):
		# self.displayVehicleList()
		self.VehicleAdministrationDetailView.newTourWidget.vehicleList.memberChooserCallBack(member, vehicleLine)
		self.previousView()

	def displayVehicleList(self):
		self.previousSite = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(0)

	def vehicleClicked(self, clickedVehicle):
		self.VehicleAdministrationDetailView.vehicleClicked(clickedVehicle)

	def showLoadingScreen(self):
		self.previousSite = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(5)
		QApplication.processEvents()

	def emptyNewTour(self):
		self.displayVehicleList()
		self.VehicleAdministrationView.update()
		self.tourMessageInputView.emptySelection()

	def previousView(self):
		temp = self.stackedWidget.currentIndex()
		self.stackedWidget.setCurrentIndex(self.previousSite)
		self.previousSite = temp
