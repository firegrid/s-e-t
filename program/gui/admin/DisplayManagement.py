from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

from subprocess import Popen, PIPE

import settings
import style
from general.functions import writeLog

from gui.misc.Buttons import AppButton
from gui.misc.Layout import ScrollableArea


class DisplayControlView(QWidget):

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.mainLayout = QVBoxLayout(self)
		statusLayoutWidget = QWidget()
		self.statusLayout = QVBoxLayout(statusLayoutWidget)

		self.scrollableArea = ScrollableArea()
		self.scrollableArea.setInnerWidget(statusLayoutWidget)

		self.restartAllButton = AppButton("Alle neustarten")
		self.refreshButton = AppButton("Aktualisieren")
		self.restartAllButton.clicked.connect(self.restartAll)
		self.refreshButton.clicked.connect(self.refresh)

		self.statuswidgets = StatusWidget.getInstances()
		for w in self.statuswidgets:
			self.statusLayout.addWidget(w)

		self.mainLayout.addWidget(self.restartAllButton)
		self.mainLayout.addWidget(self.refreshButton)
		self.mainLayout.addWidget(self.scrollableArea)



	def restartAll(self):
		for w in self.statuswidgets:
			w.restartDisplay()

	def refresh(self):
		for w in self.statuswidgets:
			w.getStatus()




class StatusWidget(QWidget):
	def __init__(self, ip, name, status):
		super(StatusWidget, self).__init__()

		self.mainLayout = QHBoxLayout(self)

		self.ip = ip
		self.name = name
		self.status = status

		self.statusText = ["offline", "online"]

		self.titleLabel = QLabel(self.name)
		self.titleLabel.setFont(style.Fonts.h2)
		self.restartButton = AppButton("neustarten")
		self.statusLabel = None
		self.statusLabel = QLabel(self.statusText[self.status])

		self.restartButton.clicked.connect(self.restartDisplay)

		self.mainLayout.addWidget(self.titleLabel)
		self.mainLayout.addWidget(self.statusLabel)
		self.mainLayout.addWidget(self.restartButton)

	def restartDisplay(self):
		p = Popen(["scripts/autorestart/autorestart", "ip", self.ip], stdout=PIPE)
		self.statusLabel.setText("wird neu gestartet")
	def getStatus(self):
		p = Popen(["scripts/autorestart/autorestart", "check", self.ip], stdout=PIPE)
		for line in p.stdout:
			ip,name,status = line.decode().split("|")
			self.status = int(status.strip())
		self.statusLabel.setText(self.statusText[self.status])

	def getInstances():
		list = []

		p = Popen(["scripts/autorestart/autorestart", "check", "all"], stdout=PIPE)

		for line in p.stdout:
			ip,name,status = line.decode().split("|")
			list.append(StatusWidget(ip, name, int(status.strip())))

		return list


		