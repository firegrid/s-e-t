from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style
from general.functions import writeLog
from gui.QtFunctions import *

from gui.misc.Layout import ScrollableArea
from gui.misc.MessageInputWidget import TourMessageBox
from gui.misc.Buttons import ImportantButton, ToggleButton, DescriptiveButton
from models.base import Database
from models.vehicle import Vehicle
from models.equipment import Equipment



class EquipmentView(QWidget):
	returnSignal = pyqtSignal()
	needKeyBoardSignal = pyqtSignal(QTextEdit)

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.currentVehicle = None
		self.mainLayout = QHBoxLayout(self)

		self.equipmentWidget = QWidget()

		self.equipmentLayout = QVBoxLayout(self.equipmentWidget)
		self.equipmentLayout.addItem(style.Layout.hSpacer)


		self.scroll = ScrollableArea()

		self.scroll.setInnerWidget(self.equipmentWidget)
		

		vehicleListW = QWidget()
		self.vehicleListLayout = QVBoxLayout(vehicleListW)

		self.vList = []

		Database.connectDB()

		for v in Vehicle.select().where(Vehicle.active==True).order_by(Vehicle.sorting.asc()):
			vehicleWidget = VehicleItem(v)
			vehicleWidget.clicked.connect(self.changeVehicle)
			self.vList.append(vehicleWidget)
			self.vehicleListLayout.addWidget(vehicleWidget)
		
		Database.closeDB()

		self.scrollableArea = ScrollableArea()
		self.scrollableArea.setInnerWidget(vehicleListW)

		self.mainLayout.addWidget(self.scrollableArea)
		self.mainLayout.addWidget(self.scroll)


	def deleteEquipment(self):
		Database.connectDB()
		Equipment.delete().where(Equipment.identifier == self.sender().model.identifier).execute()
		Database.closeDB()

		writeLog("admin.log", str(self.sender().model.identifier) + " - " + self.sender().model.name + " ("+str(self.sender().model.vehicle) + ") wurde gelöscht")


		self.renewEquipmentScreen()

		

	def newEquipment(self):
		text = self.inputTemp.toPlainText()
		
		e = Equipment()
		e.vehicle = self.currentVehicle.identifier
		e.name = text
		e.ready = 1

		Database.connectDB()
		e.save(force_insert=True)
		Database.closeDB()

		writeLog("admin.log", text + " ("+str(self.currentVehicle.identifier) + ") wurde hinzugefügt")

		self.renewEquipmentScreen()
		

	def changeVehicle(self):
		self.currentVehicle = self.sender().vehicle
		self.renewEquipmentScreen()
		

	def renewEquipmentScreen(self):
		clearLayout(self.equipmentLayout)
		self.equipmentLayout.addItem(style.Layout.hSpacer)

		self.inputWidget = ImportantButton("Neues Gerät hinzufügen")
		self.inputTemp = QTextEdit()
		self.inputWidget.clicked.connect(lambda:self.needKeyBoardSignal.emit(self.inputTemp))

		self.equipmentLayout.addWidget(self.inputWidget)

		Database.connectDB()
		for e in Equipment.select().where(Equipment.vehicle == self.currentVehicle.identifier).order_by(Equipment.name.asc()):
			eButton = DescriptiveButton(e.name, "löschen", model=e, layout=1)
			eButton.clicked.connect(self.deleteEquipment)

			self.equipmentLayout.addWidget(eButton)
		Database.closeDB()


		self.equipmentLayout.addItem(style.Layout.vSpacer)




class VehicleItem(ToggleButton):
	def __init__(self, vehicle, parent=None):
		ToggleButton.__init__(self, vehicle.longname, parent=parent)
		self.setAutoExclusive(True)
		self.vehicle = vehicle


