from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style
from general.functions import writeLog

from models.base import Database
from models.member import Member

from gui.misc.General import ToggleWidget
from gui.misc.Layout import ScrollableArea, ColLayout
from gui.misc.Forms import ClickableLineEdit
from gui.misc.Buttons import AppButton, PictureButton
from gui.misc.Dialogs import AppDialogBox
from gui.misc.VirtualKeyboard import VirtualKeyboard
from gui.misc.Emitters import ModelEmitter

from logic.VehicleDistributionServices.ServiceData.Members import Member as MemberData
from logic.VehicleAdministration import VehicleAdministration as VA



class MemberItem(ToggleWidget):
	def __init__(self, member, parent=None):
		ToggleWidget.__init__(self, None, QWidget(), QWidget(), parent=parent)

		self.member = member

		self.permissionSignal = ModelEmitter.getInstance("permission")

		self.closedLayout = QHBoxLayout(self.closedWidget)
		self.closedLayout.setSpacing(0)
		self.closedLayout.setContentsMargins(0,0,0,0)
		self.nameLabel = QLabel(self.member.fullFWName())

		self.closedLayout.addWidget(self.nameLabel)

		self.controlLayout = QHBoxLayout(self.openWidget)
		self.controlLayout.setSpacing(0)
		self.controlLayout.setContentsMargins(0,0,0,0)

		updateButton = PictureButton(":/media/images/misc/refresh.png", "misc")
		updateButton.clicked.connect(self.updateMember)
		permissionsButton = PictureButton(":/media/images/misc/lock.png", "misc")
		permissionsButton.clicked.connect(self.changePermissions)
		deleteButton = PictureButton(":/media/images/misc/close.png", "misc")
		deleteButton.clicked.connect(self.deleteMember)

		self.controlLayout.addWidget(QLabel(str(self.member.stbn)))
		self.controlLayout.addWidget(updateButton)
		self.controlLayout.addWidget(permissionsButton)
		self.controlLayout.addWidget(deleteButton)

		self.mainLayout.setSpacing(0)
		self.mainLayout.setContentsMargins(5,5,5,5)


		self.setStyleSheet("""
				QFrame{
					background-color: """ + style.Color.scheme.allowedMemberColor + """;
					border-radius: 5px;
					padding: 0;
				}""")

	def changePermissions(self):
		self.permissionSignal.emit(self.member)
		self.toggle()

	def updateMember(self):
		#TODO check fdisk
		m = MemberData()
		VA.instance.getMember(self.member.stbn, m)
		self.member = m.model
		self.nameLabel.setText(m.model.fullFWName())

		writeLog("admin.log", m.model.fullFWName() + " wurde upgedated")

		self.toggle()

	def deleteMember(self):
		yesAnswer = AppDialogBox.question("Sicher?", "Soll '" + self.member.fullFWName() + "' wirklich gelöscht werden?")

		if yesAnswer:
			Database.connectDB()
			m = self.member
			m.status = 6
			m.save()
			Database.closeDB()

			writeLog("admin.log", m.fullFWName() + " wurde gelöscht")

			self.deleteLater()
		else:
			self.toggle()


class UserManagerView(QWidget):
	#TODO add keyboard for number input (not choose from list)
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.mainLayout = QVBoxLayout(self)

		self.newUserLayout = QHBoxLayout()
		self.newUserLayout.addWidget(QLabel("Neues Mitglied: "))

		self.newUserLine = ClickableLineEdit()
		self.newUserLine.setPlaceholderText("STBNr")
		self.newUserLine.clicked.connect(self.showKeyBoard)
		self.newUserLine.setValidator(QIntValidator())
		self.newUserLine.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))
		self.newUserLine.setFixedWidth(100)
		self.newUserLine.setFixedHeight(40)
		newUserButton = AppButton("Mitglied hinzufügen")
		newUserButton.clicked.connect(self.newMember)
		self.newUserLayout.addWidget(self.newUserLine)
		self.newUserLayout.addWidget(newUserButton)
		self.newUserLayout.addItem(style.Layout.hSpacer)

		self.mainLayout.addLayout(self.newUserLayout)

		self.stackedWidget = QStackedWidget()
		self.keyboardView = QWidget()
		self.keyboardLayout = QVBoxLayout(self.keyboardView )

		Database.connectDB()

		memberDataList = [m for m in Member.select().where(Member.status << [1]).order_by(Member.stbn.asc())]
		self.memberList = ColLayout(4, memberDataList, MemberItem) # TODO make customizable

		Database.closeDB()

		self.scrollableArea = ScrollableArea()
		self.scrollableArea.setInnerWidget(self.memberList)


		self.stackedWidget.insertWidget(0, self.scrollableArea)
		self.stackedWidget.insertWidget(1, self.keyboardView)

		self.mainLayout.addWidget(self.stackedWidget)

	
	def showKeyBoard(self):
		kv = VirtualKeyboard.getInstance()
		if(self.keyboardLayout.count() == 0):
			self.keyboardLayout.insertWidget(0, kv)

		kv.showNumPad()
		try: kv.change.disconnect() 
		except Exception: pass
		kv.connectToTextField(self.newUserLine)
		self.stackedWidget.setCurrentIndex(1)

	def showEvent(self, arg):
		self.stackedWidget.setCurrentIndex(0)
		QWidget.showEvent(self, arg)

	def newMember(self):
		if self.newUserLine.text().__len__() == 0 :
			return

		m = MemberData()
		VA.instance.getMember(self.newUserLine.text(), m)
		Database.connectDB()
		m.model.status = 1
		m.model.save()
		Database.closeDB()

		writeLog("admin.log", m.model.fullFWName() + " wurde hinzugefügt")

		memberItem = MemberItem(m.model)
		
		self.memberList.addItem(memberItem)

		self.newUserLine.setText("")

		ModelEmitter.getInstance("permission").emit(m.model)

