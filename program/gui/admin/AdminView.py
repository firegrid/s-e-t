from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style

from gui.misc.Buttons import AppButton, ImportantButton
from gui.misc.Dialogs import AppDialogBox
from gui.misc.Emitters import ModelEmitter, Emitter
from gui.misc.MessageInputWidget import ManualInputWidget
from gui.misc.Forms import TouchCheckBox
from gui.misc.Dialogs import AppDialogBox

from gui.admin.UserManagement import UserManagerView
from gui.admin.PermissionManagement import PermissionView
from gui.admin.SnippetManagement import SnippetView
from gui.admin.EquipmentManagement import EquipmentView
from gui.admin.DisplayManagement import DisplayControlView
# from gui.admin.Statistics import StatisticView #TODO


from logic.Curl import Curl

import subprocess


# class Pages:
# 	Welcome = -1
# 	User = -1
# 	VehiclePermissions = -1
# 	Snippets = -1
# 	Equipment = -1
# 	Keyboard = -1
# 	# Statistics = 5


class AdminView(QWidget):
	toVehicleView = pyqtSignal()

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QHBoxLayout(self)
		self.stackedWidget = QStackedWidget()

		self.loginView = ManualInputWidget(2)
		self.welcomePage = WelcomeView()
		self.userList = UserManagerView()
		self.vehiclePermissionView = PermissionView()
		self.snippetView = SnippetView()
		self.equipmentView = EquipmentView()
		self.keyboardView = ManualInputWidget()
		self.displayControlView = DisplayControlView()
		# self.statisticView = StatisticView() #TODO

		self.pages = {}

		self.permissionSignal = ModelEmitter.getInstance("permission")
		self.permissionSignal.connect(self.showPermissions)		


		self.pages["Login"] = self.stackedWidget.addWidget(self.loginView)
		self.pages["Welcome"] = self.stackedWidget.addWidget(self.welcomePage)
		self.pages["User"] = self.stackedWidget.addWidget(self.userList)
		self.pages["VehiclePermissions"] = self.stackedWidget.addWidget(self.vehiclePermissionView)
		self.pages["Snippets"] = self.stackedWidget.addWidget(self.snippetView)
		self.pages["Equipment"] = self.stackedWidget.addWidget(self.equipmentView)
		self.pages["Keyboard"] = self.stackedWidget.addWidget(self.keyboardView)
		self.pages["DisplayControl"] = self.stackedWidget.addWidget(self.displayControlView)
		#self.pages["Statistics"] = self.stackedWidget.addWidget(self.statisticView) #TODO
		
		self.vehiclePermissionView.returnSignal.connect(lambda:self.changePage(self.pages["User"]))

		#menu
		self.menu = QWidget()
		self.menuLayout = QVBoxLayout(self.menu)

		self.userButton = AppButton("Mitglieder Verwaltung")
		self.snippetButton = AppButton("Snippet Verwaltung")
		self.equipmentButton = AppButton("Equipment Verwaltung")
		self.displayButton = AppButton("Display Verwaltung")
		#self.statisticButton = AppButton("Statistiken") #TODO
		self.backButton = ImportantButton("Zurück")

		self.userButton.clicked.connect(lambda:self.changePage(self.pages["User"]))
		self.snippetButton.clicked.connect(lambda:self.changePage(self.pages["Snippets"]))
		self.equipmentButton.clicked.connect(lambda:self.changePage(self.pages["Equipment"]))
		self.displayButton.clicked.connect(lambda:self.changePage(self.pages["DisplayControl"]))
		# self.statisticButton.clicked.connect(lambda:self.changePage(self.pages["Statistics"])) #TODO
		self.backButton.clicked.connect(self.leaveAdmin)

		self.equipmentView.needKeyBoardSignal.connect(self.showKeyboard)
		

		self.menuLayout.addWidget(self.userButton)
		self.menuLayout.addWidget(self.snippetButton)
		self.menuLayout.addWidget(self.equipmentButton)
		self.menuLayout.addWidget(self.displayButton)
		# self.menuLayout.addWidget(self.statisticButton) #TODO
		self.menuLayout.addWidget(self.backButton)
		self.menuLayout.addItem(style.Layout.vSpacer)

		self.mainLayout.addWidget(self.stackedWidget)
		self.mainLayout.addWidget(self.menu)
		self.menu.hide()

	def showLoginView(self):
		self.menu.hide()
		self.loginField = QLineEdit()

		self.loginView.connectToTextField(self.loginField)
		Emitter.getInstance("KeyboardBack").connect(self.leaveAdmin)
		Emitter.getInstance("KeyboardOk").connect(self.login)

		self.changePage(self.pages["Login"])

	def login(self):
		self.disconnectSignals()

		if self.loginField.text() == settings.Settings.get("Application", "adminLogin"):
			self.menu.show()
			self.checkForUpdates()
			self.changePage(self.pages["Welcome"])
		else:
			self.leaveAdmin()

	def leaveAdmin(self):
		self.disconnectSignals()
		self.toVehicleView.emit()

	def disconnectSignals(self):
		Emitter.getInstance("KeyboardBack").disconnect(self.leaveAdmin)
		Emitter.getInstance("KeyboardOk").disconnect(self.login)
		Emitter.getInstance("KeyboardBack").disconnect(self.previousView)
		Emitter.getInstance("KeyboardOk").disconnect(self.keyboardOk)

	def changePage(self, page):
		self.stackedWidget.setCurrentIndex(page)

	def showPermissions(self, member):
		self.changePage(self.pages["VehiclePermissions"])
		self.vehiclePermissionView.setMember(member)

	def checkForUpdates(self):
		if settings.Settings.get("Application", "allowUpdates", "bool"):
			Curl.curlCall("", settings.Settings.get("Static", "updateSite"), self.updateCallBack)


	def updateCallBack(self, newVersion):
		f = open("../.project/version","r")
		currentVersion = f.read()

		if currentVersion != newVersion:
			yesAnswer = AppDialogBox.question("Neue Updates verfügbar", "Wollen Sie das Status Erfassungsterminal auf Version " +  newVersion + " updaten?\nDerzeit befindet sich die Version " + currentVersion + " auf dem Computer.")
			if yesAnswer:
				subprocess.call("../.control/update")
				import os, sys
				script = sys.executable
				os.execl(script, script, * sys.argv)

	def showKeyboard(self, qText):
		self.keyboardView.connectToTextField(qText)
		Emitter.getInstance("KeyboardBack").connect(self.previousView)
		Emitter.getInstance("KeyboardOk").connect(self.keyboardOk)
		self.stackedWidget.setCurrentIndex(self.pages["Keyboard"])

	def keyboardOk(self):
		#only used for equipment... maybe change for other things.. like textsnippet input..
		#	var that knows last visited page + entry slot
		Emitter.getInstance("KeyboardBack").disconnect(self.previousView)
		Emitter.getInstance("KeyboardOk").disconnect(self.keyboardOk)
		self.equipmentView.newEquipment()
		self.changePage(self.pages["Equipment"])

	def previousView(self):
		Emitter.getInstance("KeyboardBack").disconnect(self.previousView)
		Emitter.getInstance("KeyboardOk").disconnect(self.keyboardOk)
		self.changePage(self.pages["Equipment"]) #TODO


	



class WelcomeView(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QVBoxLayout(self)
		welcomeHeader = QLabel("Administrationsbereich des Status Erfassungsterminals!")
		welcomeHeader.setFont(style.Fonts.h1)
		welcomeText = QLabel("Änderungen die hier gemacht werden, werden mitprotokolliert.\n\nEntwicklung durch Freiwillige Feuerwehr Achau:\nLucas Dworschak (lucas.dworschak@gmx.at)\nWolfgang Satra (satraw@gmx.at)")
		self.mainLayout.addWidget(welcomeHeader)
		self.mainLayout.addWidget(welcomeText)


		
		self.mainLayout.addItem(style.Layout.vSpacer)

		trainingsheader = QLabel("Trainingsmodus")
		trainingsheader.setFont(style.Fonts.h1)

		trainingsstate = settings.Settings.get("Application", "training", "bool")

		if settings.Settings.get("Application", "training", "bool"):
			self.trainingsmode = AppButton("Trainingsmodus abschalten")
		else:
			self.trainingsmode = AppButton("Trainingsmodus einschalten")

		if settings.Settings.get("Application", "url122", "str") == settings.Settings.get("Application", "realurl122", "str"):
			self.mockservermode = AppButton("Daten nicht mehr von 122.at holen")
		else:
			self.mockservermode = AppButton("Daten wieder von 122.at holen")

		self.trainingsmode.clicked.connect(self.changeTraining)
		self.mockservermode.clicked.connect(self.changeMockServer)

		self.mainLayout.addWidget(trainingsheader)
		self.mainLayout.addWidget(self.trainingsmode)
		self.mainLayout.addWidget(self.mockservermode)

		# changelog = QTextBrowser()
		# f = open("../changelog","r")

		# changelog.setText(f.read())

		# self.mainLayout.addWidget(changelog)
		self.mainLayout.addItem(style.Layout.vSpacer)

	def changeTraining(self):
		nowMode = settings.Settings.get("Application", "training", "bool")

		if settings.Settings.get("Application", "training", "bool"):
			answer = AppDialogBox.question("Trainingsmodus ändern", "Wollen Sie wirklich den Trainingsmodus abschalten")
		else:
			answer = AppDialogBox.question("Trainingsmodus ändern", "Wollen Sie wirklich den Trainingsmodus aktivieren")

		if answer == True:
			# change the nowMode
			settings.Settings.set("Application", "training", not nowMode)
			# start application new
			import os, sys
			script = sys.executable
			os.execl(script, script, * sys.argv)

	def changeMockServer(self):
		server = settings.Settings.get("Application", "realurl122", "str");
		if settings.Settings.get("Application", "url122", "str") == settings.Settings.get("Application", "realurl122", "str"):
			server = settings.Settings.get("Application", "mockurl122", "str")

		answer = AppDialogBox.question("Datenserver ändern", "Wirklich ändern?")

		if answer == True:
			# change the nowMode
			settings.Settings.set("Application", "url122", server)
			# start application new
			import os, sys
			script = sys.executable
			os.execl(script, script, * sys.argv)


