from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style
from general.functions import writeLog

from gui.misc.General import ToggleWidget
from gui.misc.Layout import ScrollableArea, ColLayout
from gui.misc.Emitters import  Emitter, ModelEmitter
from gui.misc.Buttons import AppButton, ImportantButton, PictureButton
from gui.misc.Dialogs import AppDialogBox
from gui.misc.Forms import ClickableTextEdit, ClickableLineEdit, TouchCheckBox, TouchComboBox
from gui.misc.VirtualKeyboard import VirtualKeyboard

from models.base import Database
from models.tour import TextSnippets

class SnippetView(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QVBoxLayout(self)


		self.snippetEditSignal = ModelEmitter.getInstance("snippetEdit")
		self.snippetEditSignal.connect(self.snippetEditSlot)

		self.stackedWidget = QStackedWidget()

		self.snippetList = SnippetListView()
		self.snippetList.newSnippetSignal.connect(self.snippetNewSlot)
		self.snippetEdit = SnippetEditView()
		self.snippetEdit.returnSignal.connect(self.snippetListSlot)

		self.stackedWidget.insertWidget(0, self.snippetList)
		self.stackedWidget.insertWidget(1, self.snippetEdit)

		self.stackedWidget.setCurrentIndex(0)

		self.mainLayout.addWidget(self.stackedWidget)

	def snippetNewSlot(self):
		self.stackedWidget.setCurrentIndex(1)
		self.snippetEdit.newSnippet()

	def snippetEditSlot(self, m):
		self.stackedWidget.setCurrentIndex(1)
		self.snippetEdit.editSnippet(m)

	def snippetListSlot(self):
		self.stackedWidget.setCurrentIndex(0)
		self.snippetList.refresh()

	def showEvent(self, arg):
		self.stackedWidget.setCurrentIndex(0)
		QWidget.showEvent(self, arg)
		





class SnippetListView(QWidget):
	newSnippetSignal = pyqtSignal()
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.mainLayout = QVBoxLayout(self)

		self.snippetRefreshSignal = Emitter.getInstance("snippetListRefresh")
		self.snippetRefreshSignal.connect(self.refresh)

		newButton = AppButton("Neues Snippet")
		newButton.clicked.connect(self.newSnippetSignal.emit)
		self.mainLayout.addWidget(newButton)

		infoTitle = QLabel("Aktions Snippets")
		infoTitle.setFont(style.Fonts.h1)
		self.mainLayout.addWidget(infoTitle)

		Database.connectDB()

		snippetList = [sn for sn in TextSnippets.select().where(TextSnippets.group>10).order_by(TextSnippets.title.asc())]

		self.snippetInfoWidget = ColLayout(3, snippetList, SnippetItem) #todo make cols customizeable
		scrollableArea = ScrollableArea()
		scrollableArea.setInnerWidget(self.snippetInfoWidget)

		self.mainLayout.addWidget(scrollableArea)


		self.mainLayout.addItem(style.Layout.vSpacer)


		contactTitle = QLabel("Zusatzinfo Snippets")
		contactTitle.setFont(style.Fonts.h1)
		self.mainLayout.addWidget(contactTitle)
		snippetContactList = [sn for sn in TextSnippets.select().where(TextSnippets.group<=10).order_by(TextSnippets.title.asc())]

		Database.closeDB()

		self.snippetContactWidget = ColLayout(3, snippetContactList, SnippetItem) #todo make cols customizeable

		scrollableArea = ScrollableArea()
		scrollableArea.setInnerWidget(self.snippetContactWidget)

		self.mainLayout.addWidget(scrollableArea)

	def refresh(self):
		# todo is there a more efficient way??
		Database.connectDB()

		snippetList = [sn for sn in TextSnippets.select().where(TextSnippets.group>10).order_by(TextSnippets.title.asc())]
		self.snippetInfoWidget.refresh(snippetList)

		snippetContactList = [sn for sn in TextSnippets.select().where(TextSnippets.group<=10).order_by(TextSnippets.title.asc())]
		self.snippetContactWidget.refresh(snippetContactList)

		Database.closeDB()


class SnippetItem(QFrame):
	changeSignal = pyqtSignal()
	def __init__(self, snippet, parent=None):
		QFrame.__init__(self, parent)

		self.snippet = snippet

		self.snippetEditSignal = ModelEmitter.getInstance("snippetEdit")

		self.mainLayout = QHBoxLayout(self)
		self.mainLayout.setSpacing(5)
		self.mainLayout.setContentsMargins(5,5,5,5)

		editButton = PictureButton(":/media/images/misc/change.png", "misc")
		editButton.clicked.connect(lambda: self.snippetEditSignal.emit(self.snippet))
		self.deactivateButton = PictureButton(":/media/images/misc/deactivate.png", "misc")
		self.deactivateButton.clicked.connect(self.deactivateSnippet)
		self.activateButton = PictureButton(":/media/images/misc/activate.png", "misc")
		self.activateButton.clicked.connect(self.activateSnippet)

		self.deleteButton = PictureButton(":/media/images/misc/close.png", "misc")
		self.deleteButton.clicked.connect(self.deleteSnippet)

		self.mainLayout.addWidget(QLabel(self.snippet.title))
		self.mainLayout.addItem(style.Layout.hSpacer)
		self.mainLayout.addWidget(editButton)
		self.mainLayout.addWidget(self.deactivateButton)
		self.mainLayout.addWidget(self.activateButton)
		self.mainLayout.addWidget(self.deleteButton)

		self.updateStyle()

		self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed))


	def updateStyle(self):
		if self.snippet.active:
			self.activateButton.hide()
			self.deactivateButton.show()
			self.setStyleSheet("""
				background-color: """ + style.Color.scheme.activatedTextSnippet + """;
				border-radius: 5px;
			""")
		else:
			self.activateButton.show()
			self.deactivateButton.hide()
			self.setStyleSheet("""
				background-color: """ + style.Color.scheme.deactivatedTextSnippet + """;
				border-radius: 5px;
			""")


	def activateSnippet(self):
		Database.connectDB()
		self.snippet.active = True
		self.snippet.save()
		Database.closeDB()

		writeLog("admin.log", self.snippet.title + " wurde aktiviert")
		
		self.updateStyle()

	def deactivateSnippet(self):
		Database.connectDB()
		self.snippet.active = False
		self.snippet.save()
		Database.closeDB()

		writeLog("admin.log", self.snippet.title + " wurde deaktiviert")

		
		self.updateStyle()

	def deleteSnippet(self):
		writeLog("admin.log", self.snippet.title + " wurde gelöscht")

		self.snippet.delete_instance()
		
		Emitter.getInstance("snippetListRefresh").emit()




class SnippetEditView(QFrame):
	returnSignal = pyqtSignal()

	def __init__(self, parent=None):
		QFrame.__init__(self, parent)

		self.snippet = None
		self.groupsDict = {}


		self.mainLayout = QVBoxLayout(self)
		self.stackedWidget = QStackedWidget()

		self.touchHelperWidget = QWidget()

		self.touchHelper = QVBoxLayout(self.touchHelperWidget)
		self.touchHelperTitle = QLabel("")
		self.touchHelperTitle.setFont(style.Fonts.h1)
		self.touchHelperAccept = ImportantButton("Übernehmen")
		self.touchHelperAccept.clicked.connect(self.returnToForm)
		self.touchHelper.addWidget(self.touchHelperTitle)
		self.touchHelper.addWidget(self.touchHelperAccept)
		self.touchHelper.addItem(style.Layout.vSpacer)

		self.formWidget = QWidget()
		self.form = QGridLayout(self.formWidget)

		self.form.addWidget(QLabel("Titel"), 0, 0)
		self.titleEdit = ClickableLineEdit()
		self.titleEdit.setProperty("header", "Titel")
		self.titleEdit.clicked.connect(lambda: self.showKeyBoard(self.titleEdit))

		self.form.addWidget(self.titleEdit, 0, 1)

		self.form.addWidget(QLabel("Nachricht"), 1, 0)
		self.messageEdit = ClickableTextEdit()
		self.messageEdit.setProperty("header", "Nachricht")
		self.messageEdit.clicked.connect(lambda: self.showKeyBoard(self.messageEdit))
		self.form.addWidget(self.messageEdit, 1, 1)

		self.form.addWidget(QLabel("Snippet Art"), 2, 0)
		self.group = TouchComboBox()
		self.groupsDict[11] = 0
		self.group.insertItem(0, "Dienstfahrt", 11)
		self.groupsDict[12] = 1
		self.group.insertItem(1, "Brandsicherheitswache", 12)
		self.groupsDict[1] = 2
		self.group.insertItem(2, "Erreichbarkeit", 1)

		self.form.addWidget(self.group, 2, 1)

		# self.form.addWidget(QLabel("Sticky"), 3, 0) # TODO Remove or use stickyies
		# self.sticky = TouchCheckBox()
		# self.form.addWidget(self.sticky, 3, 1)

		self.form.addItem(style.Layout.vSpacer)



		self.saveButton = ImportantButton("Speichern")
		self.saveButton.clicked.connect(self.save)
		self.form.addWidget(self.saveButton)

		self.stackedWidget.insertWidget(0, self.formWidget)
		self.stackedWidget.insertWidget(1, self.touchHelperWidget)
		self.mainLayout.addWidget(self.stackedWidget)

	def showKeyBoard(self, field):
		self.touchHelperTitle.setText(field.property("header") + ":")
		kv = VirtualKeyboard.getInstance()
		if(self.touchHelper.count() == 3):
			self.touchHelper.insertWidget(1, kv)

		kv.showNormal()
		try: kv.change.disconnect() 
		except Exception: pass
		kv.connectToTextField(field)
		self.stackedWidget.setCurrentIndex(1)


	def returnToForm(self):
		self.stackedWidget.setCurrentIndex(0)
		if self.titleEdit.text().__len__() > 0 and self.messageEdit.toPlainText().__len__() <= 0:
			self.messageEdit.setText(self.titleEdit.text())

	def showEvent(self, arg):
		self.stackedWidget.setCurrentIndex(0)
		QWidget.showEvent(self, arg)


	def editSnippet(self, m):
		self.snippet = m

		self.titleEdit.setText(self.snippet.title) 
		self.messageEdit.setText(self.snippet.message)
		self.group.setCurrentIndex(self.groupsDict[self.snippet.group])
		# self.sticky.setChecked(self.snippet.sticky)


	def newSnippet(self):
		self.snippet = None

		self.titleEdit.setText("") 
		self.messageEdit.setText("")
		self.group.setCurrentIndex(self.groupsDict[11])
		# self.sticky.setChecked(False)

	def save(self):

		if self.snippet == None:
			
			if self.titleEdit.text().__len__() == 0 or self.messageEdit.toPlainText().__len__() == 0:
				AppDialogBox.critical("Felder fehlen", "Das Snippet benötigt mindestens einen Titel und eine Nachricht")
				return
			Database.connectDB()
			self.snippet = TextSnippets(title = self.titleEdit.text(), message = self.messageEdit.toPlainText(), group = self.group.itemData(self.group.currentIndex()), sticky = False, active = True)
			writeLog("admin.log", "Snippet '" + self.snippet.title + "' wurde erstellt")
			self.snippet.save()
			Database.closeDB()
		else:

			if self.snippet.title != self.titleEdit.text():
				self.snippet.title = self.titleEdit.text()
				writeLog("admin.log", "Snippet Titel von id:" + str(self.snippet.identifier) + " auf " + self.snippet.title + " geändert")

			if self.snippet.message != self.messageEdit.toPlainText():
				self.snippet.message = self.messageEdit.toPlainText()
				writeLog("admin.log", "Snippet Nachricht von id:" + str(self.snippet.identifier) + " auf " + self.snippet.message + " geändert")

			if self.snippet.group != self.group.itemData(self.group.currentIndex()):
				self.snippet.group = self.group.itemData(self.group.currentIndex())
				writeLog("admin.log", "Snippet Gruppe von id:" + str(self.snippet.identifier) + " auf " + str(self.snippet.group) + " geändert")
			
			# if self.snippet.sticky != self.sticky.isChecked():
			# 	self.snippet.sticky = self.sticky.isChecked()
			# 	writeLog("admin.log", "Snippet Sticky von id:" + str(self.snippet.identifier) + " auf " + str(self.snippet.sticky) + " geändert")
			Database.connectDB()
			self.snippet.save()
			Database.closeDB()

		self.returnSignal.emit()

		
