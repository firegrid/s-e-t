from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings
import style
from general.functions import writeLog

from gui.misc.Layout import ScrollableArea
from gui.misc.Buttons import ImportantButton, ToggleButton
from models.base import Database
from models.vehicle import Vehicle
from models.member import VehiclePermission


class PermissionView(QWidget):
	returnSignal = pyqtSignal()

	def __init__(self, parent=None):
		QWidget.__init__(self, parent)

		self.member = None

		self.mainLayout = QVBoxLayout(self)

		vehicleListW = QWidget()
		self.vehicleListLayout = QVBoxLayout(vehicleListW)

		self.vList = []

		Database.connectDB()

		for v in Vehicle.select().where(Vehicle.active==True).order_by(Vehicle.sorting.asc()):
			vehicleWidget = VehicleItem(v)
			self.vList.append(vehicleWidget)
			self.vehicleListLayout.addWidget(vehicleWidget)
		
		Database.closeDB()

		self.scrollableArea = ScrollableArea()
		self.scrollableArea.setInnerWidget(vehicleListW)

		self.memberLabel = QLabel("")
		self.memberLabel.setFont(style.Fonts.h1)

		self.mainLayout.addWidget(self.memberLabel)		

		self.mainLayout.addWidget(self.scrollableArea)

		self.saveButton = ImportantButton("Speichern")
		self.saveButton.clicked.connect(self.save)

		self.mainLayout.addWidget(self.saveButton)

	def setMember(self, member):
		self.member = member
		self.memberLabel.setText("Fahrgenehmigung " + self.member.fullFWName())

		Database.connectDB()

		for w in self.vList:
			try:
				VehiclePermission.get(VehiclePermission.member == self.member.globalIdentifier, VehiclePermission.vehicle == w.vehicle.identifier)
				w.setChecked(True)
			except VehiclePermission.DoesNotExist:
				w.setChecked(False)

		Database.closeDB()

	def save(self):

		Database.connectDB()

		for w in self.vList:
			try:
				perm = VehiclePermission.get(VehiclePermission.member == self.member.globalIdentifier, VehiclePermission.vehicle == w.vehicle.identifier)
				if not w.isChecked():
					#delete Permission
					writeLog("admin.log", self.member.fullFWName() + "s Fahrgenehmigung wurde verändert -> - " + w.vehicle.longname)
					perm.delete_instance()	
			except VehiclePermission.DoesNotExist:
				if w.isChecked():
					#create Permission
					writeLog("admin.log", self.member.fullFWName() + "s Fahrgenehmigung wurde verändert -> + " + w.vehicle.longname)
					VehiclePermission.insert(member = self.member.globalIdentifier, vehicle = w.vehicle.identifier).execute()

		Database.closeDB()


		self.returnSignal.emit()



class VehicleItem(ToggleButton):
	def __init__(self, vehicle, parent=None):
		ToggleButton.__init__(self, vehicle.longname, parent=parent)
		self.setAutoExclusive(False)
		self.vehicle = vehicle


