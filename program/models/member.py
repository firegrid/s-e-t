from peewee import *
from models.base import BaseModel
from models.vehicle import Vehicle

class Member(BaseModel):
	stbn = IntegerField(unique = True, index = True) # id from Fdisk
	globalIdentifier = BigIntegerField(primary_key = True, unique = True, index = True) # id from 122.at
	rank = CharField() # eg. FM
	firstname = CharField()
	lastname = CharField()
	title = CharField() # eg. Ing.
	status = IntegerField(default = 0) # 0 == not activated | 1 == active | 2 == FJ | 3 == reserve | 4 == resigned | 5 == dead | 6 == not specified but gone
	gender = CharField() # m || f
	phonenumber = CharField()
	emailAdress = CharField()
	
	def fullFWName(self):
		return str(self.stbn) + " " + self.rank + " " + self.firstname + " " + self.lastname
	

class VehiclePermission(BaseModel):
	member = ForeignKeyField(Member, related_name="member")
	vehicle = ForeignKeyField(Vehicle, related_name="Vehicle")
	active = BooleanField(default = True)
