from peewee import *
from models.base import BaseModel
from models.member import Member
from models.tour import Tour

class Report(BaseModel):
	identifier = PrimaryKeyField()
	tour = ForeignKeyField(Tour, null = True)
	processed = BooleanField(default = False)

class MemberReport(BaseModel):
	member = ForeignKeyField(Member)
	report = ForeignKeyField(Report)
	begin = DateTimeField()
	end = DateTimeField()