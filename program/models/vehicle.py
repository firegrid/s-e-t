from peewee import *
from models.base import BaseModel

class Vehicle(BaseModel):
	identifier = BigIntegerField(primary_key = True, unique = True, index = True)
	eisIdentifier = IntegerField()
	shortname = CharField()
	longname = CharField()
	radiocallsign = CharField() # eg Tank Achau
	status = IntegerField()
	licensePlate = CharField()
	buildDate = DateField()
	acquisitionDate = DateField()
	removalDate = DateField()
	make = CharField() # automarke
	model = CharField()
	ready = BooleanField(default = True)
	active = BooleanField(default = True)
	sorting = IntegerField()