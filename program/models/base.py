from peewee import *
import settings

class Database:
	db = MySQLDatabase(settings.Settings.get("Database", "databasename"), host=settings.Settings.get("Database", "server"), user=settings.Settings.get("Database", "user"), passwd=settings.Settings.get("Database", "password"), port=settings.Settings.get("Database", "port", "int"), threadlocals=True)

	def connectDB():
		# print("connect")
		Database.db.connect()

	def closeDB():
		# print("disconnect")
		if not Database.db.is_closed():
			Database.db.close()


class BaseModel(Model):
	class Meta:
		database = Database.db

# Database.connectDB()
# Database.closeDB()
