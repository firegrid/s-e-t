from peewee import *
from models.base import BaseModel
from models.member import Member
from models.vehicle import Vehicle

class Tour(BaseModel):
	identifier = PrimaryKeyField()
	title = CharField()
	message = TextField()
	type = IntegerField()
	begin = DateTimeField()
	expectedEnd = DateTimeField()
	active = BooleanField()
	reserved = BooleanField()

class TourDriver(BaseModel):
	tour = ForeignKeyField(Tour, related_name="tour")
	vehicle = ForeignKeyField(Vehicle, related_name="vehicle")
	driver = ForeignKeyField(Member, related_name="driver")
	end = DateTimeField()


class TextSnippets(BaseModel):
	identifier = PrimaryKeyField()
	title = CharField() #text that is displayed as Snippet
	message = TextField() #text that is transmitted through the services
	group = IntegerField() #group > 10 == actiongroups; else == addinfo group |||| group 1 == communication group  |  group 11 == standard/dienstfahrt group | group 12 == bsw
	timesUsed = IntegerField()
	sticky = BooleanField() #if the snippets are ordered by timesUsed this makes sure that it is always on top
	active = BooleanField() #if the snippet is inactive (in case you dont want to delete it)
	default = BooleanField() #whether this field is selected by default or not