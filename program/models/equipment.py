from peewee import *
from models.base import BaseModel
from models.vehicle import Vehicle
from models.member import Member


class Equipment(BaseModel):
	identifier = IntegerField(primary_key = True)
	vehicle = ForeignKeyField(Vehicle)
	name = CharField()
	ready = BooleanField(default = True)
	responsiblePerson = ForeignKeyField(Member, null=True)
	temporary = BooleanField(default = False)


