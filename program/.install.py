#!/usr/bin/env python3

from peewee import *

import os
import configparser


if __name__ == '__main__':
	import sys

	args = sys.argv

	if(args.__len__() > 1):
		database = args[1]
		user = args[2]
		password = args[3]
		server = args[4]
		port = args[5]

		path = "config.ini"
		section = "Database"

		config = configparser.ConfigParser()
		if not os.path.exists(path):
			with open(path, "w") as f:
				config.write(f)
		config.read(path)

		config.set(section, "server", server)
		config.set(section, "user", user)
		config.set(section, "password", password)
		config.set(section, "databasename", database)
		config.set(section, "port", port)

		with open(path, "w") as f:
			config.write(f)

	from models.base import Database
	from models.member import *
	from models.report import *
	from models.tour import *
	from models.vehicle import *
	from models.equipment import *

	# Member.drop_table(True)
	# Vehicle.drop_table(True)
	# Tour.drop_table(True)
	# Report.drop_table(True)
	# VehiclePermission.drop_table(True)
	# MemberReport.drop_table(True)
	# TourDriver.drop_table(True)
	Database.connectDB()

	Member.create_table(True)
	Vehicle.create_table(True)
	Tour.create_table(True)
	Report.create_table(True)
	VehiclePermission.create_table(True)
	MemberReport.create_table(True)
	TourDriver.create_table(True)
	TextSnippets.create_table(True)
	Equipment.create_table(True)

	Database.closeDB()
