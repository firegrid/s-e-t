# from logic.VehicleDistributionServices.at122 import AT122Service
# from logic.VehicleDistributionServices.ServiceData.Mission import Mission, MissionStatus

# from logic.VehicleDistributionServices.ServiceData.Vehicles import Vehicle

# from logic.VehicleAdministration import VehicleAdministration

# import datetime



# va = VehicleAdministration()

# vehicles = []
# v = Vehicle()
# v.vID = 3498800206
# v.initialize()
# va.getMember(199)
# v.driver = va.member
# vehicles.append(v)

# v = Vehicle()
# v.vID = 3498800159
# v.initialize()
# va.getMember(169)
# v.driver = va.member
# vehicles.append(v)

# mission = Mission(vehicles,"test", datetime.datetime.now(), datetime.datetime.now(), MissionStatus.GenericTour)


# service = AT122Service(None)
# service.sendToTour(mission)


#####################################################
# debug 2


from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

# from ressources import ressources

# from gui.misc.Sound import Sound
# from gui.misc.Emitters import Emitter
# from functools import partial

class testWidget(QFrame):
	def __init__(self, parent=None):
		QFrame.__init__(self, parent)
		self.mainLayout = QVBoxLayout(self)
		self.setStyleSheet("""background-color: blue;""")

		
		x = QPushButton()
		x.clicked.connect(self.action)

		self.mainLayout.addWidget(x)

	def action(self):
		VehicleAdministration.instance.addError((self.test, "Lucas"))

	def test(self, var):
		print("hi " + var)




import time, sched

class VehicleAdministration:
	instance = None

	def __init__(self):
		VehicleAdministration.instance = self
		self.errorQueue = []
		self.scheduler = sched.scheduler(time.time, time.sleep)


	# adds an error to the errorQueue
	# function runs in another thread as the errorHandler
	# this function is the only way an error is added to the queue
	# input: list of tuples: (service, data)
	#	service = function that needs to be executed
	#	data = data which needs to be passed to the function
	def addError(self, error):
		self.errorQueue.append(error)

	# function checks if the error queue is empty.
	# if it isn't it calls the retryErrors function to retry every service
	# 	if the service call was successful it removes the successful elements from the errorqueue
	#	only this function can remove elements from self.errorQueue
	# everytime this function is executed it calls itself after a specified time interval
	# this function is due to the block of the scheduler.run() method in its own thread
	# 	so synchronization errors might appear.. 
	#	but since its only access to other threads is self.errorQueue, there shouldnt be any problems...
	#	TODO Test: since the Curl method is used here and this method is writing cookies in the cookie.txt file there might still be some errors if two threads are writing on this file..
	def errorHandler(self):
		print("checking for errors")
		
		if self.errorQueue.__len__() != 0:
			s = VehicleAdministration.retryErrors(self.errorQueue[:])
			self.errorQueue = [el for el in self.errorQueue if el not in s]

		if self.scheduler.empty():
			self.scheduler.enter(10, 1, self.errorHandler, ())
			self.scheduler.run()
			

	# try to use previously unavailable service
	# input: list of tuples: (service, data)
	#	service = function that needs to be executed
	#	data = data which needs to be passed to the function
	# returns a list with every service that was a success (in the format of one input item)
	def retryErrors(queue):
		print("retrying errors")
		successqueue = []

		for service, data in queue:
			try:
				service(data)
				successqueue.append((service, data))
			except DistributionError:
				# ignore future DistributionError because it is already in the queue
				pass

		return successqueue


# class testWidget2(QFrame):
# 	def __init__(self, parent=None):
# 		QFrame.__init__(self, parent)

# 		self.mainLayout = QVBoxLayout(self)
# 		self.mainLayout.addWidget(QLabel("hi"))
# 		self.setStyleSheet("""background-color: blue;""")
# 		self.mainLayout.addWidget(PictureButton("/media/images/vehicles/3498796672.png"))

# 	def mousePressEvent(self, event):
# 		self.mainLayout.insertWidget(1, keyboard.instance)
# 		super(testWidget2, self).mousePressEvent(event)

# class keyboard(QFrame):
# 	instance = None
# 	def __init__(self, parent=None):
# 		QFrame.__init__(self, parent)
# 		self.mainLayout = QVBoxLayout(self)
# 		self.mainLayout.addWidget(QLabel("keyboard"))
# 		self.setStyleSheet("""background-color: yellow;""")
# 		keyboard.instance = self

# import os

# class PictureButton(QPushButton):
# 	def __init__(self, pictureFile, specificNoPicPath="", parent=None):
# 		QPushButton.__init__(self, "", parent)

# 		pixmap = QtGui.QPixmap(pictureFile)
	
# 		if pixmap.isNull():
# 			pixmap = QtGui.QPixmap(os.getcwd() + settings.Settings.get("Application", "imagePath") + pictureFile)

# 		if pixmap.isNull():
# 			pixmap = QtGui.QPixmap(":/media/images/nopic.png")

# 		self.icon = QIcon(pixmap)
# 		self.setIcon(self.icon)
# 		self.setIconSize(QSize(100, 100))

# 		self.setStyleSheet("""AppButton{padding: 5px;}""")






#####################################################
# debug 3


# class A:
# 	def __init__(self):
# 		print("A")


# class B(A):
# 	def __init__(self):
# 		A.__init__(self)
# 		print("B")
		
		



# B()













import sys

import threading

app = QApplication(sys.argv)

s = testWidget()





s.setGeometry(1500, 500, 200, 200)
s.show()
sys.exit(app.exec_())
