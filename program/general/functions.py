
import settings
from time import strftime


def writeLog(log, message):
	f = open(settings.Settings.get("Application", "logDir") + "/" + log, "a")
	print(strftime("%d.%m.%Y %H:%M:%S") + " " + message, file=f)
	print("wrote in " + log)
	f.close()


