class darkScheme:
	backgroundColor = "#111111"
	foregroundColor = "#FFFFFF"

	brandColor = "#33852E" #main theme of the Application

	lightColor = "#999999"
	MidlightColor = "#777777"
	MidColor = "#555555"
	darkColor = "#222222"
	ShadowColor = "#000000"
	highlightColor = "#AAAAAA"


	statusBarBackground = brandColor
	loadingScreenBackground = "#276623"


	vehicleBackground = {
		-3: "#00877B",	#"checking status,
		-2: "#CD00DB",	#""Error: Unbekannter Status",
		-1: "#333333",	#"Error: no Status",
		 0: "#33852E",	#"Einsatzbereit",
		 1: "#004785",	#"Auf Dienstfahrt",
		 2: "#BD000D",	#"Im Einsatz"

		 500: "#FFFFFF"	# "Selected"
	}

	activeColor = "#33852E" #not used only for multiple references in this file
	inactiveColor = "#454545" #not used only for multiple references in this file

	activeTourBackground = "#004785"
	reservedTourBackground = inactiveColor
	

	allowedMemberColor = activeColor
	notAllowedMemberColor = inactiveColor

	activatedTextSnippet = activeColor
	deactivatedTextSnippet = inactiveColor


	vehicleNameColor = "#000000"


	#Forms
	textFieldBackground = "#222222"
	textFieldColor = foregroundColor
	textFieldBorder = "#454545"
	textLineBackground = "#222222"
	textLineColor = foregroundColor
	textLineBorder = "#454545"
	checkBoxBackground = "#222222"


	#buttons
	appButtonBackground = "#000000"
	appButtonPressedBackground = "#444444"
	appButtonColor = foregroundColor
	appButtonImportantBackground = "#690404"
	appButtonImportantColor = foregroundColor

	appToggleImportantCheck = activeColor
	appToggleImportantUncheck = "#690404"

	buttonPressedColor = activeColor

	returnButton = "#33852E"



