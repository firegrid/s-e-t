
import importlib

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from gui.MainWindow import *

import settings
from style.style import Fonts, Effects, Layout, Color

def getColorScheme():
	s = None
	try:
		s = importlib.import_module("style.colorschemes." + settings.Settings.get("Window", "colorScheme"))
	except:
		print("Color Scheme: " + settings.Settings.get("Window", "colorScheme") + " was not found.")
		Color.scheme = getattr(importlib.import_module("style.colorschemes.darkScheme"), "darkScheme")
		return
	Color.scheme = getattr(s, settings.Settings.get("Window", "colorScheme"))

def getColorPalette():
	Color.palette.setBrush(QPalette.Window, QBrush(QColor(Color.scheme.backgroundColor)))
	Color.palette.setBrush(QPalette.Foreground, QBrush(QColor(Color.scheme.foregroundColor)))
	Color.palette.setBrush(QPalette.Base, QBrush(QColor(Color.scheme.backgroundColor)))
	Color.palette.setBrush(QPalette.AlternateBase, QBrush(QColor(Color.scheme.backgroundColor).darker(20))) # maybe change this later??
	Color.palette.setBrush(QPalette.ToolTipBase, QBrush(QColor(Color.scheme.backgroundColor)))
	Color.palette.setBrush(QPalette.ToolTipText, QBrush(QColor(Color.scheme.foregroundColor)))
	Color.palette.setBrush(QPalette.Text, QBrush(QColor(Color.scheme.foregroundColor)))
	Color.palette.setBrush(QPalette.Button, QBrush(QColor(Color.scheme.highlightColor)))
	Color.palette.setBrush(QPalette.ButtonText, QBrush(QColor(Color.scheme.darkColor)))
	Color.palette.setBrush(QPalette.BrightText, QBrush(QColor(Color.scheme.foregroundColor).lighter(20)))
	Color.palette.setBrush(QPalette.Light, QBrush(QColor(Color.scheme.lightColor)))
	Color.palette.setBrush(QPalette.Midlight, QBrush(QColor(Color.scheme.MidlightColor)))
	Color.palette.setBrush(QPalette.Dark, QBrush(QColor(Color.scheme.darkColor)))
	Color.palette.setBrush(QPalette.Mid, QBrush(QColor(Color.scheme.MidColor)))
	Color.palette.setBrush(QPalette.Shadow, QBrush(QColor(Color.scheme.ShadowColor)))
	Color.palette.setBrush(QPalette.Highlight, QBrush(QColor(Color.scheme.highlightColor)))
	Color.palette.setBrush(QPalette.HighlightedText, QBrush(QColor(Color.scheme.foregroundColor).lighter(20)))

getColorScheme()
getColorPalette()
