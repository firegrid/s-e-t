from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import *

import settings

class Fonts:
	h0 = QFont(settings.Settings.get("Fonts", "h0_font"))
	h0.setBold(True)
	h0.setPointSize(settings.Settings.get("Fonts", "h0_size", "int"))


	h1 = QFont(settings.Settings.get("Fonts", "h1_font"))
	h1.setBold(True)
	h1.setPointSize(settings.Settings.get("Fonts", "h1_size", "int"))

	h2 = QFont(settings.Settings.get("Fonts", "h2_font"))
	h2.setBold(True)
	h2.setPointSize(settings.Settings.get("Fonts", "h2_size", "int"))

	h3 = QFont(settings.Settings.get("Fonts", "h3_font"))
	h3.setPointSize(settings.Settings.get("Fonts", "h3_size", "int"))

	h3bold = QFont(settings.Settings.get("Fonts", "h3_font"))
	h3bold.setBold(True)
	h3bold.setPointSize(settings.Settings.get("Fonts", "h3_size", "int"))

	h4 = QFont(settings.Settings.get("Fonts", "h4_font"))
	h4.setPointSize(settings.Settings.get("Fonts", "h4_size", "int"))
	
	bolder = QFont(settings.Settings.get("Fonts", "bolder_font"))
	bolder.setBold(True)
	bolder.setPointSize(settings.Settings.get("Fonts", "bolder_size", "int"))

	VehicleName = QFont(settings.Settings.get("Fonts", "VehicleName_font"))
	VehicleName.setBold(True)
	VehicleName.setPointSize(settings.Settings.get("Fonts", "VehicleName_size", "int"))

	VehicleStatus = QFont(settings.Settings.get("Fonts", "VehicleStatus_font"))
	VehicleStatus.setBold(False)
	VehicleStatus.setPointSize(settings.Settings.get("Fonts", "VehicleStatus_size", "int"))

	VehicleEquipment = QFont(settings.Settings.get("Fonts", "vehiclesequipment_font"))
	VehicleEquipment.setBold(False)
	VehicleEquipment.setPointSize(settings.Settings.get("Fonts", "vehiclesequipment_size", "int"))

	normal = QFont(settings.Settings.get("Fonts", "normal_font"))
	normal.setPointSize(settings.Settings.get("Fonts", "normal_size", "int"))

	bold = QFont(settings.Settings.get("Fonts", "normal_font"))
	bold.setBold(True)

	loadingTitle = QFont(settings.Settings.get("Fonts", "normal_font"))
	loadingTitle.setBold(True)
	loadingTitle.setPointSize(40)

	loadingSubTitle = QFont(settings.Settings.get("Fonts", "normal_font"))
	loadingSubTitle.setPointSize(30)


class Effects:
	def shadow(): #needs to be a function (otherwise effect would only be on last set)
		shadow = QGraphicsDropShadowEffect()
		shadow.setBlurRadius(2)
		shadow.setOffset(1,1)
		return shadow


class Layout:
	vSpacer = QSpacerItem(0,0,QSizePolicy.Minimum,QSizePolicy.Expanding)
	hSpacer = QSpacerItem(0,0,QSizePolicy.Expanding,QSizePolicy.Minimum)


class Color:
	scheme = None
	palette = QPalette()
	