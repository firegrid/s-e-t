'''
# CODE BY Lucas Dworschak
# FF Achau
# A-2481 Achau
# lucas.dworschak@gmx.at
'''

from logic.Curl import Curl
import settings

import threading

from logic.VehicleDistributionServices.Distributor import Distributor

from logic.VehicleDistributionServices.ServiceData.Vehicles import Vehicles, VehicleStatus
from logic.VehicleDistributionServices.ServiceData.Members import Member
from logic.VehicleDistributionServices.ServiceData.Mission import MissionStatus


class VehicleAdministration:
	instance = None

	def __init__(self, guiCallBack):
		Curl.curlCall("", settings.Settings.get("Application", "url122", "str") + "/service/terminal/index.php", None)# 122.at hack: "give me the key" :D

		VehicleAdministration.instance = self

		self.distributor = Distributor(self)

		self.vehicles = Vehicles()
		self.member = Member()

		self.guiCallBack = guiCallBack

		# self.distributor.mainDistributionService.getVehicleStatuses()

	def getMember(self, stbn, member = None):
		if member == None:
			self.member = member = Member()
		self.distributor.mainDistributionService.getMember(stbn, member)

	def getVehicles(self):
		t = threading.Thread(target=self.distributor.mainDistributionService.getVehicleStatuses)
		t.start()

	#dienstfahrten, brandsicherheitswachen
	def sendToTour(self, mission):
		self.distributor.sendToTour(mission)

		if mission.reason == MissionStatus.Mission:
			for v in mission.vehicleList:
				for vav in self.vehicles.vehicles:
					if v.vID == vav.vID:
						vav.changeStatus(VehicleStatus.checkStatus)
						break


		#change vehicle status in gui if in trainings mode				
		if settings.Settings.get("Application", "training", "bool") and mission.reason != MissionStatus.Reservation:
			for v in mission.vehicleList:
				for vav in self.vehicles.vehicles:
					if v.vID == vav.vID:
						vav.changeTrainingsStatus(mission.type)
						break

		self.update()

		self.guiCallBack.updateVehicles()


	#dienstfahrten, brandsicherheitswachen
	def returnFromTour(self, mission):
		self.distributor.returnFromTour(mission)

		if settings.Settings.get("Application", "url122", "str") == settings.Settings.get("Application", "realurl122", "str"):
			for v in mission.vehicleList:
				for vav in self.vehicles.vehicles:
					if v.vID == vav.vID:
						vav.changeStatus(VehicleStatus.checkStatus)
						break
		else:
			for v in mission.vehicleList:
				for vav in self.vehicles.vehicles:
					if v.vID == vav.vID:
						vav.changeStatus(VehicleStatus.ready)
						break

		if settings.Settings.get("Application", "training", "bool"):
			for v in mission.vehicleList:
				for vav in self.vehicles.vehicles:
					if v.vID == vav.vID:
						vav.changeStatus(VehicleStatus.ready)
						break

		self.update()

		self.guiCallBack.updateVehicles()

	def cancelTour(self,mission):
		self.distributor.cancelTour(mission)


	#einsätze
	def sendToMission(self, mission):
		self.distributor.sendToMission(mission)

	def returnFromMission(self, mission):
		self.distributor.returnFromMission(mission)
		

	#vehiclestatus
	def changeVehicleStatus(self, status):
		self.distributor.changeVehicleStatus(status)

	#equipment
	def addTempItem(self, item):
		self.distributor.addTempItem(item)


	def changeItem(self, item):
		self.distributor.changeItem(item)



	
	def update(self):
		self.getVehicles()

	#callbacks
	def memberCallBack(self, data):
		self.member.memberCallBack(data)

	def vehicleCallBack(self, data):
		self.vehicles.vehicleCallBack(data)




