from logic.VehicleDistributionServices.Interfaces import DistributionServiceIF
from logic.VehicleDistributionServices.ServiceData.Item import Item
from logic.VehicleDistributionServices.ServiceData.VehicleStatus import VehicleStatus
from logic.VehicleDistributionServices.ServiceData.Mission import Mission, MissionStatus


from logic.Curl import Curl

# from models.base import Database
# from models.tour import Tour, TourDriver

import settings
from general.functions import writeLog

import datetime


class StatusDistributor(DistributionServiceIF):
	def __init__(self):
		DistributionServiceIF.__init__(self)
		self.site = settings.Settings.get("Distribution", "statusDistributortrigger")

	

	def sendToTour(self, mission):
		for v in mission.vehicleList:
			if mission.reason == MissionStatus.GenericTour or mission.reason == MissionStatus.FireGuard:
				Curl.curlCall(None, self.site + str(v.model.identifier), encoding="ISO-8859-1")
			

	def returnFromTour(self, mission):
		for v in mission.vehicleList:
			Curl.curlCall(None, self.site + str(v.model.identifier), encoding="ISO-8859-1")

	#vehiclestatus
	def changeVehicleStatus(self, status):
		Curl.curlCall(None, self.site + str(status.vehicle), encoding="ISO-8859-1")

	#equipment
	def addTempItem(self, item):
		Curl.curlCall(None, self.site + str(item.vehicle), encoding="ISO-8859-1")

	def changeItem(self, item):
		Curl.curlCall(None, self.site + str(item.vehicle), encoding="ISO-8859-1")