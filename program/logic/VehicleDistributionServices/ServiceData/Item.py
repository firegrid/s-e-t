class Item:
	def __init__(self, identifier=-1, vehicle=None, name="", ready=True, person=-1, temp=True):
		self.identifier = identifier
		self.vehicle = vehicle
		self.name = name
		self.ready = ready
		self.responsiblePerson = person
		self.temporary = temp