import settings
from general.functions import writeLog

from models.base import Database
from models import Member as MemberModel

import json

class Member:
	def __init__(self):
		self.id = -1
		self.previd = -1
		self.stbn = -1
		self.rank = ""
		self.firstname = ""
		self.lastname = ""

		self.fullname122 = ""
		self.model = None

		self.fullname = ""
		self.isChecked = False


	def initialize(self):
		Database.connectDB()
		try:
			self.model = MemberModel.get(MemberModel.globalIdentifier == self.id)
			#self.model = [m for m in (query)][0]
		except MemberModel.DoesNotExist:
			try:
				MemberModel.update(globalIdentifier=self.id).where(MemberModel.stbn == self.stbn).execute()
				self.model = MemberModel.get(MemberModel.globalIdentifier == self.id)
			except MemberModel.DoesNotExist:
				MemberModel().insert(globalIdentifier=self.id, firstname=self.firstname, lastname=self.lastname, rank=self.rank, stbn = self.stbn).execute()
				self.model = MemberModel.get(MemberModel.globalIdentifier == self.id)
				writeLog("log.txt", self.firstname + " " + self.lastname + " " + str(self.id) + ": was just added to the database.")
		
		Database.closeDB()



	def loadFromDB(self, model):
		self.model = model
		self.id = self.model.globalIdentifier
		self.stbn = self.model.stbn
		self.rank = self.model.rank
		self.firstname = self.model.firstname
		self.lastname = self.model.lastname
		self.fullname = str(self.stbn) + " " + self.rank + " " + self.firstname + " " + self.lastname


	def memberCallBack(self, data):
		if data.strip().__len__() == 0:
			return

		self.isChecked = True
		d = json.loads(data)
		self.fullname122 = d["name"]
		self.previd = self.id
		self.id = d['id']

		pieces = self.fullname122.split(" ")

		lastPieceNR = pieces.__len__()-1


		self.stbn = int(pieces[0])
		self.rank = pieces[1]
		self.lastname = pieces[2]
		self.firstname = pieces[lastPieceNR]

		if(lastPieceNR != 3):
			for i in range(3, lastPieceNR):
				self.lastname = self.lastname + " " + pieces[i]
		
		if self.model == None or self.previd != self.id:
			self.initialize()

		self.fullname = str(self.stbn) + " " + self.rank + " " + self.firstname + " " + self.lastname

		self.checkIntegrity()



	def checkIntegrity(self):
		if self.firstname != self.model.firstname or self.lastname != self.model.lastname or self.rank != self.model.rank: 
			writeLog("error.txt", self.firstname + " " + self.lastname + " " + str(self.id) + ": Daten stimmten nicht mit dem von 122.at zussammen, und wurden automatisch in der Datenbank erneuert")
			Database.connectDB()
			MemberModel.update(globalIdentifier = self.id, firstname = self.firstname, lastname = self.lastname, rank = self.rank).where(MemberModel.stbn == self.model.stbn).execute()
			Database.closeDB()
			
			
		if self.stbn != self.model.stbn:
			writeLog("error.txt", self.firstname + " " + self.lastname + " " + str(self.id) + ": " + self.stbn + "=!" + self.model.stbn + "brauche manuelle richtigstellung in Mysql Datenbank")
			#this means that the globalidentifier from an member changed to an globalIdentifier of a different member in your Fire Brigade (or that the stbn changed...)
			#shouldnt happen, but would most likely be devastating -> so just an error log to truly look for, the problem..
