import settings
from general.functions import writeLog

from models.base import Database
from models import Vehicle as VehicleModel

from logic.VehicleDistributionServices.ServiceData.Mission import MissionStatus, Mission
from logic.VehicleDistributionServices.Distributor import Distributor

class VehicleStatus:
	checkStatus = -3
	unknownStatus = -2
	noStatus = -1
	ready = 0
	genericTour = 1
	missionTour = 2
	locallyNotReady = 3
	#hat brandsicherheitswache auch eine eigene farbe ???





readableStatus = {
	-3: "Status Aktualisierung",
	-2: "Error: Unbekannter Status",
	-1: "Error: no Status",
	0: "Einsatzbereit",
	1: "Auf Dienstfahrt",
	2: "Im Einsatz",
	3: "Nicht Einsatzbereit"
}


class Vehicle:

	Purpose = {
		MissionStatus.noStatus: VehicleStatus.noStatus, #kein aktionswechsel
		MissionStatus.Ready: VehicleStatus.ready, #Einsatzbereit
		MissionStatus.FireGuard: VehicleStatus.genericTour, #Brandsicherheitswache
		MissionStatus.GenericTour: VehicleStatus.genericTour #Dienstfahrt
	}

	def __init__(self):
		self.vID = -1
		self.name = ''
		self.status = VehicleStatus.noStatus
		self.selected = False
		self.driver = None
		self.model = None

	def initialize(self):
		Database.connectDB()
		try:

			self.model = VehicleModel.get(VehicleModel.identifier == self.vID)
		except VehicleModel.DoesNotExist:
			writeLog("log.txt", self.name + " " + str(self.vID) + ": was just added to the database.")
			self.model = VehicleModel().create(identifier=self.vID, longname=self.name)
		Database.closeDB()

	def getSelectOrStatus(self):
		if self.selected:
			return 500
		return self.status

	def toogleSelected(self):
		self.selected = not self.selected

	def changeStatus(self, s):
		if self.status == s:
			return

		if s == VehicleStatus.missionTour:
			m = Mission([self]) #maybe fill with more infos in the future (zb starttime == now...)
			Distributor.instance.sendToMission(m)
		elif self.status == VehicleStatus.missionTour:
			m = Mission([self]) # maybe fill with more infos in the future (zb endtime == now...)
			Distributor.instance.returnFromMission(m)


		if self.status != VehicleStatus.noStatus:
			writeLog("log.txt", "changing Status of " + str(self.name) + " from " +  str(self.status) + " to " + str(s))
			self.status = s
			self.model.status = s
			Database.connectDB()
			self.model.save()
			Database.closeDB()
		else:
			self.status = s


	def changeTrainingsStatus(self,s):
		self.status = Vehicle.Purpose.get(s, VehicleStatus.ready)


from html.parser import HTMLParser

class VehicleParser(HTMLParser):
	def handle_starttag(self, tag, attrs):
		if self.inRessource == True:
			for a in attrs:
				if a[0] == 'id':
					self.currentVehicle.vID = int(a[1])
				# elif a[0] == 'style':
				# 	self.currentVehicle.changeStatus(VehicleParser.getStatus(a[1]))
				elif a[0] == 'adr':
					if a[1] == '':
						self.currentVehicle.status = 1 #probably on dienstfahrt (maybe on einsatz.... TODO check)
					else:
						self.currentVehicle.status = int(a[1])

		else:
			for a in attrs:
				if a[0] == 'id' and a[1] == 'ressourcen_text':
					self.inRessource = True

	def handle_data(self, data):
		if self.inRessource == True:
			self.currentVehicle.name = data

			self.changeStatusOrAdd()

			self.currentVehicle = Vehicle()

	def __init__(self, vehicles):
		self.inRessource = False
		self.vehicles = vehicles
		self.currentVehicle = Vehicle()
		super().__init__()

	def changeStatusOrAdd(self):
		for v in self.vehicles:
			if v.vID == self.currentVehicle.vID:
				if settings.Settings.get("Application", "training", "bool") and v.status != VehicleStatus.noStatus:
					return
				v.changeStatus(self.currentVehicle.status)
				return

		self.currentVehicle.initialize()
		self.vehicles.append(self.currentVehicle)

	def getStatus(hexStatus):#deprecated !!??
		styles = hexStatus.split(';')
		bg = ''
		for st in styles:
			style = st.split(': ')
			if style[0]=="background-color":
				bg = style[1]

		if bg == '#C1FFC1':
			return VehicleStatus.ready
		elif bg == '#FFD697':
			return VehicleStatus.genericTour
		elif bg == '#FILLLMISSION':
			return VehicleStatus.missionTour
		else:
			writeLog("colors.txt", "unknown status " + self.vID + ": " + bg )
			return VehicleStatus.unknownStatus
			



from models import Vehicle as VehicleModel

class Vehicles:
	def __init__(self):
		self.vehicles = []

		self.selectedVehicleList = []

		cv = Vehicle()
		Database.connectDB()
		for v in VehicleModel.select().where(VehicleModel.active == True).order_by(VehicleModel.sorting.asc()):
			cv.vID = v.identifier
			cv.name = v.longname #TODO make customizable
			cv.status = -1
			cv.model = v
			self.vehicles.append(cv)
			cv = Vehicle()
		Database.closeDB()

	def parse(self, data):
		parser = VehicleParser(self.vehicles)

		parser.feed(data)
		self.vehicles = sorted(self.vehicles, key=lambda vh: vh.model.sorting)

	def vehicleCallBack(self, data):
		self.parse(data)

	def deselectAll(self):
		for v in reversed(self.selectedVehicleList):
			v.selected = False
			self.selectedVehicleList.remove(v)

	def getList(self,vList):
		reList = []
		for v in vList:
			for sv in self.vehicles:
				if v.vID == sv.vID:
					reList.append(sv)

		return reList

	def selectList(self, vList):
		for v in vList:
			for sv in self.vehicles:
				if v.vID == sv.vID:
					sv.selected = True
					self.selectedVehicleList.append(sv)

