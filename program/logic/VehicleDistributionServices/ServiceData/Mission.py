class MissionStatus:
	noStatus = -1
	Ready = 0
	GenericTour = 11 #dienstfahrt
	FireGuard = 12 #bsw
	Mission = 13 #einsatz
	Reservation = 14


class Mission:
	def __init__(self, vehicles = [], title = "", message = "", timestart=None, timeend=None, reason=-1, type=-1):
		
		self.vehicleList = vehicles
		self.model = None
		self.type = type
		self.title = title
		self.message = message
		self.timestart = timestart
		self.timeend = timeend
		self.reason = reason

