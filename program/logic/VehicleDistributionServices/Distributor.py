from logic.VehicleDistributionServices.at122 import AT122Service
from logic.VehicleDistributionServices.EIS import EISService
from logic.VehicleDistributionServices.DB import DBService
from logic.VehicleDistributionServices.StatusDistributor import StatusDistributor
from logic.VehicleDistributionServices.DistributionError import DistributionErrorHandler, DistributionError

import threading

import settings

from gui.misc.Emitters import Emitter

class Distributor:
	instance = None

	def __init__(self, va):
		Distributor.instance = self

		self.distributionServices = []

		self.errorHandler = DistributionErrorHandler()
		self.errorHandlerThread = threading.Thread(target=self.errorHandler.errorHandler)
		self.errorHandlerThread.start()

		Emitter.getInstance("closeApplication").connect(self.close)
		

		self.distributionServices.append(DBService())
		self.distributionServices.append(AT122Service(va))

		if settings.Settings.get("Distribution", "eis", "bool"):
			self.distributionServices.append(EISService())

		if settings.Settings.get("Distribution", "statusDistributor", "bool"):
			self.distributionServices.append(StatusDistributor())

		self.mainDistributionService = None
		for s in self.distributionServices:
			if s.MainDataSource:
				self.mainDistributionService = s


	def close(self):
		self.errorHandler.running = False
		# self.errorHandlerThread.join()
		


	def sendToTour(self, mission):
		for s in self.distributionServices:
			try:
				s.sendToTour(mission)
			except DistributionError:
				self.errorHandler.addError(s.sendToTour, mission)

	def returnFromTour(self, mission):
		for s in self.distributionServices:
			try:
				s.returnFromTour(mission)
			except DistributionError:
				self.errorHandler.addError(s.returnFromTour, mission)


	def cancelTour(self,mission):
		for s in self.distributionServices:
			try:
				s.cancelTour(mission)
			except DistributionError:
				self.errorHandler.addError(s.cancelTour, mission)


	def sendToMission(self, mission):
		for s in self.distributionServices:
			try:
				s.sendToMission(mission)
			except DistributionError:
				self.errorHandler.addError(s.sendToMission, mission)


	def returnFromMission(self, mission):
		for s in self.distributionServices:
			try:
				s.returnFromMission(mission)
			except DistributionError:
				self.errorHandler.addError(s.returnFromMission, mission)


	#vehiclestatus
	def changeVehicleStatus(self, status):
		for s in self.distributionServices:
			try:
				s.changeVehicleStatus(status)
			except DistributionError:
				self.errorHandler.addError(s.changeVehicleStatus, status)


	#equipment
	def addTempItem(self, item):
		for s in self.distributionServices:
			try:
				s.addTempItem(item)
			except DistributionError:
				self.errorHandler.addError(s.addTempItem, item)


	def changeItem(self, item):
		for s in self.distributionServices:
			try:
				s.changeItem(item)
			except DistributionError:
				self.errorHandler.addError(s.changeItem, item)

	


	



	


		


