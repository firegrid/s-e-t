from logic.VehicleDistributionServices.Interfaces import DistributionServiceIF
from logic.VehicleDistributionServices.ServiceData.Mission import Mission, MissionStatus
from logic.Curl import Curl
import settings

from general.functions import writeLog

import datetime

from models.base import Database
from models.tour import Tour, TourDriver
from models.vehicle import Vehicle
from models.equipment import Equipment


class DBService(DistributionServiceIF):
	def __init__(self):
		DistributionServiceIF.__init__(self)
		self.site = ""

	def sendToTour(self, mission):
		active = False
		reserved = False
		if mission.reason == MissionStatus.Reservation:
			reserved = True
		elif mission.reason == MissionStatus.Mission:
			return
		else:
			active = True

		
		Database.connectDB()
		if mission.model != None:
			mission.model.update(title=mission.title, message=mission.message, type=mission.type, begin=mission.timestart, expectedEnd=mission.timeend, active=active, reserved=reserved).where(Tour.identifier==mission.model.identifier).execute()
		else:
			mission.model = Tour.create(title=mission.title, message=mission.message, type=mission.type, begin=mission.timestart, expectedEnd=mission.timeend, active=active, reserved=reserved)

		for v in mission.vehicleList:
			td = None
			try:
				td = TourDriver.get(tour=mission.model.identifier, vehicle=v.model.identifier)
				td.update(driver=v.driver.model.globalIdentifier)
			except TourDriver.DoesNotExist:
				td = TourDriver.create(tour=mission.model.identifier, vehicle=v.model.identifier, driver=v.driver.model.globalIdentifier)
			if(not reserved):
				v.model.status = 1
			v.model.save()

		Database.closeDB()
		


	def returnFromTour(self, mission):
		Database.connectDB()
		
		possibleMissions = []
		if mission.model == None:
			possibleMissions = [t for t in Tour.select().where(Tour.active==True)]
		else:
			possibleMissions.append(mission.model)
		for v in mission.vehicleList:
			found = False

			v.model.status = 0
			v.model.save()

			for m in possibleMissions:
				try:
					td = TourDriver.get(tour=m.identifier, vehicle=v.model.identifier, end="0000-00-00 00:00:00")
					td.update(driver=v.driver.model.globalIdentifier, end=datetime.datetime.now()).where(TourDriver.tour==m.identifier, TourDriver.vehicle==v.model.identifier).execute()
					found = True
					mission.model = Tour.get(Tour.identifier==m.identifier)
					break
				except TourDriver.DoesNotExist:
					found = False

			if found == False:
				writeLog("error.txt", "Tried to end Tour in DB but couldnt find it...")
		for m in possibleMissions:
			notEndedTours = [t for t in TourDriver.select().where(TourDriver.end=="0000-00-00 00:00:00", TourDriver.tour==m.identifier)]
			if notEndedTours.__len__() == 0:
				m.update(active=False).where(Tour.identifier==m.identifier).execute()

		Database.closeDB()

		


				
	def cancelTour(self, mission):
		Database.connectDB()
		mission.model.update(active=False, reserved=False).where(Tour.identifier==mission.model.identifier).execute()
		TourDriver.delete().where(TourDriver.tour==mission.model.identifier).execute()
		Database.closeDB()


	#vehiclestatus
	def changeVehicleStatus(self, status):
		Database.connectDB()
		vehicle = Vehicle.get(Vehicle.identifier == status.vehicle)
		vehicle.ready = status.ready
		vehicle.save()
		Database.closeDB()

	#equipment
	def addTempItem(self, item):
		Database.connectDB()
		e = Equipment()
		e.vehicle = item.vehicle
		e.name = item.name
		e.ready = item.ready
		e.responsiblePerson = item.responsiblePerson
		e.temporary = item.temporary

		e.save(force_insert=True)
		Database.closeDB()

	def changeItem(self, item):
		Database.connectDB()
		# print(item.identifier)
		# print("================0")
		e = Equipment().get(Equipment.identifier == item.identifier)
		e.ready = item.ready
		e.responsiblePerson = item.responsiblePerson
		e.save()
		Database.closeDB()
		
