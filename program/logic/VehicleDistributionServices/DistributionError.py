import time, sched

from gui.misc.Emitters import IntegerEmitter

class DistributionError(Exception):
	pass




class DistributionErrorHandler:

	instance = None
	RETRYINTERVAL = 30 # in seconds

	def __init__(self):
		DistributionErrorHandler.instance = self
		self.errorQueue = []
		self.scheduler = sched.scheduler(time.time, time.sleep)
		self.running = True


	# adds an error to the errorQueue
	# function runs in another thread as the errorHandler
	# this function is the only way an error is added to the queue
	# input: 
	#	service = function that needs to be executed
	#	data = data which needs to be passed to the function
	# errorQueue Items contain tuples: (service, data)
	# sends an DistributionErrorLength Signal everytime an error is added
	def addError(self, service, data):
		self.errorQueue.append((service, data))
		IntegerEmitter.getInstance("DistributionErrorLength").emit(self.errorQueue.__len__())

	# function checks if the error queue is empty.
	# if it isn't it calls the retryErrors function to retry every service
	# 	if the service call was successful it removes the successful elements from the errorqueue
	#	only this function can remove elements from self.errorQueue
	# everytime this function is executed it calls itself after a specified time interval
	#	unless the running flag is turned to false, which causes the thread to come to an end, when its ready
	# sends an DistributionErrorLength Signal everytime an error is removed
	# this function is due to the block of the scheduler.run() method in its own thread
	# 	so synchronization errors might appear.. 
	#	but since its only access to other threads is self.errorQueue, there shouldnt be any problems...
	#	TODO Test: since the Curl method is used here and this method is writing cookies in the cookie.txt file there might still be some errors if two threads are writing on this file..
	def errorHandler(self):		
		if self.errorQueue.__len__() != 0:
			s = DistributionErrorHandler.retryErrors(self.errorQueue[:])
			self.errorQueue = [el for el in self.errorQueue if el not in s]
			IntegerEmitter.getInstance("DistributionErrorLength").emit(self.errorQueue.__len__())

		if self.scheduler.empty() and self.running:
			self.scheduler.enter(DistributionErrorHandler.RETRYINTERVAL, 1, self.errorHandler, ())
			self.scheduler.run()
			

	# try to use previously unavailable service
	# input: list of tuples: (service, data)
	#	service = function that needs to be executed
	#	data = data which needs to be passed to the function
	# returns a list with every service that was a success (in the format of one input item)
	def retryErrors(queue):
		successqueue = []

		for service, data in queue:
			try:
				service(data)
				successqueue.append((service, data))
			except DistributionError:
				# ignore future DistributionError because it is already in the queue
				pass

		return successqueue