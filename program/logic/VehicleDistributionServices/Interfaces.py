
class DistributionServiceIF:
	def __init__(self):
		self.MainDataSource = False
		self.site = ""

	def sendToMission(self, mission):
		return

	def returnFromMission(self, mission):
		return

	def sendToTour(self, mission):
		return

	def returnFromTour(self, mission):
		return

	def cancelTour(self, mission):
		return

	#vehiclestatus
	def changeVehicleStatus(self, status):
		return

	#equipment
	def addTempItem(self, item):
		return

	def changeItem(self, item):
		return


class MainDistributionServiceIF(DistributionServiceIF):
	def __init__(self, callBack):
		self.MainDataSource = True
		self.callBack = callBack

	def getVehicleStatuses(self):
		#unsupported
		return

	def getMember(self, stbn, member):
		#unsupported
		return
