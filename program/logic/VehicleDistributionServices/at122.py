from logic.VehicleDistributionServices.Interfaces import MainDistributionServiceIF
from logic.VehicleDistributionServices.ServiceData.Mission import Mission, MissionStatus

import datetime

from logic.Curl import Curl
import settings


class AT122Service(MainDistributionServiceIF):
	Purpose = {
		MissionStatus.noStatus: -1, #kein aktionswechsel
		MissionStatus.Ready: 1, #Einsatzbereit
		MissionStatus.FireGuard: 2, #Brandsicherheitswache
		MissionStatus.GenericTour: 3 #Dienstfahrt
	}


	def __init__(self, callBack):
		MainDistributionServiceIF.__init__(self, callBack)
		self.site = settings.Settings.get("Application", "url122", "str") + "/service/terminal/terminal_config.php"

	def sendToTour(self, mission):
		#if the mission doesnt exist here yet just send a normal generic Tour
		if mission.reason == MissionStatus.Reservation:
			#Reservations are not transmitted to 122.at
			return

		#translate internal numbering to 122 specific numbering
		indent = AT122Service.Purpose.get(mission.reason, MissionStatus.GenericTour)

		customMessage = mission.message

		if mission.timestart != mission.timeend:
			customMessage = customMessage + " \n" + "Erwartetes Ende: " + mission.timeend.strftime("%d.%m.%Y %H:%M")

		for v in mission.vehicleList:
			postData = 'save_status=save_status&aid=' + settings.Settings.get("Feuerwehr", "aid") + '&stbn=' + str(v.driver.model.globalIdentifier) + '&mitglied=' + v.driver.fullname122 + '&aktion=info_yes&status=' + str(indent) + '&text=' + customMessage + '&fahrzeugid=' + str(v.vID)
			if not settings.Settings.get("Application", "training", "bool"):
				Curl.curlCall(postData, self.site)

	def returnFromTour(self, mission):
		for v in mission.vehicleList:
			postData = 'close_event=close_event&aid=' + settings.Settings.get("Feuerwehr", "aid") + '&fahrzeugid=' + str(v.vID)
			if not settings.Settings.get("Application", "training", "bool"):
				Curl.curlCall(postData, self.site)


	def cancelTour(self, mission):
		notUsed = True


		


	def getVehicleStatuses(self):
		postData = 'check=check&rid='+ settings.Settings.get("Feuerwehr", "rid") + '&aid=' + settings.Settings.get("Feuerwehr", "aid") + '&fwname=' + settings.Settings.get("Feuerwehr", "name")
		Curl.curlCall(postData, self.site, self.callBack.vehicleCallBack)

	def getMember(self, stbn, member):
		postData = 'search_fwmitglied=search_fwmitglied&search_stbn=' + str(stbn) + '&id=' + settings.Settings.get("Feuerwehr", "aid")
		Curl.curlCall(postData, self.site, member.memberCallBack)


