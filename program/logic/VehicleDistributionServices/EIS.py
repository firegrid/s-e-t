from logic.VehicleDistributionServices.Interfaces import DistributionServiceIF
from logic.VehicleDistributionServices.ServiceData.Mission import Mission, MissionStatus
from logic.Curl import Curl

# from models.base import Database
# from models.tour import Tour, TourDriver

import settings
from general.functions import writeLog

import datetime


class EISService(DistributionServiceIF):
	def __init__(self):
		DistributionServiceIF.__init__(self)
		self.site = "http://admin:FFA2013@www.ffmoedling.at/fleet_bezirk/admin_achau.php"

	def sendToMission(self, mission):
		for v in mission.vehicleList:
			post2Data = "action=change_truck_status&truck_id=" + str(v.model.eisIdentifier) + "&name=server&button=ausger%FCckt"
			Curl.curlCall(post2Data, self.site, encoding="ISO-8859-1")

	def returnFromMission(self, mission):
		for v in mission.vehicleList:
			post2Data = "action=change_truck_status&truck_id=" + str(v.model.eisIdentifier) + "&name=server&button=Einsatzbereit"
			Curl.curlCall(post2Data, self.site, encoding="ISO-8859-1")


	def sendToTour(self, mission):
		timeend = mission.timeend
		if mission.timestart == mission.timeend:
			timeend = mission.timestart + datetime.timedelta(hours = 2)

		postData = "action=Speichern&id=&icon=auto.png&time_text="

		# if mission.reason == Purpose.reservation:
		# 	postData = "action=Speichern&id=&icon=auto.png&time_text="
		# else:
		# 	postData = "action=change_truck_status&name=server&button="

		# 	if mission.reason == Purpose.notAvailable:
		# 		postData = postData + "nicht+Verf%FCgbar"
		# 	elif mission.reason == Purpose.mission:
		# 		postData = postData + "ausger%FCckt"
		# 	else:
		# 		postData = postData + "nicht+Einsatzbereit"

		
		for v in mission.vehicleList:
			customMessage = v.model.shortname + " - " + v.driver.rank + " " + v.driver.firstname + " " + v.driver.lastname + "\n" +  mission.message

			if mission.reason == MissionStatus.Mission:
				postData = postData + "&truck_id="+ str(v.model.eisIdentifier)
			else:
				postData = postData + "&truck_id="+ str(v.model.eisIdentifier) +"&text="+customMessage+"&from_date="+mission.timestart.strftime("%d.%m.%Y")+"&from_time="+mission.timestart.strftime("%H:%M")+"&till_date="+timeend.strftime("%d.%m.%Y")+"&till_time="+timeend.strftime("%H:%M") + "&id=" + str((mission.model.identifier + v.model.eisIdentifier * 100) % 1000)

			Curl.curlCall(postData, self.site, encoding="ISO-8859-1")

		if mission.reason == MissionStatus.Mission:
			self.sendToMission(mission)
			

	def returnFromTour(self, mission):
		for v in mission.vehicleList:
			post2Data = "action=L%F6schen&id=" + str((mission.model.identifier + v.model.eisIdentifier * 100) % 1000)
			Curl.curlCall(post2Data, self.site, encoding="ISO-8859-1")

	def cancelTour(self, mission):
		for v in mission.vehicleList:
			postData = "action=L%F6schen&id=" + str((mission.model.identifier + v.model.eisIdentifier * 100) % 1000)
			Curl.curlCall(postData, self.site, encoding="ISO-8859-1")

