import pycurl
from general.functions import writeLog
from io import BytesIO

from gui.misc.Emitters import Emitter

from logic.VehicleDistributionServices.DistributionError import DistributionError

import gzip

class Curl:
	#utf-8, ISO-8859-1
	def curlCall(data, url, callBack = None, encoding = "utf-8"):
		c = pycurl.Curl()
		c.setopt(pycurl.SSL_VERIFYPEER, 0)
		c.setopt(pycurl.SSL_VERIFYHOST, 0)
		c.setopt(pycurl.URL, url)
		c.setopt(pycurl.HTTPHEADER, ['Accept: text/html, */*; q=0.01'])
		c.setopt(pycurl.HTTPHEADER, ['Content-Type : application/x-www-form-urlencoded; charset=UTF-8'])
		c.setopt(pycurl.REFERER, "https://www.122.at/service/terminal/index.php?id=599809867200we122berf0e21b1da054d90935f6d2851ba48b04we144berdf77ee58302b1eea03a37bfd7d70bec9&color=black%20%20IDList=")
		c.setopt(pycurl.USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0')
		c.setopt(pycurl.COOKIEFILE, 'cookie.txt')
		c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
		c.setopt(pycurl.HTTPHEADER,['X-Requested-With: XMLHttpRequest', "Accept-Language: en,de-DE;q=0.9,de;q=0.8"])
		c.setopt(pycurl.NOSIGNAL, 1)
		if data != None:
			data = data.encode(encoding)
			c.setopt(pycurl.POST, 1)
			c.setopt(pycurl.POSTFIELDS, data)

		data = BytesIO()
		c.setopt(c.WRITEFUNCTION, data.write)

		try:
			c.perform()
		except pycurl.error as error:
			errno = error.args[0]
			errstr = error.args[1]
			writeLog("error.log", str(errno) + " " + str(errstr))
			if errno in [6, 7]:
				raise DistributionError()
				


		c.close()

		if callBack != None:
			callBack(data.getvalue().decode('utf8'))
			# callBack(gzip.decompress(data.getvalue()).decode('utf8'))
		else:
			return data.getvalue().decode('utf8')


			

