#!/usr/bin/env python3

'''
# CODE BY Lucas Dworschak
# FF Achau
# A-2481 Achau
# lucas.dworschak@gmx.at
'''

import settings
import style

# import logic
# import logic.VehicleDistributionServices
# import logic.VehicleDistributionServices.ServiceData

import sys

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from gui.MainWindow import *

from gui.misc.Dialogs import AppDialogBox


from gui.misc.Emitters import Emitter
from functools import partial

if __name__ == '__main__':
	import sys

	app = QApplication(sys.argv)

	app.setStyle(QStyleFactory.create("fusion"))
	app.setPalette(style.Color.palette)

	args = sys.argv
	
	AppDialogBox.init()
	screen = MainWindow()

	if settings.Settings.get("Window","fullscreen", "bool"):
		screen.showFullScreen()
	else:
		screen.setGeometry(settings.Settings.get("Window", "startX", "int"),settings.Settings.get("Window", "startY", "int"), settings.Settings.get("Window", "width", "int"), settings.Settings.get("Window", "height", "int"))
		screen.show()


	if not settings.Settings.get("Window", "showCursor", "bool"):
		screen.setCursor(QCursor(Qt.BlankCursor))

	screen.setFont(style.Fonts.normal)


	# connect Sounds
	if settings.Settings.get("Application","audio", "bool"):
		from gui.misc.Sound import Sound
		Sound.setInstance("click", "click.wav")
		Emitter.getInstance("clickSound").connect(partial(Sound.play, "click"))

	screen.show()
	sys.exit(app.exec_())