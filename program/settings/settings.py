import configparser

class Settings:
	configFile = "config.ini"
	loadedSettings = {}
	config = configparser.ConfigParser()
	initialized = False

	def loadStr(section, name):
		return Settings.config.get(section, name)
	def loadInt(section, name):
		return Settings.config.getint(section, name)
	def loadfloat(section, name):
		return Settings.config.getfloat(section, name)
	def loadBool(section, name):
		return Settings.config.getboolean(section, name)

	load = {
		"str"	: loadStr,
		"int"	: loadInt,
		"float"	: loadfloat,
		"bool"	: loadBool,
	}

	def init():
		Settings.initialized = True
		Settings.config = configparser.ConfigParser()
		Settings.config.read(Settings.configFile)

		#static settings here
		Settings.loadedSettings["Static"] = {}
		Settings.loadedSettings["Static"]["updateSite"] = "http://www.ffachau.at/firegrid/s-e-t/version"

	def get(section, name, type="str"):
		if not Settings.initialized:
			Settings.init()

		if section in Settings.loadedSettings:
			if name in Settings.loadedSettings[section]:
				return Settings.loadedSettings[section][name]
			else:
				Settings.loadedSettings[section][name] = Settings.load[type](section, name)
		else:
			Settings.loadedSettings[section] = {}
			Settings.loadedSettings[section][name] = Settings.load[type](section, name)
			
		return Settings.loadedSettings[section][name]


	# writes settings to config file
	def set(section, name, value):
		if not Settings.initialized:
			Settings.init()

		# if settings are already loaded overwrite the current value
		if section in Settings.loadedSettings:
			if name in Settings.loadedSettings[section]:
				Settings.loadedSettings[section][name] = str(value)

		# write the new value into the list
		Settings.config.set(section, name, str(value))

		with open(Settings.configFile, "w") as f:
			Settings.config.write(f)


