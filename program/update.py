#!/usr/bin/env python3

import configparser
import os


class UpdateSettings:
	def __init__(self, path):
		self.config = configparser.ConfigParser()
		self.path = path
		if not os.path.exists(self.path):
			self.createConfig()

		self.config.read(self.path)

		self.section = ""


	def update(self):
		print("updating Settings File ...")

		self.changeSection("Application")
		self.setStandardSetting("logDir", "logs")
		self.setStandardSetting("imagePath", "/media/images/")
		self.setStandardSetting("training", "True")
		self.setStandardSetting("allowUpdates", "True")
		self.setStandardSetting("adminLogin", "0000")
		self.setStandardSetting("audio", "False")
		self.setStandardSetting("url122", "https://www.122.at")

		self.changeSection("Database")
		self.setStandardSetting("server", "192.168.1.3")
		self.setStandardSetting("user", ">USERNAME<")
		self.setStandardSetting("password", ">PASSWORD<")
		self.setStandardSetting("databasename", "ffachau")
		self.setStandardSetting("port", "3306")

		self.changeSection("Feuerwehr")
		self.setStandardSetting("aid", "3332277040")
		self.setStandardSetting("rid", "3490466072")
		self.setStandardSetting("name", "FF Achau")

		self.changeSection("Fonts")
		self.setStandardSetting("selectedVehicle_size", "18")
		self.setStandardSetting("selectedVehicle_font", "Ubuntu")
		self.setStandardSetting("h0_size", "28")
		self.setStandardSetting("h0_font", "Ubuntu")
		self.setStandardSetting("h1_size", "22")
		self.setStandardSetting("h1_font", "Ubuntu")
		self.setStandardSetting("h2_size", "18")
		self.setStandardSetting("h2_font", "Ubuntu")
		self.setStandardSetting("h3_size", "16")
		self.setStandardSetting("h3_font", "Ubuntu")
		self.setStandardSetting("h4_size", "14")
		self.setStandardSetting("h4_font", "Ubuntu")
		self.setStandardSetting("bolder_size", "14")
		self.setStandardSetting("bolder_font", "Ubuntu")
		self.setStandardSetting("VehicleName_size", "16")
		self.setStandardSetting("VehicleName_font", "Ubuntu")
		self.setStandardSetting("VehicleStatus_size", "14")
		self.setStandardSetting("VehicleStatus_font", "Ubuntu")
		self.setStandardSetting("normal_size", "10")
		self.setStandardSetting("normal_font", "Ubuntu")

		self.changeSection("Vehicle")
		self.setStandardSetting("imagePath", "vehicles/")
		self.setStandardSetting("layout", "0")
		self.setStandardSetting("columns", "3")
		self.setStandardSetting("imageSize", "225")
		self.setStandardSetting("spacing", "20")
		self.setStandardSetting("borderRadius", "20")
		self.setStandardSetting("width", "70")

		self.changeSection("Window")
		self.setStandardSetting("fullscreen", "True")
		self.setStandardSetting("startX", "0")
		self.setStandardSetting("startY", "20")
		self.setStandardSetting("width", "1600")
		self.setStandardSetting("height", "850")
		self.setStandardSetting("colorScheme", "darkScheme")
		self.setStandardSetting("animation", "False")
		self.setStandardSetting("showCursor", "False")

		self.changeSection("Distribution")
		self.setStandardSetting("eis", "False")
		self.setStandardSetting("equipmenttrigger", "")

		self.changeSection("Layout")
		self.setStandardSetting("memberColumns", "4")



		with open(self.path, "w") as f:
			self.config.write(f)



	def createConfig(self):
		with open(self.path, "w") as f:
			self.config.write(f)

	def changeSection(self, section):
		self.section = section
		if not self.config.has_section(section):
			self.config.add_section(section)

	def setStandardSetting(self, name, value):
		if not self.config.has_option(self.section, name):
			self.config.set(self.section, name, value)


class Updater:
	def __init__(self):
		UpdateSettings("config.ini").update()




u = Updater()


